﻿namespace Ggd.Splorr.Gadabout.MGClient

open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Graphics
open Ggd.Splorr.Gadabout
open Microsoft.Xna.Framework.Input
open System


type Client()  as this =
    inherit Game()

    let graphics = new GraphicsDeviceManager(this)
    let mutable context : SpriteBatch * Map<Pattern,Texture2D> = (Unchecked.defaultof<SpriteBatch>, Map.empty)
    let mutable lastCommandCounts:Map<Command, uint32> = Map.empty
    let mutable lastCommandTimes:Map<Command,TimeSpan> = Map.empty

    let mutable world:World option = None
    do
        graphics.PreferredBackBufferWidth  <- DisplayConstants.ScreenWidth
        graphics.PreferredBackBufferHeight <- DisplayConstants.ScreenHeight

    override this.LoadContent() =
        context <- new SpriteBatch(this.GraphicsDevice),  DisplayPatterns.loadTextures(this.GraphicsDevice)
        this.Window.Title <- "Gadabout of Splorr!!"

    override this.Update(gameTime:GameTime) : unit =
        let commandCounts = ViewState.getCommandCounts (Keyboard.GetState()) (GamePad.GetState(PlayerIndex.One))

        let (c:Set<Command*bool>, cc:Map<Command, TimeSpan>) = 
            commandCounts
            |> Map.fold (fun (x,xx) k v -> 
                            match lastCommandCounts.TryFind k, v, lastCommandTimes.TryFind k with
                            | None, count, _ when count > 0u -> //command press
                                (x |> Set.add (k,false),xx |> Map.add k gameTime.TotalGameTime) 
                            | Some count, 0u, _  when count > 0u -> //command release
                                (x,xx|>Map.remove k)
                            | Some lastCount, count, Some time when lastCount>0u && count>0u && (gameTime.TotalGameTime-time).TotalMilliseconds>250.0 -> //hold
                                (x |> Set.add (k,true),xx |> Map.add k gameTime.TotalGameTime) 
                            | _ ->
                                (x,xx)) (Set.empty,lastCommandTimes)

        lastCommandCounts <- commandCounts
        lastCommandTimes <- cc

        world <-
            (world, c)
            ||> Set.fold (fun acc c -> acc |> Game.handle c)

    override this.Draw(gameTime:GameTime) :unit = 
        this.GraphicsDevice.Clear <| DisplayColors.getColor Hue.Onyx

        (context |> fst).Begin(samplerState=new SamplerState(Filter=TextureFilter.Point))

        world |> ViewState.render context

        (context |> fst).End()


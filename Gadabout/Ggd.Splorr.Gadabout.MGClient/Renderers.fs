﻿namespace Ggd.Splorr.Gadabout.MGClient

open Ggd.Splorr.Gadabout
open Microsoft.Xna.Framework.Graphics
open Microsoft.Xna.Framework


type RenderingContext = SpriteBatch * Map<Pattern,Texture2D>

module RectangleRenderer =
    let render (spriteBatch:SpriteBatch, textures:Map<Pattern,Texture2D>) (scale:int) (x:int,y:int) (hue:Hue) (width:int,height:int) : unit =
        spriteBatch.Draw(textures.[Pattern.Solid], new Rectangle(scale*x,scale*y,scale*width,scale*height),DisplayColors.getColor hue)

module PatternRenderer =
    let render  (spriteBatch:SpriteBatch, textures:Map<Pattern,Texture2D>) (scale:int) (x:int,y:int) (hue:Hue) (pattern: Pattern) : unit =
        spriteBatch.Draw(textures.[pattern],new Rectangle(x * scale,y * scale,scale * Constants.PatternWidth,scale * Constants.PatternHeight),DisplayColors.getColor hue)

module TextRenderer =
    let private table =
        [
        ('\u0000',Pattern.Character00);
        ('\u0001',Pattern.Character01);
        ('\u0002',Pattern.Character02);
        ('\u0003',Pattern.Character03);
        ('\u0004',Pattern.Character04);
        ('\u0005',Pattern.Character05);
        ('\u0006',Pattern.Character06);
        ('\u0007',Pattern.Character07);
        ('\u0008',Pattern.Character08);
        ('\u0009',Pattern.Character09);
        ('\u000A',Pattern.Character0A);
        ('\u000B',Pattern.Character0B);
        ('\u000C',Pattern.Character0C);
        ('\u000D',Pattern.Character0D);
        ('\u000E',Pattern.Character0E);
        ('\u000F',Pattern.Character0F);

        ('\u0010',Pattern.Character10);
        ('\u0011',Pattern.Character11);
        ('\u0012',Pattern.Character12);
        ('\u0013',Pattern.Character13);
        ('\u0014',Pattern.Character14);
        ('\u0015',Pattern.Character15);
        ('\u0016',Pattern.Character16);
        ('\u0017',Pattern.Character17);
        ('\u0018',Pattern.Character18);
        ('\u0019',Pattern.Character19);
        ('\u001A',Pattern.Character1A);
        ('\u001B',Pattern.Character1B);
        ('\u001C',Pattern.Character1C);
        ('\u001D',Pattern.Character1D);
        ('\u001E',Pattern.Character1E);
        ('\u001F',Pattern.Character1F);

        ('\u0020',Pattern.Character20);
        ('\u0021',Pattern.Character21);
        ('\u0022',Pattern.Character22);
        ('\u0023',Pattern.Character23);
        ('\u0024',Pattern.Character24);
        ('\u0025',Pattern.Character25);
        ('\u0026',Pattern.Character26);
        ('\u0027',Pattern.Character27);
        ('\u0028',Pattern.Character28);
        ('\u0029',Pattern.Character29);
        ('\u002A',Pattern.Character2A);
        ('\u002B',Pattern.Character2B);
        ('\u002C',Pattern.Character2C);
        ('\u002D',Pattern.Character2D);
        ('\u002E',Pattern.Character2E);
        ('\u002F',Pattern.Character2F);

        ('\u0030',Pattern.Character30);
        ('\u0031',Pattern.Character31);
        ('\u0032',Pattern.Character32);
        ('\u0033',Pattern.Character33);
        ('\u0034',Pattern.Character34);
        ('\u0035',Pattern.Character35);
        ('\u0036',Pattern.Character36);
        ('\u0037',Pattern.Character37);
        ('\u0038',Pattern.Character38);
        ('\u0039',Pattern.Character39);
        ('\u003A',Pattern.Character3A);
        ('\u003B',Pattern.Character3B);
        ('\u003C',Pattern.Character3C);
        ('\u003D',Pattern.Character3D);
        ('\u003E',Pattern.Character3E);
        ('\u003F',Pattern.Character3F);

        ('\u0040',Pattern.Character40);
        ('\u0041',Pattern.Character41);
        ('\u0042',Pattern.Character42);
        ('\u0043',Pattern.Character43);
        ('\u0044',Pattern.Character44);
        ('\u0045',Pattern.Character45);
        ('\u0046',Pattern.Character46);
        ('\u0047',Pattern.Character47);
        ('\u0048',Pattern.Character48);
        ('\u0049',Pattern.Character49);
        ('\u004A',Pattern.Character4A);
        ('\u004B',Pattern.Character4B);
        ('\u004C',Pattern.Character4C);
        ('\u004D',Pattern.Character4D);
        ('\u004E',Pattern.Character4E);
        ('\u004F',Pattern.Character4F);

        ('\u0050',Pattern.Character50);
        ('\u0051',Pattern.Character51);
        ('\u0052',Pattern.Character52);
        ('\u0053',Pattern.Character53);
        ('\u0054',Pattern.Character54);
        ('\u0055',Pattern.Character55);
        ('\u0056',Pattern.Character56);
        ('\u0057',Pattern.Character57);
        ('\u0058',Pattern.Character58);
        ('\u0059',Pattern.Character59);
        ('\u005A',Pattern.Character5A);
        ('\u005B',Pattern.Character5B);
        ('\u005C',Pattern.Character5C);
        ('\u005D',Pattern.Character5D);
        ('\u005E',Pattern.Character5E);
        ('\u005F',Pattern.Character5F);

        ('\u0060',Pattern.Character60);
        ('\u0061',Pattern.Character61);
        ('\u0062',Pattern.Character62);
        ('\u0063',Pattern.Character63);
        ('\u0064',Pattern.Character64);
        ('\u0065',Pattern.Character65);
        ('\u0066',Pattern.Character66);
        ('\u0067',Pattern.Character67);
        ('\u0068',Pattern.Character68);
        ('\u0069',Pattern.Character69);
        ('\u006A',Pattern.Character6A);
        ('\u006B',Pattern.Character6B);
        ('\u006C',Pattern.Character6C);
        ('\u006D',Pattern.Character6D);
        ('\u006E',Pattern.Character6E);
        ('\u006F',Pattern.Character6F);

        ('\u0070',Pattern.Character70);
        ('\u0071',Pattern.Character71);
        ('\u0072',Pattern.Character72);
        ('\u0073',Pattern.Character73);
        ('\u0074',Pattern.Character74);
        ('\u0075',Pattern.Character75);
        ('\u0076',Pattern.Character76);
        ('\u0077',Pattern.Character77);
        ('\u0078',Pattern.Character78);
        ('\u0079',Pattern.Character79);
        ('\u007A',Pattern.Character7A);
        ('\u007B',Pattern.Character7B);
        ('\u007C',Pattern.Character7C);
        ('\u007D',Pattern.Character7D);
        ('\u007E',Pattern.Character7E);
        ('\u007F',Pattern.Character7F);

        ('\u0080',Pattern.Character80);
        ('\u0081',Pattern.Character81);
        ('\u0082',Pattern.Character82);
        ('\u0083',Pattern.Character83);
        ('\u0084',Pattern.Character84);
        ('\u0085',Pattern.Character85);
        ('\u0086',Pattern.Character86);
        ('\u0087',Pattern.Character87);
        ('\u0088',Pattern.Character88);
        ('\u0089',Pattern.Character89);
        ('\u008A',Pattern.Character8A);
        ('\u008B',Pattern.Character8B);
        ('\u008C',Pattern.Character8C);
        ('\u008D',Pattern.Character8D);
        ('\u008E',Pattern.Character8E);
        ('\u008F',Pattern.Character8F);

        ('\u0090',Pattern.Character90);
        ('\u0091',Pattern.Character91);
        ('\u0092',Pattern.Character92);
        ('\u0093',Pattern.Character93);
        ('\u0094',Pattern.Character94);
        ('\u0095',Pattern.Character95);
        ('\u0096',Pattern.Character96);
        ('\u0097',Pattern.Character97);
        ('\u0098',Pattern.Character98);
        ('\u0099',Pattern.Character99);
        ('\u009A',Pattern.Character9A);
        ('\u009B',Pattern.Character9B);
        ('\u009C',Pattern.Character9C);
        ('\u009D',Pattern.Character9D);
        ('\u009E',Pattern.Character9E);
        ('\u009F',Pattern.Character9F);

        ('\u00A0',Pattern.CharacterA0);
        ('\u00A1',Pattern.CharacterA1);
        ('\u00A2',Pattern.CharacterA2);
        ('\u00A3',Pattern.CharacterA3);
        ('\u00A4',Pattern.CharacterA4);
        ('\u00A5',Pattern.CharacterA5);
        ('\u00A6',Pattern.CharacterA6);
        ('\u00A7',Pattern.CharacterA7);
        ('\u00A8',Pattern.CharacterA8);
        ('\u00A9',Pattern.CharacterA9);
        ('\u00AA',Pattern.CharacterAA);
        ('\u00AB',Pattern.CharacterAB);
        ('\u00AC',Pattern.CharacterAC);
        ('\u00AD',Pattern.CharacterAD);
        ('\u00AE',Pattern.CharacterAE);
        ('\u00AF',Pattern.CharacterAF);

        ('\u00B0',Pattern.CharacterB0);
        ('\u00B1',Pattern.CharacterB1);
        ('\u00B2',Pattern.CharacterB2);
        ('\u00B3',Pattern.CharacterB3);
        ('\u00B4',Pattern.CharacterB4);
        ('\u00B5',Pattern.CharacterB5);
        ('\u00B6',Pattern.CharacterB6);
        ('\u00B7',Pattern.CharacterB7);
        ('\u00B8',Pattern.CharacterB8);
        ('\u00B9',Pattern.CharacterB9);
        ('\u00BA',Pattern.CharacterBA);
        ('\u00BB',Pattern.CharacterBB);
        ('\u00BC',Pattern.CharacterBC);
        ('\u00BD',Pattern.CharacterBD);
        ('\u00BE',Pattern.CharacterBE);
        ('\u00BF',Pattern.CharacterBF);

        ('\u00C0',Pattern.CharacterC0);
        ('\u00C1',Pattern.CharacterC1);
        ('\u00C2',Pattern.CharacterC2);
        ('\u00C3',Pattern.CharacterC3);
        ('\u00C4',Pattern.CharacterC4);
        ('\u00C5',Pattern.CharacterC5);
        ('\u00C6',Pattern.CharacterC6);
        ('\u00C7',Pattern.CharacterC7);
        ('\u00C8',Pattern.CharacterC8);
        ('\u00C9',Pattern.CharacterC9);
        ('\u00CA',Pattern.CharacterCA);
        ('\u00CB',Pattern.CharacterCB);
        ('\u00CC',Pattern.CharacterCC);
        ('\u00CD',Pattern.CharacterCD);
        ('\u00CE',Pattern.CharacterCE);
        ('\u00CF',Pattern.CharacterCF);

        ('\u00D0',Pattern.CharacterD0);
        ('\u00D1',Pattern.CharacterD1);
        ('\u00D2',Pattern.CharacterD2);
        ('\u00D3',Pattern.CharacterD3);
        ('\u00D4',Pattern.CharacterD4);
        ('\u00D5',Pattern.CharacterD5);
        ('\u00D6',Pattern.CharacterD6);
        ('\u00D7',Pattern.CharacterD7);
        ('\u00D8',Pattern.CharacterD8);
        ('\u00D9',Pattern.CharacterD9);
        ('\u00DA',Pattern.CharacterDA);
        ('\u00DB',Pattern.CharacterDB);
        ('\u00DC',Pattern.CharacterDC);
        ('\u00DD',Pattern.CharacterDD);
        ('\u00DE',Pattern.CharacterDE);
        ('\u00DF',Pattern.CharacterDF);

        ('\u00E0',Pattern.CharacterE0);
        ('\u00E1',Pattern.CharacterE1);
        ('\u00E2',Pattern.CharacterE2);
        ('\u00E3',Pattern.CharacterE3);
        ('\u00E4',Pattern.CharacterE4);
        ('\u00E5',Pattern.CharacterE5);
        ('\u00E6',Pattern.CharacterE6);
        ('\u00E7',Pattern.CharacterE7);
        ('\u00E8',Pattern.CharacterE8);
        ('\u00E9',Pattern.CharacterE9);
        ('\u00EA',Pattern.CharacterEA);
        ('\u00EB',Pattern.CharacterEB);
        ('\u00EC',Pattern.CharacterEC);
        ('\u00ED',Pattern.CharacterED);
        ('\u00EE',Pattern.CharacterEE);
        ('\u00EF',Pattern.CharacterEF);

        ('\u00F0',Pattern.CharacterF0);
        ('\u00F1',Pattern.CharacterF1);
        ('\u00F2',Pattern.CharacterF2);
        ('\u00F3',Pattern.CharacterF3);
        ('\u00F4',Pattern.CharacterF4);
        ('\u00F5',Pattern.CharacterF5);
        ('\u00F6',Pattern.CharacterF6);
        ('\u00F7',Pattern.CharacterF7);
        ('\u00F8',Pattern.CharacterF8);
        ('\u00F9',Pattern.CharacterF9);
        ('\u00FA',Pattern.CharacterFA);
        ('\u00FB',Pattern.CharacterFB);
        ('\u00FC',Pattern.CharacterFC);
        ('\u00FD',Pattern.CharacterFD);
        ('\u00FE',Pattern.CharacterFE);
        ('\u00FF',Pattern.CharacterFF)]
        |> Map.ofList

    let render (context: RenderingContext) (scale:int) (x:int,y:int) (hue:Hue) (text:string) : unit =
        text.ToCharArray()
        |> Array.fold(fun nx ch -> 
                        table
                        |> Map.tryFind ch
                        |> Option.iter (fun p -> PatternRenderer.render context scale (nx,y) hue p)
                        (nx + Constants.PatternWidth)) x
        |> ignore
        ()

module PlacardRenderer =
    let private addPositions (x1:int,y1:int) (x2:int,y2:int) : int*int =
        (x1+x2,y1+y2)

    let rec render (context: RenderingContext) (scale:int) (position:int*int) (placard:Placard) : unit =
        match placard with
        | Placard.Conditional (f, p) -> if f then render context scale position p
        | Placard.Rectangle r ->
            RectangleRenderer.render context scale (r.position |> addPositions position) r.hue r.size
        | Placard.Pattern p ->
            PatternRenderer.render context scale (p.position |> addPositions position) p.hue p.pattern
        | Placard.Text t ->
            TextRenderer.render context scale (t.position |> addPositions position) t.hue t.text
        | Placard.Scale (s,i) ->
            render context s position i
        | Placard.Group g ->
            g |> List.iter (render context scale position)
        | Placard.Offset (p,i) ->
            render context scale (p |> addPositions position) i


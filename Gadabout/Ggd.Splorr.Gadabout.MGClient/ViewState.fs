﻿namespace Ggd.Splorr.Gadabout.MGClient

open Ggd.Splorr.Gadabout

module private DisplayConstants =

    let ZoomFactor=(4)
    let BaseWidth=(208)
    let BaseHeight=(240)
    let ScreenWidth=(BaseWidth * ZoomFactor)
    let ScreenHeight=(BaseHeight * ZoomFactor)
    let ScaleSmall=(ScreenWidth/BaseWidth)
    let ScaleLarge=(ScaleSmall * 2)
    let ScaleWidth(s)= (ScreenWidth/(s))
    let ScaleHeight(s)= (ScreenHeight/(s))
    let scaleSize(s) = (ScaleWidth(s),ScaleHeight(s))

module ViewState =
    open System
    open Microsoft.Xna.Framework.Input

    let private renderDead (world:World) (context:RenderingContext) : unit =
        [{PlacardRectangle.hue = Hue.DarkCarnelian;position=(0,0);size=DisplayConstants.scaleSize(DisplayConstants.ScaleSmall)} |> Placard.Rectangle;
        {PlacardRectangle.hue = Hue.DarkerSilver;position=Constants.patternPosition(10,1);size=Constants.patternSize(7,1)} |> Placard.Rectangle;
        {PlacardText.position=Constants.patternPosition(0 ,0); hue=Hue.Onyx; text="Yer Dead!"}|> Placard.Text;
        {PlacardText.position=Constants.patternPosition(0 ,1); hue=Hue.Silver; text="Press F10/        Button"}|> Placard.Text;
        {PlacardText.position=Constants.patternPosition(0 ,2); hue=Hue.Silver; text="to respawn"}|> Placard.Text;
        {PlacardText.position=Constants.patternPosition(10,1); hue=Hue.DarkSilver; text="(Start)"}|> Placard.Text;
        ]
        |> Placard.Group
        |> PlacardRenderer.render context DisplayConstants.ScaleSmall (0,0)

    let private renderTitle (context:RenderingContext) : unit =
        [{PlacardRectangle.hue = Hue.DarkTurquoise;position=(0,0);size=DisplayConstants.scaleSize(DisplayConstants.ScaleSmall)} |> Placard.Rectangle;
        {PlacardRectangle.hue = Hue.Jade;position=Constants.patternPosition(8,0);size=Constants.patternSize(3,1)} |> Placard.Rectangle;
        {PlacardText.position=(0,0); hue=Hue.Silver; text="Press E/    Button"}|> Placard.Text;
        {PlacardText.position=Constants.patternPosition(8,0); hue=Hue.DarkerJade; text="(A)"}|> Placard.Text;
        ]
        |> Placard.Group
        |> PlacardRenderer.render context DisplayConstants.ScaleSmall (0,0)

    let private keyboardCommands =
        [
            (Keys.Up, Command.Up);
            (Keys.Down, Command.Down);
            (Keys.Left, Command.Left);
            (Keys.Right, Command.Right);
            (Keys.OemComma, Command.Previous);
            (Keys.OemPeriod, Command.Next);
            (Keys.Escape, Command.Back);
            (Keys.F10, Command.Start);
            (Keys.F, Command.Blue);
            (Keys.E, Command.Green);
            (Keys.C, Command.Yellow);
            (Keys.Z, Command.Red);
            (Keys.W, Command.Up);
            (Keys.A, Command.Left);
            (Keys.S, Command.Down);
            (Keys.D, Command.Right)
        ]

    let private addKeyboardCommandCounts (keyboardState:KeyboardState) (counts:Map<Command,uint32>) : Map<Command,uint32> =
        keyboardCommands
        |> List.fold (fun acc (k,c) -> 
                        match keyboardState.IsKeyDown(k), acc |> Map.tryFind c with
                        | true, None -> acc |> Map.add c 1u
                        | true, Some x -> acc |> Map.add c (x+1u)
                        |_ -> acc) counts

    let private gamePadCommands : Map<Command, (GamePadState -> bool)>=
        [
            (Command.Up, fun (gp:GamePadState) -> (gp.DPad.Up = ButtonState.Pressed) || (gp.ThumbSticks.Left.Y>0.5f))
            (Command.Down, fun (gp:GamePadState) -> (gp.DPad.Down = ButtonState.Pressed) || (gp.ThumbSticks.Left.Y<(-0.5f)))
            (Command.Left, fun (gp:GamePadState) -> (gp.DPad.Left = ButtonState.Pressed) || (gp.ThumbSticks.Left.X<(-0.5f)))
            (Command.Right, fun (gp:GamePadState) -> (gp.DPad.Right = ButtonState.Pressed) || (gp.ThumbSticks.Left.X>0.5f))

            (Command.Green, fun (gp:GamePadState) ->  (gp.Buttons.A = ButtonState.Pressed))
            (Command.Blue, fun (gp:GamePadState) ->   (gp.Buttons.X = ButtonState.Pressed))
            (Command.Red, fun (gp:GamePadState) ->    (gp.Buttons.B = ButtonState.Pressed))
            (Command.Yellow, fun (gp:GamePadState) -> (gp.Buttons.Y = ButtonState.Pressed))

            (Command.Next, fun (gp:GamePadState) ->     (gp.Buttons.RightShoulder = ButtonState.Pressed))
            (Command.Previous, fun (gp:GamePadState) -> (gp.Buttons.LeftShoulder = ButtonState.Pressed))
            (Command.Back, fun (gp:GamePadState) ->     (gp.Buttons.Back = ButtonState.Pressed))
            (Command.Start, fun (gp:GamePadState) ->    (gp.Buttons.Start = ButtonState.Pressed))
        ]
        |> Map.ofList

    let private addGamePadCommandCounts (gamePadState:GamePadState) (counts:Map<Command,uint32>) : Map<Command,uint32> =
        gamePadCommands
        |> Map.fold (fun acc k v -> 
                        match gamePadState |> v, acc |> Map.tryFind k with
                        | true, None -> acc |> Map.add k 1u
                        | true, Some x -> acc |> Map.add k (x+1u)
                        | _ -> acc) counts

    let getCommandCounts (keyboardState:KeyboardState) (gamePadState:GamePadState) : Map<Command, uint32> =
        Map.empty
        |> addKeyboardCommandCounts keyboardState
        |> addGamePadCommandCounts gamePadState

    let updateCommands (timeSpan:TimeSpan) (keyboardStates:KeyboardState*KeyboardState) (gamePadStates:GamePadState*GamePadState) (commands:Map<Command,bool*TimeSpan>) : Map<Command,bool*TimeSpan> =
        commands

    let private renderAtlasCell (scale:int) (screenPosition:int*int) (cell:AtlasCell) (context:RenderingContext) : unit =
        //render terrain
        let bg,fg,pat = cell.terrain.pattern
        PatternRenderer.render context scale screenPosition bg Pattern.Solid
        PatternRenderer.render context scale screenPosition fg pat
        //render item
        if cell.items.visibleTable.IsEmpty |> not then
            let item =
                cell.items.visibleTable
                |> Map.toList
                |> List.head
                |> fst
            PatternRenderer.render context scale screenPosition item.hue item.pattern
        //render creature
        cell.creature
        |> Option.iter (fun c -> 
                        let hue, pat = c.getPattern
                        PatternRenderer.render context scale screenPosition hue pat)

    let private renderAtlas (screenPosition:int*int) (world:World) (context:RenderingContext) : unit =
        let scale = DisplayConstants.ScaleLarge
        let offsetX = world.avatar.sight * Constants.PatternWidth + (screenPosition|>fst)
        let offsetY = world.avatar.sight * Constants.PatternHeight + (screenPosition|>snd)
        [-world.avatar.sight..world.avatar.sight]
        |> List.iter (fun column -> 
                        [-world.avatar.sight..world.avatar.sight]
                        |> List.iter (fun row -> 
                                        let worldX  = column  + (world.avatar.getX)
                                        let worldY  = row     + (world.avatar.getY)
                                        let screenX = offsetX + column * Constants.PatternWidth
                                        let screenY = offsetY + row * Constants.PatternHeight

                                        match world.tryGetAtlasCell (worldX,worldY) with
                                        | None      -> ()
                                        | Some cell -> renderAtlasCell scale (screenX,screenY) cell context))
        let cursorPosition =
            match world.avatar.facing with
            | Direction.North -> (offsetX,offsetY - Constants.PatternHeight)
            | Direction.South -> (offsetX,offsetY + Constants.PatternHeight)
            | Direction.East -> (offsetX+Constants.PatternWidth,offsetY)
            | Direction.West -> (offsetX-Constants.PatternWidth,offsetY)
        PatternRenderer.render context scale cursorPosition Hue.Turquoise Pattern.Selector

    let private renderMessages (screenPosition:int*int) (world:World) (context:RenderingContext) : unit =
        (screenPosition, world.messages)
        ||> List.fold (fun (x,y) (h,s) -> 
                        TextRenderer.render context DisplayConstants.ScaleSmall (x+1,y+1) Hue.Onyx s
                        TextRenderer.render context DisplayConstants.ScaleSmall (x,y) h s
                        (x,y-Constants.PatternHeight))
        |> ignore

    let private renderVerbCaption (x:int,y:int) (world:World) (context:RenderingContext) : unit =
        let text =
            (world.avatar.verb
            |> VerbDescriptor.getDescriptor).caption
        let hue = 
            if world.getAvatarCreature.canDoVerb (world.getAvatarInteractAtlasCell.terrain,world.getAvatarAtlasCell.terrain) (world.avatar.verb |> VerbDescriptor.getDescriptor) then
                Hue.Light
            else
                Hue.DarkCarnelian
        text
        |> TextRenderer.render context DisplayConstants.ScaleSmall (x-text.Length * Constants.PatternWidth/2,y) hue
             

    let private renderVerbs (screenPosition:int*int) (world:World) (context:RenderingContext) : unit =
        let verbs = 
            (world
            |> Game.getAvatarCreature).getVerbs  ((world.getAvatarInteractAtlasCell).terrain, (world.getAvatarAtlasCell).terrain) (VerbDescriptor.getVerbs())
            |> List.sortBy (fun (_,x) -> x.ordinal)
        let ordinal = 
            (world.avatar.verb
            |> VerbDescriptor.getDescriptor).ordinal
        let position =
            verbs
            |> List.fold (fun (x,y) (_,v) -> if v.ordinal < ordinal then (x-Constants.PatternWidth,y) else (x,y)) screenPosition

        let hue = 
            if world.getAvatarCreature.canDoVerb (world.getAvatarInteractAtlasCell.terrain,world.getAvatarAtlasCell.terrain) (world.avatar.verb |> VerbDescriptor.getDescriptor) then
                Hue.Light
            else
                Hue.DarkCarnelian
        PatternRenderer.render context DisplayConstants.ScaleLarge screenPosition hue Pattern.Selector
        verbs
        |> List.fold (fun (x,y) (_,descriptor) -> 
                        PatternRenderer.render context DisplayConstants.ScaleLarge (x,y) descriptor.hue descriptor.pattern
                        (x+Constants.PatternWidth,y)) position
        |> ignore
    
    let private renderPlay (world:World) (context:RenderingContext) : unit =
        (world, context) ||> renderAtlas (0,Constants.PatternHeight/2)
        (world, context) ||> renderVerbs (DisplayConstants.ScaleWidth(DisplayConstants.ScaleLarge)/2-Constants.PatternWidth/2, DisplayConstants.ScaleHeight(DisplayConstants.ScaleLarge)-Constants.PatternWidth)
        (world, context) ||> renderMessages (0, DisplayConstants.ScaleHeight(DisplayConstants.ScaleSmall)-Constants.PatternHeight * 4)
        (world, context) ||> renderVerbCaption (DisplayConstants.ScaleWidth(DisplayConstants.ScaleSmall)/2-Constants.PatternWidth/2, DisplayConstants.ScaleHeight(DisplayConstants.ScaleSmall)-Constants.PatternWidth*3)

        [{PlacardRectangle.hue = Hue.DarkerSilver;position=(0,0);size=(DisplayConstants.ScaleWidth(DisplayConstants.ScaleSmall),Constants.PatternWidth)} |> Placard.Rectangle;
        {PlacardPattern.position=(0,0); hue=Hue.Turquoise; pattern=Pattern.Energy} |> Placard.Pattern;
        {PlacardText.position=(Constants.PatternWidth,0); hue=Hue.Turquoise; text=world |> Game.getAvatarStatistic (Some 0.0,Some 100.0) Statistic.Energy |> sprintf "%3u"}|> Placard.Text;
        {PlacardPattern.position=(5 * Constants.PatternWidth,0); hue=Hue.Carnelian; pattern=Pattern.Character03} |> Placard.Pattern;
        {PlacardText.position=(6 * Constants.PatternWidth,0); hue=Hue.Carnelian; text=world |> Game.getAvatarStatistic (Some 0.0,Some 100.0) Statistic.Health |> sprintf "%3u"}|> Placard.Text;
        {PlacardPattern.position=(10 * Constants.PatternWidth,0); hue=Hue.Amethyst; pattern=Pattern.Stomach} |> Placard.Pattern;
        {PlacardText.position=(11 * Constants.PatternWidth,0); hue=Hue.Amethyst; text=world |> Game.getAvatarStatistic (Some 0.0,Some 100.0) Statistic.Hunger |> sprintf "%3u"}|> Placard.Text]
        |> Placard.Group
        |> PlacardRenderer.render context DisplayConstants.ScaleSmall (0,0)

    let private renderInventory (contextMode:InventoryContextMode, scrollers:Map<InventoryContextMode,Scroller>) (world:World) (context:RenderingContext) : unit =
        let onGroundOffsetX = DisplayConstants.BaseWidth/2
        let inHandScroller = scrollers.[InventoryContextMode.InHand]
        let onGroundScroller = scrollers.[InventoryContextMode.OnGround]
        [{PlacardRectangle.hue = Hue.DarkerSilver;position=(0,0);size=(DisplayConstants.ScaleWidth(DisplayConstants.ScaleSmall),DisplayConstants.ScaleHeight(DisplayConstants.ScaleSmall))} |> Placard.Rectangle;
        {PlacardText.position=(0,0); hue=Hue.Silver; text="Inventory:"}|> Placard.Text;
        {PlacardText.position=(onGroundOffsetX,0); hue=Hue.Silver; text="On Ground:"}|> Placard.Text;
        (inHandScroller.itemCount=0u,{PlacardText.position=(0,Constants.PatternHeight); hue=Hue.Silver; text="(nothing)"}|> Placard.Text) |> Placard.Conditional;
        (onGroundScroller.itemCount=0u,{PlacardText.position=(onGroundOffsetX,Constants.PatternHeight); hue=Hue.Silver; text="(nothing)"}|> Placard.Text) |> Placard.Conditional;
        ]
        |> Placard.Group
        |> PlacardRenderer.render context DisplayConstants.ScaleSmall (0,0)

        let renderItems (x:int) (flag:bool) (scroller:Scroller) (inventory:Inventory) =
            inventory.visibleTable
            |> Map.fold (fun (p:Placard list,n) k v -> 
                            if v>0u && (scroller.isIndexVisible n) then
                                ( p |> List.append [
                                                        {PlacardPattern.hue=Hue.Onyx; pattern=Pattern.Solid; position=(x,Constants.PatternHeight+(n|>int)*Constants.PatternHeight)}|>Placard.Pattern;
                                                        (n=scroller.index && (flag),{PlacardPattern.hue=Hue.Lightest; pattern=Pattern.Box; position=(x,Constants.PatternHeight+(n|>int)*Constants.PatternHeight)}|>Placard.Pattern)|>Placard.Conditional;
                                                        (n=scroller.index && (flag),{PlacardText.hue=Hue.Lightest; text=k.description; position=(0,29 * Constants.PatternHeight)}|>Placard.Text)|>Placard.Conditional;
                                                        (n=scroller.index && (flag|>not),{PlacardPattern.hue=Hue.Darkest; pattern=Pattern.Box; position=(x,Constants.PatternHeight+(n|>int)*Constants.PatternHeight)}|>Placard.Pattern)|>Placard.Conditional;
                                                        {PlacardPattern.hue=k.hue; pattern=k.pattern; position=(x,Constants.PatternHeight+(n|>int)*Constants.PatternHeight)}|>Placard.Pattern;
                                                        {PlacardText.hue=Hue.Silver; text=sprintf "x%u" v; position=(x+Constants.PatternWidth,Constants.PatternHeight+(n|>int)*Constants.PatternHeight)} |> Placard.Text;
                                                   ],n+1u)
                            else
                                (p,n)) ([],0u)
            |> fst
            |> Placard.Group
            |> PlacardRenderer.render context DisplayConstants.ScaleSmall (0,0)

        world.getAvatarCreature.getTagonDescriptor.inventory
        |> renderItems 0 (contextMode=InventoryContextMode.InHand) inHandScroller

        world.getAvatarAtlasCell.items
        |> renderItems onGroundOffsetX (contextMode=InventoryContextMode.OnGround) onGroundScroller

    let renderCrafting (scroller:Scroller) (world:World)  (context:RenderingContext): unit =
        let inventoryOffsetX = DisplayConstants.BaseWidth - Constants.PatternWidth*4
        let terrain =
            (world.getAvatarInteractAtlasCell).terrain
        [{PlacardRectangle.hue = Hue.DarkAmethyst;position=(0,0);size=(DisplayConstants.ScaleWidth(DisplayConstants.ScaleSmall),DisplayConstants.ScaleHeight(DisplayConstants.ScaleSmall))} |> Placard.Rectangle;
        {PlacardText.position=(0,0); hue=Hue.Silver; text=terrain.craftingCaption}|> Placard.Text;
        {PlacardText.position=(inventoryOffsetX,0); hue=Hue.Silver; text="Have"}|> Placard.Text;
        (scroller.itemCount=0u,{PlacardText.position=(0,Constants.PatternHeight); hue=Hue.Silver; text=terrain.noCraftableItemsText}|> Placard.Text) |> Placard.Conditional;
        ]
        |> Placard.Group
        |> PlacardRenderer.render context DisplayConstants.ScaleSmall (0,0)

        let inventory = 
            ((world.getAvatarCreature).getTagonDescriptor).getInventory
        let renderText = TextRenderer.render context DisplayConstants.ScaleSmall
        let renderPattern = PatternRenderer.render context DisplayConstants.ScaleSmall

        let renderRecipeItem (craftable:bool) (hilite:bool) (index:uint32) (y:int) (x:int) (k:Item) (v:uint32) : int =
            renderPattern (x,y) Hue.Onyx Pattern.Solid
            if hilite && index=scroller.index then
                renderPattern (x,y) Hue.Lightest Pattern.Box
            renderPattern (x,y) k.hue k.pattern
            let text = sprintf "x%d" v
            renderText (x+Constants.PatternWidth,y) Hue.Silver text
            (x + Constants.PatternWidth * (text.Length+1))
        
        ((0u,Constants.PatternHeight), Recipe.getVisibleRecipes terrain inventory)
        ||> List.fold (fun (index,y) r -> 
                        if scroller.isIndexVisible index then
                            //draw output
                            let x =
                                (0,r.output.visibleTable)
                                ||> Map.fold (renderRecipeItem (r.isCraftable terrain inventory) true index y)
                            //draw arrow
                            renderPattern (x,y) (if r.isCraftable terrain inventory then Hue.Turquoise else Hue.Carnelian) Pattern.Character1B
                            //draw input
                            (x+Constants.PatternWidth,r.input.visibleTable)
                            ||> Map.fold (renderRecipeItem (r.isCraftable terrain inventory) false index y)
                            |> ignore
                            if index=scroller.index then
                                //description at bottom of screen
                                renderText (Constants.patternPosition(0,29)) Hue.Lightest r.description
                                let items = 
                                    (Set.empty, r.input.visibleTable)
                                    ||> Map.fold (fun acc k _ -> acc |> Set.add k)
                                let items =
                                    (items, r.output.visibleTable)
                                    ||> Map.fold (fun acc k _ -> acc |> Set.add k)
                                //draw "have" area
                                (Constants.PatternHeight, items)
                                ||> Set.fold(fun y k ->
                                                renderPattern (inventoryOffsetX,y) Hue.Onyx Pattern.Solid
                                                renderPattern (inventoryOffsetX,y) k.hue k.pattern

                                                match inventory.visibleTable |> Map.tryFind k with
                                                | None -> " x0"
                                                | Some x when x<100u -> sprintf " x%d" x
                                                | Some x -> " >99"
                                                |> renderText (inventoryOffsetX,y) Hue.Silver
                                                (y+Constants.PatternHeight))
                                |> ignore
                                ()
                            (index+1u,y+Constants.PatternHeight)
                        else
                            (index+1u,y))
        |> ignore

    let render (context:RenderingContext) (world:World option) : unit =
        match world with
        | None -> context |> renderTitle
        | Some w -> 
            match w.avatar.commandMode with
            | CommandMode.Dead -> 
                context |> renderDead w
            | CommandMode.Navigation -> 
                context |> renderPlay w
            | CommandMode.Inventory (mode,scrollers) ->
                context |> renderInventory (mode,scrollers) w
            | CommandMode.Crafting (scroller) ->
                context
                |> renderCrafting scroller w


﻿open Ggd.Splorr.Gadabout
open System.IO
open System
open Ggd.Splorr.Gadabout
open System.Drawing
open System.Drawing.Imaging
open Ggd.Splorr.Gadabout.MGClient

let terrainList =
    [Terrain.Clear Grass;
    Terrain.Clear Rubble;
    Terrain.Clear (Dirt 1u);
    Terrain.Clear Stump;
    Terrain.Tree (1u,1u);
    Terrain.Rock 1u;
    Terrain.Flintrock 1u;
    Terrain.Water (1u,1u);
    Terrain.Seed (Seed.Berry, 1u);
    Terrain.Seed (Seed.Tree, 1u);
    Terrain.OreRock (Item.Ore Copper,1u);
    Terrain.OreRock (Item.Ore Tin,1u);
    Terrain.OreRock (Item.Ore Iron,1u);
    Terrain.OreRock (Item.Ore Mithril,1u);
    Terrain.OreRock (Item.Ore Unobtainium,1u);
    Terrain.Bush {timers=Map.empty;berry=Berry.Amberry};
    Terrain.DeadBush;
    Terrain.Fixture (Item.CraftingBox CraftingMethod.Workbench);
    Terrain.Fixture (Item.CraftingBox CraftingMethod.Furnace);
    Terrain.Fixture (Item.CraftingBox CraftingMethod.Altar);
    Terrain.Spawner (Rabbit,1u);
    Terrain.Wall WallMaterial.Wood;
    Terrain.Wall WallMaterial.Stone;
    Terrain.Door true;
    Terrain.Door false]

let lowerCaseNoCaps (s:string) : string =
    String.Join("",s.ToLower().Split(' '))

let itemToImage (path:string) (item:Item) : string =
    sprintf "![Onyx%s%s](%simages/Onyx%s%s.png)" (item.hue.ToString()) (item.pattern.ToString()) path (item.hue.ToString()) (item.pattern.ToString())

let terrainToImage (path:string) (terrain:Terrain) : string =
    let b, f, p = terrain.pattern
    sprintf "![%s%s%s](%simages/%s%s%s.png)" (b.ToString()) (f.ToString()) (p.ToString()) path (b.ToString()) (f.ToString()) (p.ToString())

let verbToImage (path:string) (descriptor:VerbDescriptor) : string =
    sprintf "![Onyx%s%s](%simages/Onyx%s%s.png)" (descriptor.hue.ToString()) (descriptor.pattern.ToString()) path (descriptor.hue.ToString()) (descriptor.pattern.ToString())


let documentRecipeGroup (documentRoot:string) (recipeName:string) (recipes: Recipe list) : unit =
    use writer = File.CreateText(Path.Combine(documentRoot,sprintf "%s.md" (recipeName |> lowerCaseNoCaps)))
    writer.WriteLine(sprintf "# %s" recipeName)
    recipes
    |> List.iter 
        (fun recipe ->
            writer.WriteLine("## Inputs")
            writer.WriteLine("| Item | Image | Count |")
            writer.WriteLine("| --- | --- | --- |")
            recipe.input.table
            |> Map.iter 
                (fun k v -> 
                    let image = itemToImage "../" k
                    writer.WriteLine(sprintf "| [%s](items/%s) | %s | %d |" k.name (k.name |> lowerCaseNoCaps) image v))
            writer.WriteLine("## Outputs")
            writer.WriteLine("| Item | Image | Count |")
            writer.WriteLine("| --- | --- | --- |")
            recipe.output.table
            |> Map.iter 
                (fun k v -> 
                    let image = itemToImage "../" k
                    writer.WriteLine(sprintf "| [%s](items/%s) | %s | %d |" k.name (k.name |> lowerCaseNoCaps) image v))
            writer.WriteLine("## Crafted On")
            writer.WriteLine()
            writer.WriteLine("| Terrain | Image |")
            writer.WriteLine("| --- | --- |")
            let terrains = 
                terrainList
                |> List.filter (recipe.canCraftOnTerrain)
                |> List.map (fun t -> 
                    let image = terrainToImage "../" t
                    sprintf "| [%s](terrains/%s) | %s |" t.name (t.name |> lowerCaseNoCaps) image)
                |> List.sort
            writer.WriteLine(String.Join(", ", terrains))
            writer.WriteLine("---"))

let documentRecipes (documentRoot:string) : unit =
    use writer = File.CreateText(Path.Combine(documentRoot,"recipelist.md"))
    writer.WriteLine(sprintf "# Recipe List")
    (Map.empty, Recipe.recipes)
    ||> List.fold 
        (fun buckets recipe -> 
            if buckets.ContainsKey recipe.name then
                buckets 
                |> Map.add recipe.name (recipe :: buckets.[recipe.name])
            else
                buckets 
                |> Map.add recipe.name [recipe])
    |> Map.fold (fun acc k v -> 
        (documentRecipeGroup (Path.Combine(documentRoot,"recipes")) k v)
        acc
        |> List.append [sprintf "*  [%s](recipes/%s)" k (k |> lowerCaseNoCaps)]) List.empty
    |> List.sort
    |> List.iter (fun x -> writer.WriteLine(x))

let documentVerbGroup (documentRoot:string) (verbName:string) (descriptors: VerbDescriptor list) : unit =
    use writer = File.CreateText(Path.Combine(documentRoot,sprintf "%s.md" (verbName |> lowerCaseNoCaps)))
    writer.WriteLine(sprintf "# %s" verbName)
    descriptors
    |> List.iter
        (fun descriptor ->
            if descriptors.Length<>1 then
                writer.Write(descriptor.caption |> sprintf "# %s")
                writer.WriteLine()
            writer.WriteLine(descriptor |> verbToImage "../")
            writer.WriteLine()
            writer.WriteLine("| Property | Value |")
            writer.WriteLine("| --- | --- |")
            descriptor.item
            |> Option.iter
                (fun item ->
                    writer.WriteLine(item.name |> sprintf "| Item Used | %s |"))
            writer.WriteLine(descriptor.energy |> sprintf "| Energy Cost | %f |")
            writer.WriteLine(descriptor.exertion |> sprintf "| Exertion | %f |")
            writer.WriteLine(descriptor.verb.description |> sprintf "| Description | %s |")
            writer.Write("| Terrain | ")
            let terrains =
                terrainList
                |> List.filter descriptor.canInteract
                |> List.map (fun t -> sprintf "[%s](terrains/%s)" t.name (t.name |> lowerCaseNoCaps))
                |> List.sort
            writer.Write(String.Join(", ", terrains))
            writer.WriteLine(" |")
            writer.WriteLine("---"))

let documentVerbs (documentRoot:string) : unit =
    use writer = File.CreateText(Path.Combine(documentRoot,"verblist.md"))
    writer.WriteLine(sprintf "# Verb List")
    (Map.empty, VerbDescriptor.descriptors)
    ||> List.fold
        (fun buckets descriptor -> 
            if buckets.ContainsKey descriptor.verb.name then
                buckets
                |> Map.add descriptor.verb.name (descriptor :: buckets.[descriptor.verb.name])
            else
                buckets 
                |> Map.add descriptor.verb.name [descriptor])
    |> Map.fold (fun acc k v -> 
        (documentVerbGroup (Path.Combine(documentRoot,"verbs")) k v)
        acc
        |> List.append [sprintf "*  [%s](verbs/%s)" k (k |> lowerCaseNoCaps)]) List.empty
    |> List.sort
    |> List.iter (fun x -> writer.WriteLine(x))

let documentTerrain (documentRoot:string) (terrain: Terrain) : unit =
    use writer = File.CreateText(Path.Combine(documentRoot,sprintf "%s.md" (terrain.name |> lowerCaseNoCaps)))
    writer.WriteLine(sprintf "# %s" terrain.name)
    writer.WriteLine()
    writer.WriteLine("| Property | Value |")
    writer.WriteLine("| --- | --- |")
    writer.WriteLine(sprintf "| Image | %s |" (terrain |> terrainToImage "../"))
    writer.WriteLine(sprintf "| Can Enter | %b |" terrain.canEnter)
    writer.WriteLine()
    if VerbDescriptor.descriptors |> List.exists (fun x -> x.canInteract terrain) then
        writer.WriteLine("## Verbs Usable On This Terrain")
        writer.WriteLine()
        writer.WriteLine("| Verb | Images |")
        writer.WriteLine("| --- | --- |")
    (Set.empty, VerbDescriptor.descriptors)
    ||> List.fold
        (fun acc descriptor ->
            if descriptor.canInteract terrain then
                acc |> Set.add descriptor.verb
            else
                acc)
    |> Set.toList
    |> List.sortBy (fun x-> x.name)
    |> List.iter (fun x-> 
        let verbImages =
            VerbDescriptor.descriptors
            |> List.filter (fun descriptor -> descriptor.verb=x)
            |> List.map ((verbToImage "../") >> (sprintf " %s "))
            |> List.reduce (+)
        writer.WriteLine (sprintf "| [%s](verbs/%s) | %s |" x.name (x.name |> lowerCaseNoCaps) verbImages))
    if Recipe.recipes |> List.exists (fun x-> x.canCraftOnTerrain terrain) then 
        writer.WriteLine("## Recipes Craftable On This Terrain")
        writer.WriteLine()
        writer.WriteLine("| Recipe |")
        writer.WriteLine("| --- |")
    Recipe.recipes
    |> List.filter (fun x-> x.canCraftOnTerrain terrain)
    |> List.map (fun x-> sprintf "| [%s](recipes/%s) |" x.name (x.name |> lowerCaseNoCaps))
    |> List.distinct
    |> List.sort
    |> List.iter writer.WriteLine

let documentTerrains (documentRoot:string) : unit =
    use writer = File.CreateText(Path.Combine(documentRoot,"terrainlist.md"))
    writer.WriteLine(sprintf "# Terrain List")
    writer.WriteLine()
    writer.WriteLine("| Name | Image |")
    writer.WriteLine("| --- | --- |")
    (List.empty, terrainList)
    ||> List.fold
        (fun x terrain -> 
            documentTerrain (Path.Combine(documentRoot,"terrains")) terrain
            x
            |> List.append [terrain])
    |> List.map
        (fun x -> 
            sprintf "| [%s](terrains/%s) | %s |" x.name (x.name |> lowerCaseNoCaps) (terrainToImage "" x))
    |> List.sort
    |> List.iter writer.WriteLine


let createPng (documentRoot:string) (pattern:Pattern, foreground: Hue, background: Hue) : unit =
    let bitmap = new Bitmap(32,32, PixelFormat.Format32bppArgb)
    let data = DisplayPatterns.patterns.[pattern]
    [for y=0 to 31 do
        for x=0 to 31 do
            yield (x,y,if data.[x/4+(y/4)*8]=Microsoft.Xna.Framework.Color.White then foreground else background)]
    |> List.iter
        (fun (x,y,c) -> 
            let displayColor = DisplayColors.getColor(c)
            bitmap.SetPixel(x,y,Color.FromArgb(displayColor.A |> int, displayColor.R |> int, displayColor.G |> int, displayColor.B |> int)))
    bitmap.Save(Path.Combine(documentRoot,sprintf "%s%s%s.png" (background.ToString()) (foreground.ToString()) (pattern.ToString())), ImageFormat.Png)
        
let createPngs (documentRoot:string) : unit =
    VerbDescriptor.descriptors
    |> List.map
        (fun item ->
            (item.pattern, item.hue, Hue.Onyx))
    |> List.iter (createPng documentRoot)
    VerbDescriptor.descriptors
    |> List.filter
        (fun x -> 
            x.item.IsSome)
    |> List.map
        (fun x->
            (x.item.Value.pattern, x.item.Value.hue, Hue.Onyx))
    |> List.iter  (createPng documentRoot)
    terrainList
    |> List.map
        (fun terrain ->
            let b, f, p = terrain.pattern
            (p,f,b))
    |> List.iter  (createPng documentRoot)
    Recipe.recipes
    |> List.iter
        (fun recipe ->
            recipe.input.table
            |> Map.iter
                (fun k _ -> 
                    createPng documentRoot (k.pattern, k.hue, Hue.Onyx))
            recipe.output.table
            |> Map.iter
                (fun k _ -> 
                    createPng documentRoot (k.pattern, k.hue, Hue.Onyx)))

let documentItem (documentRoot:string) (item:Item) : unit =
    use writer = File.CreateText(Path.Combine(documentRoot,sprintf "%s.md" (item.name |> lowerCaseNoCaps)))
    writer.WriteLine(sprintf "# %s" item.name)
    writer.WriteLine()
    writer.WriteLine("| Property | Value |")
    writer.WriteLine("| --- | --- |")
    writer.WriteLine(sprintf "| Image | %s |" (item |> itemToImage "../"))
    writer.WriteLine()
    //crafting
    if Recipe.recipes |> List.exists (fun x -> x.input.table.ContainsKey(item)) then
        writer.WriteLine("## Recipes With This Item as an Input")
        writer.WriteLine()
        writer.WriteLine("| Recipe |")
        writer.WriteLine("| --- |")
    Recipe.recipes
    |> List.filter (fun x -> x.input.table.ContainsKey(item))
    |> List.map (fun x-> sprintf "| [%s](recipes/%s) |" x.name (x.name |> lowerCaseNoCaps))
    |> List.distinct
    |> List.sort
    |> List.iter writer.WriteLine
    if Recipe.recipes |> List.exists (fun x -> x.output.table.ContainsKey(item)) then
        writer.WriteLine("## Recipes With This Item as an Output")
        writer.WriteLine()
        writer.WriteLine("| Recipe |")
        writer.WriteLine("| --- |")
    Recipe.recipes
    |> List.filter (fun x -> x.output.table.ContainsKey(item))
    |> List.map (fun x-> sprintf "| [%s](recipes/%s) |" x.name (x.name |> lowerCaseNoCaps))
    |> List.distinct
    |> List.sort
    |> List.iter writer.WriteLine
    //verbs
    if VerbDescriptor.descriptors 
        |> List.exists 
            (fun x -> 
                match x.item with
                | Some i when i=item -> true
                | _ -> false) then
        writer.WriteLine("## Verbs Performed Using This Item")
        writer.WriteLine()
        writer.WriteLine("| Verb | Image |")
        writer.WriteLine("| --- | --- |")
    VerbDescriptor.descriptors
    |> List.filter
        (fun x -> 
            match x.item with
            | Some i when i=item -> true
            | _ -> false)
    |> List.map (fun x -> sprintf "| [%s](verbs/%s) | %s |" x.verb.name (x.verb.name |> lowerCaseNoCaps) (x |> verbToImage "../") )
    |> List.distinct
    |> List.sort
    |> List.iter (writer.WriteLine)

let documentItems (documentRoot:string) : unit =
    use writer = File.CreateText(Path.Combine(documentRoot,"itemlist.md"))
    writer.WriteLine(sprintf "# Item List")
    writer.WriteLine()
    writer.WriteLine("| Item | Image |")
    writer.WriteLine("| --- | --- |")
    (List.empty, ((Set.empty, VerbDescriptor.descriptors)
    ||> List.fold
        (fun acc descriptor ->
            match descriptor.item with
            | Some x -> 
                acc 
                |> Set.add x
            | _ ->
                acc), Recipe.recipes)
    ||> List.fold
        (fun acc recipe -> 
            ((acc, recipe.input.table)
            ||> Map.fold
                (fun acc2 k _ -> 
                    acc2
                    |> Set.add k), recipe.output.table)
            ||> Map.fold
                (fun acc2 k _ -> 
                    acc2
                    |> Set.add k)))
    ||> Set.fold 
        (fun acc x ->
            documentItem (Path.Combine(documentRoot,"items")) x
            acc
            |> List.append [sprintf "| [%s](items/%s) | %s |" x.name (x.name |> lowerCaseNoCaps) (x|> itemToImage "")])
    |> List.sort
    |> List.iter writer.WriteLine

[<EntryPoint>]
let main argv = 
    let documentRoot = argv.[0]
    Console.WriteLine("Images!")
    createPngs (Path.Combine(documentRoot,"images"))
    Console.WriteLine("Recipes!")
    documentRecipes documentRoot
    Console.WriteLine("Verbs!")
    documentVerbs documentRoot
    Console.WriteLine("Terrains!")
    documentTerrains documentRoot
    Console.WriteLine("Items!")
    documentItems documentRoot
    Console.WriteLine("Done!")
    0

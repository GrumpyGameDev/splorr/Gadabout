﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Carcass =
    | Fish
    | Rabbit
    | Human

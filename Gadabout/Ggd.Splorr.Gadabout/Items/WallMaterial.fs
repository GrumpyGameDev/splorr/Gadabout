﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type WallMaterial =
    | Wood
    | Stone
    member x.adjective =
        match x with
        | Wood -> "Wooden"
        | Stone -> "Stone"
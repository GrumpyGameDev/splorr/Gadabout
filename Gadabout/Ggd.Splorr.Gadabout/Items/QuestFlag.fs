﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type QuestFlag =
    | Log
    | FurTrading
    member this.name =
        match this with
        | Log -> "Log"
        | FurTrading -> "Fur Trading"




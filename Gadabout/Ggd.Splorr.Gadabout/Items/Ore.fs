﻿namespace Ggd.Splorr.Gadabout

type Ore =
    | Copper
    | Tin
    | Iron
    | Mithril
    | Unobtainium
    | Flint
    | Stone
    member this.hue =
        match this with
        | Ore.Copper      -> Hue.DarkerJade
        | Ore.Tin         -> Hue.DarkerSilver
        | Ore.Iron        -> Hue.DarkCopper
        | Ore.Mithril     -> Hue.DarkRuby
        | Ore.Unobtainium -> Hue.DarkAmethyst
        | Ore.Stone       -> Hue.DarkSilver
        | Ore.Flint       -> Hue.DarkerCopper

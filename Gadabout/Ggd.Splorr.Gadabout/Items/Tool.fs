﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Tool =
    | Axe 
    | Pick
    | Spear 
    | Shovel
    | Knife 
    | Hammer
    | Hoe 
    | Trap
    member x.pattern =
        match x with
        | Axe    -> Pattern.Axe
        | Pick   -> Pattern.PickAxe
        | Spear  -> Pattern.Spear
        | Shovel -> Pattern.Shovel
        | Knife  -> Pattern.Knife
        | Hammer -> Pattern.Hammer
        | Hoe    -> Pattern.Hoe
        | Trap   -> Pattern.Trap
    member x.noun =
        match x with
        | Axe    -> "Axe"
        | Pick   -> "Pick"
        | Spear  -> "Spear"
        | Shovel -> "Shovel"
        | Knife  -> "Knife"
        | Hammer -> "Hammer"
        | Hoe    -> "Hoe"
        | Trap   -> "Trap"





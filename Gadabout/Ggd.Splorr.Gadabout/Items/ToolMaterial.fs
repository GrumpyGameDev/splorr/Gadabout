﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type ToolMaterial =
    | Wood
    | Stone
    | Flint
    | Copper
    | Bronze
    | Iron
    | Mithril
    | Unobtanium
    member this.hue =
        match this with
        | ToolMaterial.Wood       -> Hue.DarkCopper
        | ToolMaterial.Stone      -> Hue.DarkSilver
        | ToolMaterial.Flint      -> Hue.DarkerCopper
        | ToolMaterial.Copper     -> Hue.Copper
        | ToolMaterial.Bronze     -> Hue.MediumCopper
        | ToolMaterial.Iron       -> Hue.MediumSilver
        | ToolMaterial.Mithril    -> Hue.Ruby
        | ToolMaterial.Unobtanium -> Hue.Amethyst
    member this.adjective = 
        match this with
        | ToolMaterial.Wood       -> "Wooden"
        | ToolMaterial.Stone      -> "Stone"
        | ToolMaterial.Flint      -> "Flint"
        | ToolMaterial.Copper     -> "Copper"
        | ToolMaterial.Bronze     -> "Bronze"
        | ToolMaterial.Iron       -> "Iron"
        | ToolMaterial.Mithril    -> "Mithril"
        | ToolMaterial.Unobtanium -> "Unobtanium"

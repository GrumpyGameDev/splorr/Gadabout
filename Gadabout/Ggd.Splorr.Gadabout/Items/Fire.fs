﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Fire =
    | Normal
    | Magic

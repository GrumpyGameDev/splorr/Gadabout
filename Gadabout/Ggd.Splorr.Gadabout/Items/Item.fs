﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Item =
    | Log
    | Carcass of Carcass
    | Berry of Berry
    | CraftingBox of CraftingMethod
    | Seed of Seed
    | Tool of Tool*ToolMaterial
    | Gravel
    | Wall of WallMaterial
    | Ore of Ore
    | Fire of Fire
    | Meat of bool
    | RabbitPelt
    | Door
    | Bone
    | BoneMeal
    | Ingot of ToolMaterial
    | TradeToken of Trader
    | QuestFlag of QuestFlag
    member internal this.isVisible =
        match this with
        | QuestFlag _
        | TradeToken _ -> false
        | _ -> true
    member this.isTrap =
        match this with
        | Item.Tool (Tool.Trap, _) -> true
        | _ -> false
    member private this.huePattern =
        match this with
        | Item.Log                                  -> (Hue.DarkCopper, Pattern.Log)
        | Item.Ore x                                -> (x.hue, Pattern.Rock)
        | Item.Carcass Carcass.Fish                 -> (Hue.Ruby, Pattern.Fish)
        | Item.Berry x                              -> (x.hue, Pattern.Berries)
        | Item.CraftingBox CraftingMethod.Workbench -> (Hue.DarkCopper, Pattern.Workbench)
        | Item.CraftingBox CraftingMethod.Altar     -> (Hue.DarkSilver, Pattern.Workbench)
        | Item.Tool (tool,material)                 -> (material.hue, tool.pattern)
        | Item.Seed Seed.Berry                      -> (Hue.DarkCopper, Pattern.BerrySeed)
        | Item.Seed Seed.Tree                       -> (Hue.DarkCopper, Pattern.Acorn)
        | Item.Fire Fire.Normal                     -> (Hue.Carnelian, Pattern.Fire)
        | Item.Fire Fire.Magic                      -> (Hue.Silver, Pattern.Fire)
        | Item.Gravel                               -> (Hue.DarkSilver, Pattern.Pile)
        | Item.Carcass Carcass.Rabbit               -> (Hue.DarkTurquoise, Pattern.DeadRabbit)
        | Item.Carcass Carcass.Human                -> (Hue.MediumDark, Pattern.DeadTagon)
        | Item.RabbitPelt                           -> (Hue.DarkTurquoise, Pattern.Pelt)
        | Item.Meat false                           -> (Hue.Amethyst, Pattern.Meat)
        | Item.Bone                                 -> (Hue.Silver, Pattern.Bone)
        | Item.BoneMeal                             -> (Hue.Silver, Pattern.Pile)
        | Item.Meat true                            -> (Hue.DarkAmethyst, Pattern.Meat)
        | Item.Door                                 -> (Hue.DarkCopper, Pattern.Door)
        | Item.Wall WallMaterial.Wood               -> (Hue.DarkCopper, Pattern.Wall)
        | Item.Wall WallMaterial.Stone              -> (Hue.DarkSilver, Pattern.Wall)
        | Item.CraftingBox CraftingMethod.Furnace   -> (Hue.DarkSilver, Pattern.Furnace)
        | Item.Ingot x                              -> (x.hue, Pattern.Ingot)
        | _                                         -> (Hue.Amethyst, Pattern.Character3F)
    member this.hue = this.huePattern |> fst
    member this.pattern = this.huePattern |> snd
    member this.name =
        match this with
        | Item.Log                                  -> "Log"
        | Item.Ore Stone                            -> "Stone"
        | Item.Ore Flint                            -> "Flint"
        | Item.Carcass Carcass.Fish                 -> "Fish"
        | Item.Berry x                              -> x.name
        | Item.CraftingBox CraftingMethod.Workbench -> "Workbench"
        | Item.CraftingBox CraftingMethod.Altar     -> "Altar"
                                                    
        | Item.Tool (tool, material)                -> (material.adjective, tool.noun) ||> sprintf "%s %s"
        | Item.Ingot material                       -> material.adjective |> sprintf "%s Ingot"

        | Item.Seed Seed.Berry                      -> "Berry Seed"
        | Item.Seed Seed.Tree                       -> "Acorn"

        | Item.Bone                                 -> "Bone"
        | Item.BoneMeal                             -> "Bone Meal"

        | Item.Meat true                            -> "Cooked Meat"
        | Item.Meat false                           -> "Raw Meat"
        | Item.Carcass Carcass.Rabbit               -> "Dead Rabbit"
        | Item.Carcass Carcass.Human                -> "Human Carcass"
        | Item.RabbitPelt                           -> "Rabbit Pelt"

        | Item.Wall x                               -> x.adjective |> sprintf "%s Wall"
        | Item.Door                                 -> "Door"
        | Item.Fire Fire.Normal                     -> "Fire"
        | Item.Fire Fire.Magic                      -> "Magic Fire"
        | Item.CraftingBox CraftingMethod.Furnace   -> "Furnace"
        | Item.Gravel                               -> "Gravel"

        | Item.Ore Copper                           -> "Copper Ore"
        | Item.Ore Tin                              -> "Tin Ore"
        | Item.Ore Iron                             -> "Iron Ore"
        | Item.Ore Mithril                          -> "Mithril Ore"
        | Item.Ore Unobtainium                      -> "Unobtainium Ore"

        | Item.TradeToken t                         -> sprintf "%s Trade Token" t.name
        | Item.QuestFlag f                          -> sprintf "%s Quest Flag" f.name
    member this.description =
        match this with
        | _ -> this.name

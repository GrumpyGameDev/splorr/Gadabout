﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Berry =
    | Carberry
    | Copberry
    | Golberry
    | Jadberry
    | Turberry
    | Ruberry
    | Amberry
    | Silberry
    member this.hue =
        match this with
        | Berry.Carberry -> Hue.Carnelian
        | Berry.Copberry -> Hue.Copper
        | Berry.Golberry -> Hue.Gold
        | Berry.Jadberry -> Hue.Jade
        | Berry.Turberry -> Hue.Turquoise
        | Berry.Ruberry  -> Hue.Ruby
        | Berry.Amberry  -> Hue.Amethyst
        | Berry.Silberry -> Hue.Silver
    member this.name =
        match this with
        | Berry.Carberry -> "Carberry"
        | Berry.Copberry -> "Copberry"
        | Berry.Golberry -> "Golberry"
        | Berry.Jadberry -> "Jadberry"
        | Berry.Turberry -> "Turberry"
        | Berry.Ruberry  -> "Ruberry"
        | Berry.Amberry  -> "Amberry"
        | Berry.Silberry -> "Silberry"
    member this.generatorWeight =
        match this with
        | Berry.Carberry -> 128u
        | Berry.Copberry -> 64u
        | Berry.Golberry -> 32u
        | Berry.Jadberry -> 16u
        | Berry.Turberry -> 8u
        | Berry.Ruberry  -> 4u
        | Berry.Amberry  -> 2u
        | Berry.Silberry -> 1u
    static member generator =
        [(Berry.Carberry);
        (Berry.Copberry);
        (Berry.Golberry);
        (Berry.Jadberry);
        (Berry.Turberry);
        (Berry.Ruberry);
        (Berry.Amberry);
        (Berry.Silberry)]
        |> List.map (fun x -> x, x.generatorWeight)
        |> Map.ofList

﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Seed =
    | Berry
    | Tree

﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Trader =
    | Wood
    | Fur
    member x.name =
        match x with
        | Wood -> "Wood"
        | Fur -> "Fur"
    member x.hue =
        match x with
        | Wood -> Hue.DarkCopper
        | Fur -> Hue.DarkTurquoise




﻿namespace Ggd.Splorr.Gadabout

type Inventory = 
    {table:Map<Item,uint32>}
    member instance.visibleTable =
        instance.table |> Map.filter (fun k _ -> k.isVisible)
    static member ofList (values:(Item*uint32) list) =
        {table = 
            values 
            |> List.fold 
                (fun acc (i,c) -> 
                    match c, acc.TryFind i with
                    | 0u, _ -> 
                        acc
                    | _,Some v ->
                        acc |> Map.add i (c+v)
                    | _,None ->
                        acc |> Map.add i c) Map.empty}
    member x.tryGetFirstItem () : Item option =
        x.table |> Map.tryPick (fun k v -> if v>0u then Some k else None)
    member x.getFirstItem = x.tryGetFirstItem >> Option.get
    member x.getItemCount (item:Item) : uint32 =
        match x.table |> Map.tryFind item with
        | Some count ->
            count
        | None ->
            0u
    member x.setItemCount (item:Item) (count:uint32) : Inventory =
        if count = 0u then
            {x with table = x.table |> Map.remove item}
        else
            {x with table = x.table |> Map.add item count}
    member x.addItemCount (item:Item) (count:uint32) : Inventory =
        x.setItemCount item ((x.getItemCount item)+count)
    member x.removeItemCount (item:Item) (count:uint32) : Inventory =
        match x.getItemCount item with
        | c when c > count -> 
            {x with table = x.table |> Map.add item (c-count)}
        | _ -> 
            {x with table = x.table |> Map.remove item}
    member x.tryGetScrollerItem (scroller:Scroller) : Item option =
        x.table
        |> Map.fold (fun (item:Item option, counter:uint32) k v -> 
                        match item, counter, v with
                        | Some i, _, _ -> (i |> Some, 0u)
                        | None, 0u, x when x>0u -> (k |> Some, 0u)
                        | None, x, y when y>0u -> (None, x - 1u)
                        | _ -> (item,counter)) (None, scroller.index)
        |> fst

    member x.getScrollerItem (scroller:Scroller) = 
        x.tryGetScrollerItem scroller 
        |> Option.get

    member x.getScrollableItemCount : uint32 =
        x.visibleTable
        |> Map.fold (fun acc k v -> if v > 0u then acc + 1u else acc) 0u

    member x.hasAny (needed:Inventory) : bool =
        let total =
            (0u, needed.table)
            ||> Map.fold (fun acc item count -> 
                match count, x.table |> Map.tryFind item with
                | x, Some y when y>x -> acc + x
                | _, Some y -> acc + y
                | _ -> acc)
        total > 0u     

    member x.meetsNeed (needed:Inventory) : bool =
        (true, needed.table)
        ||> Map.fold (fun acc k v ->
                        match x.table |> Map.tryFind k with
                        | Some n when v <= n -> 
                            acc && true
                        | _ -> 
                            acc && false)

    member x.hasContraband (contraband:Inventory) : bool =
        if contraband.table |> Map.isEmpty then
            false
        else
            x.meetsNeed contraband

    member x.subtract (subtrahend:Inventory) : Inventory =
        (x, subtrahend.table)
        ||> Map.fold (fun acc k v -> acc.removeItemCount k v)
    member x.add (other:Inventory) : Inventory =
        (x, other.table)
        ||> Map.fold (fun acc k v -> acc.addItemCount k v)



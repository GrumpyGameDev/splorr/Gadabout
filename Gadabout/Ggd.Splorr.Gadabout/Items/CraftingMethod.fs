﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type CraftingMethod =
    | Workbench
    | Furnace
    | Altar

﻿namespace Ggd.Splorr.Gadabout

open System

module Generator = 
    let generate (random:Random) (generator:Map<'TValue,uint32>) : 'TValue =
        (((0u,generator)
        ||> Map.fold (fun acc _ v -> acc + v)
        |> int
        |> random.Next
        |> uint32, None), generator)
        ||> Map.fold (fun (g,p) k v -> 
            match (g,p) with
            | (n, None) -> if n < v then (0u,Some k) else (n-v,None)
            | _         -> (g,p))
        |> snd
        |> Option.get


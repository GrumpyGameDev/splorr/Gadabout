﻿namespace Ggd.Splorr.Gadabout

type PlacardRectangle =
    {hue:Hue;
    position:int*int;
    size:int*int}

type PlacardPattern =
    {hue:Hue;
    pattern:Pattern;
    position:int*int}

type PlacardText =
    {hue:Hue;
    text:string;
    position:int*int}

[<RequireQualifiedAccess>]
type Placard =
    | Rectangle   of PlacardRectangle
    | Pattern     of PlacardPattern
    | Text        of PlacardText
    | Scale       of int * Placard
    | Group       of Placard list
    | Offset      of (int*int) * Placard
    | Conditional of bool * Placard


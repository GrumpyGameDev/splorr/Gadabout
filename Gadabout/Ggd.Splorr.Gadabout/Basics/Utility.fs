﻿namespace Ggd.Splorr.Gadabout

module Utility =
    let wrapValue (maximum:int) (value:int) : int =
        let result = value % maximum
        if result<0 then result+maximum else result

    let wrapColumn = wrapValue Constants.WorldColumns

    let wrapRow = wrapValue Constants.WorldRows



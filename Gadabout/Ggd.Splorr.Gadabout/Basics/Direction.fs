﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccessAttribute>]
type Direction =
    | North
    | East
    | South
    | West

module Direction =
    let nextPosition (x:int,y:int) (direction:Direction) : int*int =
        match direction with
        | Direction.North -> (x   , y-1)
        | Direction.East  -> (x+1 , y  )
        | Direction.South -> (x   , y+1)
        | Direction.West  -> (x-1 , y  )



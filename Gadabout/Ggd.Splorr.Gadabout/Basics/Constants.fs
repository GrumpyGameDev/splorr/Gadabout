﻿namespace Ggd.Splorr.Gadabout

module Constants =
    let PatternHeight=(8)
    let PatternWidth=(8)
    let patternPosition (x,y) = (x * PatternWidth, y* PatternHeight)
    let patternSize = patternPosition
    let WorldColumns = 256
    let WorldRows = 256


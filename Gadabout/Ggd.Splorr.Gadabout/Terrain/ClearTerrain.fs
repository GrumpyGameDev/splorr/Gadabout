﻿namespace Ggd.Splorr.Gadabout

type ClearTerrain =
    | Grass
    | Dirt of grassTimer:uint32
    | Stump
    | Rubble
    member instance.name =
        match instance with
        | Grass  -> "Grass"
        | Dirt _ -> "Dirt"
        | Stump  -> "Stump"
        | Rubble -> "Rubble"




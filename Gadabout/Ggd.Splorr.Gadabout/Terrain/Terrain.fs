﻿namespace Ggd.Splorr.Gadabout

open System

[<RequireQualifiedAccess>]
type Terrain =
    | Clear of ClearTerrain
    | Tree of woodLeft:uint32 * acornTimer:uint32
    | Rock of stoneLeft:uint32
    | Flintrock of flintLeft:uint32
    | Water of uint32*uint32
    | Seed of seed: Seed * growthTimer:uint32
    | OreRock of Item*uint32
    | Bush of BushDescriptor
    | DeadBush
    | Fixture of Item
    | Spawner of spawner:Spawner * spawnTimer:uint32
    | Wall of WallMaterial
    | Door of bool
    | Trader of trade: Trader
    | Quest
    member x.pattern =
        match x with
        | Terrain.Clear Grass                                         -> (Hue.Onyx,         Hue.DarkJade,     Pattern.Field)
        | Terrain.Tree      _                                         -> (Hue.Onyx,         Hue.Jade,         Pattern.Tree)
        | Terrain.Rock      _                                         -> (Hue.Onyx,         Hue.DarkSilver,   Pattern.Rock)
        | Terrain.Flintrock _                                         -> (Hue.Onyx,         Hue.DarkerCopper, Pattern.Rock)
        | Terrain.OreRock (Item.Ore Ore.Copper,_)                     -> (Hue.Onyx,         Hue.DarkerJade,   Pattern.Rock)
        | Terrain.OreRock (Item.Ore Ore.Tin,_)                        -> (Hue.Onyx,         Hue.DarkerSilver, Pattern.Rock)
        | Terrain.OreRock (Item.Ore Ore.Iron,_)                       -> (Hue.Onyx,         Hue.DarkCopper,   Pattern.Rock)
        | Terrain.OreRock (Item.Ore Ore.Mithril,_)                    -> (Hue.Onyx,         Hue.DarkRuby,     Pattern.Rock)
        | Terrain.OreRock (Item.Ore Ore.Unobtainium,_)                -> (Hue.Onyx,         Hue.DarkAmethyst, Pattern.Rock)
        | Terrain.Water     (0u,_)                                    -> (Hue.DarkRuby,     Hue.Onyx,         Pattern.Field)
        | Terrain.Water     _                                         -> (Hue.DarkRuby,     Hue.Ruby,         Pattern.Field)
        | Terrain.Seed (Seed.Berry, _)                                -> (Hue.Onyx,         Hue.DarkCopper,   Pattern.BerrySeed)
        | Terrain.Seed (Seed.Tree, _)                                 -> (Hue.Onyx,         Hue.DarkCopper,   Pattern.Acorn)
        | Terrain.Bush      _                                         -> (Hue.Onyx,         Hue.DarkJade,     Pattern.Bush)
        | Terrain.DeadBush  _                                         -> (Hue.Onyx,         Hue.DarkCopper,   Pattern.Bush)
        | Terrain.Clear Stump                                         -> (Hue.Onyx,         Hue.Jade,         Pattern.Field)
        | Terrain.Spawner (Rabbit,_)                                  -> (Hue.Onyx,         Hue.Copper,       Pattern.Hole)
        | Terrain.Clear Rubble                                        -> (Hue.Onyx,         Hue.DarkSilver,   Pattern.Field)
        | Terrain.Clear (Dirt _)                                      -> (Hue.Onyx,         Hue.DarkCopper,   Pattern.Field)

        | Terrain.Fixture (Item.CraftingBox CraftingMethod.Workbench) -> (Hue.Onyx,         Hue.DarkCopper,   Pattern.Workbench)
        | Terrain.Fixture (Item.CraftingBox CraftingMethod.Altar)     -> (Hue.Onyx,         Hue.DarkSilver,   Pattern.Workbench)
        | Terrain.Fixture (Item.CraftingBox CraftingMethod.Furnace)   -> (Hue.Onyx,         Hue.DarkSilver,   Pattern.Furnace)

        | Terrain.Wall WallMaterial.Wood                              -> (Hue.DarkerCopper, Hue.DarkCopper,   Pattern.Wall)
        | Terrain.Wall WallMaterial.Stone                             -> (Hue.DarkerSilver, Hue.DarkSilver,   Pattern.Wall)
                                                                      
        | Terrain.Door true                                           -> (Hue.Onyx        , Hue.DarkCopper,   Pattern.Door)
        | Terrain.Door false                                          -> (Hue.Onyx        , Hue.DarkCopper,   Pattern.OpenDoor)
                                                                      
        | Trader t                                                    -> (Hue.Onyx,         t.hue,            Pattern.House)
        | Quest                                                       -> (Hue.Onyx,         Hue.DarkAmethyst, Pattern.Sign)
        | _                                                           -> (Hue.Onyx,         Hue.Amethyst,     Pattern.Character3F)
    static member createMalachite (random:Random) : Terrain =
        [((Ore.Copper |> Item.Ore, 10u) |>Terrain.OreRock,1u)]|>Map.ofList|>Generator.generate random

    static member createCassiterite (random:Random) : Terrain =
        [((Ore.Tin |> Item.Ore, 10u) |>Terrain.OreRock,1u)]|>Map.ofList|>Generator.generate random

    static member createBauxite (random:Random) : Terrain =
        [((Ore.Iron |> Item.Ore, 10u) |>Terrain.OreRock,1u)]|>Map.ofList|>Generator.generate random

    static member createMithrilOre (random:Random) : Terrain =
        [((Ore.Mithril |> Item.Ore, 10u) |>Terrain.OreRock,1u)]|>Map.ofList|>Generator.generate random

    static member createUnobtainiumOre (random:Random) : Terrain =
        [((Ore.Unobtainium |> Item.Ore, 10u) |>Terrain.OreRock,1u)]|>Map.ofList|>Generator.generate random

    static member createGrass (random:Random) : Terrain =
        [(Terrain.Clear Grass,1u)]|>Map.ofList|>Generator.generate random

    static member createTree (random:Random) : Terrain =
        [(Terrain.Tree (10u,100u),1u)]|>Map.ofList|>Generator.generate random

    static member createRock (random:Random) : Terrain =
        [(Terrain.Rock 10u,1u)]|>Map.ofList|>Generator.generate random

    static member createFlintrock (random:Random) : Terrain =
        [(Terrain.Flintrock 5u,1u)]|>Map.ofList|>Generator.generate random

    static member createWater (random:Random) : Terrain =
        [(Terrain.Water (5u,20u),1u)]|>Map.ofList|>Generator.generate random

    static member createTrader (random:Random) : Terrain =
        [(Terrain.Trader Trader.Wood,1u);(Terrain.Trader Trader.Fur,1u)]|>Map.ofList|>Generator.generate random

    static member createQuest (random:Random) : Terrain =
        [(Terrain.Quest,1u)]|>Map.ofList|>Generator.generate random

    static member createBerrySeed (random:Random) : Terrain =
        let growthTimerGenerator = [(150u,1u);(300u,3u);(450u,2u)] |> Map.ofList
        (Seed.Berry, growthTimerGenerator |> Generator.generate random) |> Terrain.Seed

    static member createTreeSeed (random:Random) : Terrain =
        let growthTimerGenerator = [(150u,1u);(300u,3u);(450u,2u)] |> Map.ofList
        (Seed.Tree, growthTimerGenerator |> Generator.generate random) |> Terrain.Seed

    static member createDirt (random:Random) : Terrain =
        let generator = [(30u,1u);(60u,2u);(90u,3u);(120u,3u);(150u,2u);(180u,1u)]|>Map.ofList
        generator |> Generator.generate random |> Dirt |> Terrain.Clear

    static member createRabbitHole (random:Random) : Terrain =
        let generator = [(30u,1u);(60u,2u);(90u,3u);(120u,3u);(150u,2u);(180u,1u)]|>Map.ofList//TODO: make rabbit spawn timer have its own values
        (Rabbit, generator |> Generator.generate random) |> Terrain.Spawner

    member instance.isTree : bool =
        match instance with
        | Terrain.Tree _ -> true
        | _ -> false

    member instance.isQuest : bool =
        match instance with
        | Terrain.Quest -> true
        | _ -> false

    member instance.isWorkbench : bool =
        match instance with
        | Terrain.Fixture (Item.CraftingBox CraftingMethod.Workbench) -> true
        | _ -> false

    member instance.isAltar : bool =
        match instance with
        | Terrain.Fixture (Item.CraftingBox CraftingMethod.Altar) -> true
        | _ -> false

    member instance.isFurnace : bool =
        match instance with
        | Terrain.Fixture (Item.CraftingBox CraftingMethod.Furnace) -> true
        | _ -> false

    member instance.isBush : bool =
        match instance with
        | Terrain.Bush _ -> true
        | Terrain.DeadBush -> true
        | _ -> false

    member instance.isClear : bool =
        match instance with
        | Terrain.Clear _ -> true
        | _ -> false

    member instance.isDirt : bool =
        match instance with
        | Terrain.Clear (Dirt _) -> true
        | _ -> false

    member instance.isTrader (traderType:Trader): bool =
        match instance with
        | Terrain.Trader t when t = traderType -> true
        | _ -> false

    member instance.isNotTrader : bool =
        match instance with
        | Terrain.Trader _ -> false
        | _ -> true

    member instance.canPlant (plantType:Plant) : bool =
        match instance,plantType with
        | Terrain.Clear (Dirt _),_ -> true
        | _ -> false
        
    member instance.canMine : bool =
        match instance with
        | Terrain.Wall WallMaterial.Stone
        | Terrain.Fixture (Item.CraftingBox CraftingMethod.Furnace)
        | Terrain.Rock _
        | Terrain.Flintrock _ -> true
        | Terrain.OreRock _ -> true
        | _ -> false

    member instance.canShovel : bool =
        match instance with
        | Terrain.Seed (Seed.Tree, _)
        | Terrain.Clear Rubble
        | Terrain.Clear Stump -> true
        | _ -> false

    member instance.canUseBoneMeal : bool =
        match instance with
        | Terrain.Bush _ -> true
        | _ -> false

    member instance.canSpear : bool =
        match instance with
        | Terrain.Water _ -> true
        | _ -> false

    member instance.canHoe : bool =
        match instance with
        | Terrain.Clear Grass -> true
        | _ -> false

    member instance.canChop : bool =
        match instance with
        | Terrain.Tree _
        | Terrain.DeadBush
        | Terrain.Fixture (Item.CraftingBox CraftingMethod.Workbench)
        | Terrain.Door _
        | Terrain.Wall WallMaterial.Wood
        | Terrain.Bush _ -> true
        | _ -> false

    member instance.canHammer : bool =
        match instance with
        | Terrain.Fixture (Item.CraftingBox CraftingMethod.Altar) -> true
        | _ -> false

    member instance.canEnter : bool = 
        match instance with
        | Terrain.Door false -> true
        | _ -> instance.isClear

    member instance.isOpenable : bool = 
        match instance with
        | Terrain.Door true -> true
        | _ -> false

    member instance.isShuttable : bool = 
        match instance with
        | Terrain.Door false -> true
        | _ -> false

    member instance.noCraftableItemsText : string =
        match instance with
        | Terrain.Quest -> "(no posted fetch quests!)"
        | Terrain.Trader Trader.Wood -> "Berries for logs!"
        | _ -> "(no craftable items)"
    
    member instance.craftingCaption : string =
        match instance with
        | Terrain.Quest -> "Bulletin Board"
        | Terrain.Trader Trader.Wood -> "Wood Trader"
        | _ -> "Craft:"

    member instance.name : string =
        match instance with
        | Terrain.Clear x      -> x.name
        | Terrain.Fixture x    -> x.name |> sprintf "Installed %s"
        | Terrain.Door true    -> "Open Door"
        | Terrain.Door false   -> "Shut Door"
        | Terrain.Wall x       -> x.adjective |> sprintf "%s Wall"
        | Tree _               -> "Tree"
        | Rock _               -> "Rock"
        | Flintrock _          -> "Flintrock"
        | Water _              -> "Water"
        | Seed (Seed.Berry,_)  -> "Berry Seed"
        | Seed (Seed.Tree, _)  -> "Tree Seed"
        | OreRock (x,_)        -> x.name
        | Bush _               -> "Bush"
        | DeadBush             -> "Dead Bush"
        | Spawner (Rabbit,_)   -> "Rabbit Hole"
        | _                    -> "????????"

module Terrain =
    let isWorkbench                  (x:Terrain) = x.isWorkbench
    let isFurnace                    (x:Terrain) = x.isFurnace
    let isAltar                      (x:Terrain) = x.isAltar
    let canChop                      (x:Terrain) = x.canChop
    let canMine                      (x:Terrain) = x.canMine
    let canShovel                    (x:Terrain) = x.canShovel
    let canSpear                     (x:Terrain) = x.canSpear
    let canHammer                    (x:Terrain) = x.canHammer
    let canHoe                       (x:Terrain) = x.canHoe
    let alwaysInteract               (_:Terrain) = true
    let isBush                       (x:Terrain) = x.isBush
    let isTree                       (x:Terrain) = x.isTree
    let isClear                      (x:Terrain) = x.isClear
    let isDirt                       (x:Terrain) = x.isDirt
    let isOpenable                   (x:Terrain) = x.isOpenable
    let isTrader                 (t) (x:Terrain) = x.isTrader t
    let isNotTrader                  (x:Terrain) = x.isNotTrader
    let isQuest                      (x:Terrain) = x.isQuest
    let isNotQuest                               = isQuest >> not
    let isNotTraderOrQuest           (x:Terrain) = (x.isQuest |> not) && x.isNotTrader
    let isShuttable                  (x:Terrain) = x.isShuttable
    let canPlant       (plant:Plant) (x:Terrain) = x.canPlant plant
    let canUseBoneMeal               (x:Terrain) = x.canUseBoneMeal

﻿namespace Ggd.Splorr.Gadabout

open System

type BushDescriptor =
    {timers:Map<BushTimer,uint32>;
    berry:Berry}
    static member create (random:Random) : BushDescriptor =
        {berry= Berry.generator |> Generator.generate random;
        timers = BushTimer.generators |> Map.map (fun _ v -> v |> Generator.generate random)}

﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Plant =
    | Berry
    | Tree
    member x.name =
        match x with
        | Berry -> "Berry"
        | Tree -> "Tree"




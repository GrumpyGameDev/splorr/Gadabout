﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type BushTimer =
    | BerrySpawn
    | BushDeath
    member x.generator =
        match x with 
        | BushTimer.BerrySpawn ->
            [(30u, 1u);
            (60u, 4u);
            (150u, 9u)]
            |> Map.ofList
        | BushTimer.BushDeath ->
            [(300u, 1u);
            (600u, 3u);
            (900u, 3u);
            (1200u, 1u)]
            |> Map.ofList
    static member generators =
        [(BushTimer.BerrySpawn);
        (BushTimer.BushDeath)]
        |> List.map (fun x -> x, x.generator)
        |> Map.ofList
﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Command =
    | Up
    | Down
    | Left
    | Right
    | Green
    | Blue
    | Red
    | Yellow
    | Next
    | Previous
    | Back
    | Start


﻿namespace Ggd.Splorr.Gadabout

open System

module Game =
    let tryGetAtlasCell = (fun (w:World) -> w.tryGetAtlasCell)

    let getAvatarCreature = (fun (w:World) -> w.getAvatarCreature)

    let tryGetAvatarCreature = (fun (w:World) -> w.tryGetAvatarCreature)

    let private handleMoveAvatar (direction:Direction) (world:World) : World =
        (if world.avatar.facing = direction then
            let nextPosition = world.avatar.facing |> Direction.nextPosition world.avatar.position
            if world.canEnter nextPosition then
                world.moveAvatar nextPosition
            else
                world
        else
            world.setAvatarFacing direction).doTimers.handleTagonDeath

    let private getPreferredVerbs (world:World) : Verb list =
        (world.getAvatarInteractAtlasCell).terrain
        |> Verb.getPreferredVerbs

    let private updateVerb (world:World) : World =
        match world |> tryGetAvatarCreature with
        | Some creature ->
            let verbs =
                (world
                |> getAvatarCreature).getVerbs ((world.getAvatarInteractAtlasCell).terrain, (world.getAvatarAtlasCell).terrain) (VerbDescriptor.getVerbs())
                |> List.sortBy (fun (_,x) -> x.ordinal)
            let ordinal = 
                (world.avatar.verb
                |> VerbDescriptor.getDescriptor).ordinal
            let newVerb =
                if world.avatar.verb <> Verb.Look then
                    verbs
                    |> List.tryFind (fun (_,x) -> x.ordinal = ordinal)
                else
                    None
            match newVerb with
            | Some (_,v) -> 
                world.transformAvatar (fun a -> a.setVerb v.verb)
            | None ->
                let preferredVerb = 
                    world
                    |> getPreferredVerbs
                    |> List.find (fun x -> verbs |> List.exists (fun (_,y) -> y.verb=x))
                world.transformAvatar (fun a -> a.setVerb preferredVerb)
        | _ -> 
            world

    let private handleNextVerb (world:World) : World =
        let verbs =
            (world
            |> getAvatarCreature).getVerbs ((world.getAvatarInteractAtlasCell).terrain, (world.getAvatarInteractAtlasCell).terrain) (VerbDescriptor.getVerbs())
            |> List.sortBy (fun (_,x) -> x.ordinal)
        let ordinal = 
            (world.avatar.verb
            |> VerbDescriptor.getDescriptor).ordinal
        let newVerb =
            verbs
            |> List.tryFind (fun (_,x) -> x.ordinal > ordinal)
            |> Option.fold (fun _ b -> b) (verbs.Head)
        world.transformAvatar (fun a -> a.setVerb (newVerb |> snd).verb)

    let private handlePreviousVerb (world:World) : World =
        let verbs =
            (world
            |> getAvatarCreature).getVerbs ((world.getAvatarInteractAtlasCell).terrain, (world.getAvatarInteractAtlasCell).terrain) (VerbDescriptor.getVerbs())
            |> List.sortBy (fun (_,x) -> -(x.ordinal))
        let ordinal = 
            (world.avatar.verb
            |> VerbDescriptor.getDescriptor).ordinal
        let newVerb =
            verbs
            |> List.tryFind (fun (_,x) -> x.ordinal < ordinal)
            |> Option.fold (fun _ b -> b) (verbs.Head)
        world.transformAvatar (fun a -> a.setVerb (newVerb |> snd).verb)

    let private validateVerb (world:World) : World =
        match world |> tryGetAvatarCreature with
        | Some creature ->
            let verbs =
                creature.getVerbs ((world.getAvatarInteractAtlasCell).terrain, (world.getAvatarAtlasCell).terrain) (VerbDescriptor.getVerbs())
                |> List.map (fun (_,v) -> v.verb)
                |> Set.ofList
            if verbs.Contains(world.avatar.verb) then
                world
            else
                world |> handlePreviousVerb
        | _ -> world

    let private handleInteract (world:World) : World =
        let verbDescriptor = 
            world.avatar.verb
            |> VerbDescriptor.getDescriptor
        (world.doInteraction verbDescriptor).doTimers
        |> validateVerb

    let private handleUse (world:World) : World =
        let verbDescriptor = 
            world.avatar.verb
            |> VerbDescriptor.getDescriptor
        world.doUse verbDescriptor
        |> validateVerb

    let private handleSwitchToMapScreen (world:World) : World =
        world.setCommandMode CommandMode.Navigation

    let private handleRespawnTagon (world:World) : World =
        world.respawnAvatar

    let private handleSwitchToCraftingScreen (listSize:uint32) (world:World) : World =
        let tagon = 
            (world
            |> getAvatarCreature).getTagonDescriptor
        let cell =
            world.getAvatarInteractAtlasCell
        let scroller = Scroller.initialize (Recipe.getVisibleRecipeCount cell.terrain tagon.inventory) listSize
        world.setCommandMode (scroller |> CommandMode.Crafting)

    let private handleSwitchToInventoryScreen (listSize:uint32) (world:World) : World =
        let inHandScroller =
            (world
            |> getAvatarCreature).getTagonDescriptor.initializeInHandScroller listSize
        let onGroundScroller = 
            (world.getAvatarAtlasCell).initializeOnGroundScroller listSize
        let commandMode =
            (InventoryContextMode.InHand, [(InventoryContextMode.InHand, inHandScroller);
                (InventoryContextMode.OnGround, onGroundScroller)]
                |> Map.ofList)
                |> CommandMode.Inventory
        world.setCommandMode commandMode

    let private handleChangeListOnInventoryScreen (world:World) : World =
        match world.avatar.commandMode with
        | CommandMode.Inventory (mode,context) ->
            if mode = InventoryContextMode.InHand then
                world.setCommandMode ((InventoryContextMode.OnGround,context) |> CommandMode.Inventory)
            else
                world.setCommandMode ((InventoryContextMode.InHand,context) |> CommandMode.Inventory)
        | _ -> world

    let private handleDropOrPickUp (all:bool) (world:World) : World =
        let tagon =
            (world
            |> getAvatarCreature).getTagonDescriptor
        let cell = 
            world.getAvatarAtlasCell
        match world.avatar.commandMode with
        | CommandMode.Inventory (mode,context) ->
            match mode, (tagon.tryGetScrollerItem context.[InventoryContextMode.InHand]), (cell.tryGetScrollerItem context.[InventoryContextMode.OnGround]) with
            | InventoryContextMode.InHand, Some itemType, _ ->
                //determine item
                let itemCount = if all then tagon.getItemCount itemType else 1u
                //remove from inhand
                let tagon =
                    tagon.removeItemCount itemType itemCount
                //add to onground
                let cell = 
                    (cell.addItems itemType itemCount).placeCreature (tagon |> Creature.Tagon |> Some)
                //update command mode
                let commandMode =
                    (mode,[(InventoryContextMode.InHand,context.[InventoryContextMode.InHand].setItemCount (tagon.getScrollableItemCount));
                        (InventoryContextMode.OnGround,context.[InventoryContextMode.OnGround].setItemCount (cell.getScrollableItemCount))]
                        |> Map.ofList) |> CommandMode.Inventory
                (world.setCommandMode commandMode).updateAvatarCell cell
            | InventoryContextMode.OnGround, _, Some itemType ->
                //determine item
                let itemType = cell.getScrollerItem context.[InventoryContextMode.OnGround]
                let itemCount = if all then cell.getItemCount itemType else 1u
                //remove from inhand
                let tagon =
                    tagon.addItemCount itemType itemCount
                //add to onground
                let cell = 
                    (cell.removeItems itemType itemCount).placeCreature (tagon |> Creature.Tagon |> Some)
                //update command mode
                let commandMode =
                    (mode,[(InventoryContextMode.InHand,context.[InventoryContextMode.InHand].setItemCount (tagon.getScrollableItemCount));
                        (InventoryContextMode.OnGround,context.[InventoryContextMode.OnGround].setItemCount (cell.getScrollableItemCount))]
                        |> Map.ofList) |> CommandMode.Inventory
                (world.setCommandMode commandMode).updateAvatarCell cell
            | _ -> world
        | _ -> world

    let handleCraftingNextIndex (world:World) : World =
        match world.avatar.commandMode with
        | CommandMode.Crafting scroller ->
            world.setCommandMode (scroller.nextIndex() |> CommandMode.Crafting)
        | _ ->
            world

    let handleCraftingPreviousIndex (world:World) : World =
        match world.avatar.commandMode with
        | CommandMode.Crafting scroller ->
            world.setCommandMode (scroller.previousIndex() |> CommandMode.Crafting)
        | _ ->
            world

    let handleInventoryPreviousIndex (world:World) : World =
        match world.avatar.commandMode with
        | CommandMode.Inventory (InventoryContextMode.InHand, context) ->
            world.setCommandMode ((InventoryContextMode.InHand, context |> Map.add InventoryContextMode.InHand (context.[InventoryContextMode.InHand].previousIndex())) |> CommandMode.Inventory)
        | CommandMode.Inventory (InventoryContextMode.OnGround, context) ->
            world.setCommandMode ((InventoryContextMode.OnGround, context |> Map.add InventoryContextMode.OnGround (context.[InventoryContextMode.OnGround].previousIndex())) |> CommandMode.Inventory)
        | _ ->
            world

    let handleInventoryNextIndex (world:World) : World =
        match world.avatar.commandMode with
        | CommandMode.Inventory (InventoryContextMode.InHand, context) ->
            world.setCommandMode ((InventoryContextMode.InHand, context |> Map.add InventoryContextMode.InHand (context.[InventoryContextMode.InHand].nextIndex())) |> CommandMode.Inventory)
        | CommandMode.Inventory (InventoryContextMode.OnGround, context) ->
            world.setCommandMode ((InventoryContextMode.OnGround, context |> Map.add InventoryContextMode.OnGround (context.[InventoryContextMode.OnGround].nextIndex())) |> CommandMode.Inventory)
        | _ ->
            world

    let handleCraftCurrentRecipe (world:World) : World =
        match world.avatar.commandMode with
        | CommandMode.Crafting scroller ->
            if scroller.itemCount > 0u then
                let tagon =
                    (world
                    |> getAvatarCreature).getTagonDescriptor
                let terrain =
                    (world.getAvatarInteractAtlasCell).terrain
                let recipe =
                    Recipe.getVisibleRecipes terrain tagon.inventory
                    |> List.item (scroller.index|>int)
                let tagon =
                    if recipe.isCraftable terrain tagon.inventory then
                        tagon.craftRecipe recipe
                    else
                        tagon
                let commandMode =
                    scroller.setItemCount (Recipe.getVisibleRecipeCount terrain tagon.inventory)
                    |> CommandMode.Crafting
                (world.updateAvatarCell (world.getAvatarAtlasCell.placeCreature (tagon |> Creature.Tagon |> Some))).setCommandMode commandMode
            else
                world
        | _ -> 
            world

    let rec handleRespawn (world:World) : World =
        let spawnPosition = (world.random.Next(Constants.WorldColumns),world.random.Next(Constants.WorldRows))
        let creature = TagonDescriptor.create world.random |> Creature.Tagon
        match world.tryGetAtlasCell spawnPosition with
        | None ->
            let atlasCell = AtlasCell.spawn(world.random).placeCreature (creature|> Some)
            if atlasCell.terrain.isClear then
                ((world.setAtlasCell spawnPosition atlasCell).transformAvatar (fun a -> (a.setPosition spawnPosition).setCommandMode CommandMode.Navigation)).generate
            else
                world |> handleRespawn
        | Some x when x.terrain.isClear && x.creature.IsNone-> 
            let atlasCell = world.getAtlasCell spawnPosition
            ((world.setAtlasCell spawnPosition atlasCell).transformAvatar (fun a -> (a.setPosition spawnPosition).setCommandMode CommandMode.Navigation)).generate
        | _ -> 
            world
            |> handleRespawn


    let handle (command:Command,repeat:bool) (world:World option) : World option =
        match world, (world|>Option.map (fun w->w.avatar.commandMode)), command, repeat with

        | (None, None, Command.Green, _)                                  -> World.create()                         |> Some

        | (Some w, Some CommandMode.Navigation , Command.Up, _)           -> w |> handleMoveAvatar Direction.North  |> updateVerb |> Some
        | (Some w, Some CommandMode.Navigation , Command.Down, _)         -> w |> handleMoveAvatar Direction.South  |> updateVerb |> Some
        | (Some w, Some CommandMode.Navigation , Command.Left, _)         -> w |> handleMoveAvatar Direction.West   |> updateVerb |> Some
        | (Some w, Some CommandMode.Navigation , Command.Right, _)        -> w |> handleMoveAvatar Direction.East   |> updateVerb |> Some
        | (Some w, Some CommandMode.Navigation , Command.Next, _)         -> w |> handleNextVerb                    |> Some
        | (Some w, Some CommandMode.Navigation , Command.Previous, _)     -> w |> handlePreviousVerb                |> Some
        | (Some w, Some CommandMode.Navigation , Command.Blue, _)         -> w |> handleInteract                    |> Some
        | (Some w, Some CommandMode.Navigation , Command.Green, false)    -> w |> handleUse                         |> Some
        | (Some w, Some CommandMode.Navigation , Command.Red, false)      -> w |> handleSwitchToInventoryScreen 26u |> Some
        | (Some w, Some CommandMode.Navigation , Command.Yellow, false)   -> w |> handleSwitchToCraftingScreen 26u  |> Some

        | (Some w, Some (CommandMode.Dead), Command.Start, false)         -> w |> handleRespawn                     |> Some
                                                                                  
        | (Some w, Some (CommandMode.Inventory _), Command.Red, false)    -> w |> handleSwitchToMapScreen           |> Some
        | (Some w, Some (CommandMode.Inventory _), Command.Next, _)       -> w |> handleChangeListOnInventoryScreen |> Some
        | (Some w, Some (CommandMode.Inventory _), Command.Previous, _)   -> w |> handleChangeListOnInventoryScreen |> Some
        | (Some w, Some (CommandMode.Inventory _), Command.Green, _)      -> w |> handleDropOrPickUp true           |> Some
        | (Some w, Some (CommandMode.Inventory _), Command.Blue, _)       -> w |> handleDropOrPickUp false          |> Some
        | (Some w, Some (CommandMode.Inventory _), Command.Down, _)       -> w |> handleInventoryNextIndex          |> Some
        | (Some w, Some (CommandMode.Inventory _), Command.Up, _)         -> w |> handleInventoryPreviousIndex      |> Some

        | (Some w, Some (CommandMode.Crafting _) , Command.Yellow, false) -> w |> handleSwitchToMapScreen           |> Some
        | (Some w, Some (CommandMode.Crafting _) , Command.Green, false)  -> w |> handleCraftCurrentRecipe          |> Some
        | (Some w, Some (CommandMode.Crafting _), Command.Down, _)        -> w |> handleCraftingNextIndex           |> Some
        | (Some w, Some (CommandMode.Crafting _), Command.Up, _)          -> w |> handleCraftingPreviousIndex       |> Some

        | _                                                                    -> world

    let getAvatarStatistic (minimum: float option,maximum: float option)  (statistic:Statistic) (world:World) : uint32 =
        let value = 
            ((world
            |> getAvatarCreature).getTagonDescriptor).getCurrentStatistic statistic
        match value, minimum, maximum with
        | x, Some y, _ when x < y -> y |> uint32
        | x, _, Some y when x > y -> y |> uint32
        | x, _, _ -> x |> uint32


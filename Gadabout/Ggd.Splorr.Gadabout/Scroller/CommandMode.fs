﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type CommandMode =
    | Navigation
    | Dead
    | Inventory of  InventoryContextMode * InventoryContext
    | Crafting of Scroller


﻿namespace Ggd.Splorr.Gadabout

type Scroller =
    {itemCount:uint32;
    listSize:uint32;
    scroll:uint32;
    index:uint32}
    static member initialize (itemCount:uint32) (listSize:uint32) : Scroller =
        {itemCount=itemCount;
        listSize=listSize;
        index=0u;
        scroll=0u}

    member x.getMaximumScroll () =
        if x.listSize>x.itemCount then 0u else x.itemCount-x.listSize

    member private x.setIndex (index:uint32) : Scroller =
        let index =
            match index, x.itemCount with
            | _, 0u -> 0u
            | i, ic when i < ic -> i
            | i, ic -> i % ic
        {x with index=index}

    member private x.setScroll (scroll:uint32) : Scroller =
        let scroll =
            match scroll, x.index with
            | s, i when i < s -> i
            | s, i when i >= (s+x.listSize) -> System.Math.Min(i-x.listSize+1u,x.getMaximumScroll())
            | s, _ -> s
        {x with scroll=scroll}

    member x.refresh () : Scroller =
        (x.setIndex x.index).setScroll x.scroll

    member x.setItemCount (itemCount:uint32) : Scroller =
        {x with itemCount=itemCount}.refresh()

    member x.nextIndex () : Scroller =
        {x with index=x.index + 1u}.refresh()
    
    member x.previousIndex () : Scroller =
        {x with index=x.index + x.itemCount - 1u}.refresh()

    member x.isIndexVisible (candidate:uint32) : bool =
        candidate>=x.scroll && candidate<(x.scroll+x.listSize)
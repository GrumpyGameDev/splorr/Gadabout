﻿namespace Ggd.Splorr.Gadabout

type Avatar = 
    {position: int*int; 
    facing: Direction; 
    sight:int; 
    verb: Verb;
    commandMode:CommandMode}
    member avatar.getX : int =
        avatar.position |> fst

    member avatar.getY : int =
        avatar.position |> snd

    member avatar.setFacing (direction:Direction) : Avatar =
        {avatar with facing=direction}

    member avatar.setVerb (verb:Verb) : Avatar =
        {avatar with verb = verb}

    member avatar.setCommandMode (commandMode:CommandMode) : Avatar =
        {avatar with commandMode=commandMode}

    static member create () : Avatar =
        {position=(0,0); 
        facing=Direction.North;
        sight=6;
        verb=Verb.Look;
        commandMode=CommandMode.Navigation}

    member avatar.setPosition (position:int*int) : Avatar =
        {avatar with position = position}

    member avatar.getCursorPosition : int * int =
        let x,y = avatar.position
        match avatar.facing with 
        | Direction.North -> (x,y-1)
        | Direction.South -> (x,y+1)
        | Direction.East  -> (x+1,y)
        | Direction.West  -> (x-1,y)

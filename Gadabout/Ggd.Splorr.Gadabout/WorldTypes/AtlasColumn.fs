﻿namespace Ggd.Splorr.Gadabout

open System

type AtlasColumn =
    {table: Map<int,AtlasCell>}
    static member create () : AtlasColumn = {table = Map.empty}

    member atlasColumn.doTimers (random:Random) (column:int) : EffectInstance list=
        ([], atlasColumn.table)
        ||> Map.fold (fun effects row atlasCell -> effects |> List.append (atlasCell |> EffectInstance.doAtlasCellTimers random (column,row)))

    member atlasColumn.finalizeCreatures : AtlasColumn =
        {table = 
            (atlasColumn.table, atlasColumn.table)
            ||> Map.fold
                    (fun ac row cell -> 
                        ac 
                        |> Map.add row (cell.finalizeCreature))}
﻿namespace Ggd.Splorr.Gadabout

open System

type World =
    {avatar:Avatar;
    random:Random;
    atlas:Map<int,AtlasColumn>;
    messages:(Hue*string) list}

    member private world.doOpenInteraction (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (cellUpdater:AtlasCell->World->World) (tagon:TagonDescriptor) (cell:AtlasCell) (verbDescriptor:VerbDescriptor) : World = 
        let cell =
            match cell.terrain with
            | Terrain.Door true ->
                {cell with terrain = Terrain.Door false}
            | _ -> 
                cell
        world
        |> cellUpdater cell

    member private world.doShutInteraction (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (cellUpdater:AtlasCell->World->World) (tagon:TagonDescriptor) (cell:AtlasCell) (verbDescriptor:VerbDescriptor) : World = 
        let cell =
            match cell.terrain with
            | Terrain.Door false ->
                {cell with terrain = Terrain.Door true}
            | _ -> 
                cell
        world
        |> cellUpdater cell

    member private world.doToolInteraction (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (cellUpdater:AtlasCell->World->World) (tagon:TagonDescriptor) (cell:AtlasCell) (verbDescriptor:VerbDescriptor) : World = 
        let (itemsGained:(Item*uint32) option), (itemLost:Item option), newTerrain =
            match cell.terrain, verbDescriptor.verb with 
            /////////////////////////////////////////////////////////////////////////////////
            // Chopping Trees
            /////////////////////////////////////////////////////////////////////////////////
            | Terrain.Tree (1u, _), Verb.TakeLog
            | Terrain.Tree (1u, _), Verb.UseAxe _ ->
                (Some (Item.Log,1u), verbDescriptor.item, Terrain.Clear Stump)
            | Terrain.Tree (2u, _), Verb.UseAxe _->
                (Some (Item.Log,2u), verbDescriptor.item, Terrain.Clear Stump)
            | Terrain.Tree (x,y), Verb.TakeLog ->
                (Some (Item.Log,1u), verbDescriptor.item, Terrain.Tree (x-1u,y))
            | Terrain.Tree (x, y), Verb.UseAxe _->
                (Some (Item.Log,2u), verbDescriptor.item, Terrain.Tree (x-2u,y))

            /////////////////////////////////////////////////////////////////////////////////
            // Chopping Places
            /////////////////////////////////////////////////////////////////////////////////
            | Terrain.Bush _, Verb.UseAxe _->
                (None, verbDescriptor.item, Terrain.DeadBush)
            | Terrain.DeadBush, Verb.UseAxe _->
                (None, verbDescriptor.item, world.random |> Terrain.createDirt)
            | Terrain.Fixture (Item.CraftingBox CraftingMethod.Workbench), Verb.UseAxe _->
                (Some (Item.Log, 2u), verbDescriptor.item, world.random |> Terrain.createDirt)
            | Terrain.Wall WallMaterial.Wood, Verb.UseAxe _->
                (Some (Item.Log, 4u), verbDescriptor.item, world.random |> Terrain.createDirt)

            /////////////////////////////////////////////////////////////////////////////////
            // Mining
            /////////////////////////////////////////////////////////////////////////////////
            | Terrain.Flintrock 1u, Verb.UsePick _ -> 
                (Some (Item.Ore Flint,1u), verbDescriptor.item, Terrain.Clear Rubble)
            | Terrain.Flintrock x, Verb.UsePick _ -> 
                (Some (Item.Ore Flint, 1u), verbDescriptor.item, Terrain.Flintrock (x-1u))
            | Terrain.Rock 1u, Verb.UsePick _ -> 
                (Some (Item.Ore Stone,1u), verbDescriptor.item, Terrain.Clear Rubble)
            | Terrain.Rock x, Verb.UsePick _ -> 
                (Some (Item.Ore Stone, 1u), verbDescriptor.item, Terrain.Rock (x-1u))
            | Terrain.Fixture (Item.CraftingBox CraftingMethod.Furnace), Verb.UsePick _ -> 
                (Some (Item.Ore Stone, 4u), verbDescriptor.item, world.random |> Terrain.createDirt)
            | Terrain.Wall WallMaterial.Stone, Verb.UsePick _ -> 
                (Some (Item.Ore Stone, 4u), verbDescriptor.item, world.random |> Terrain.createDirt)

            | (Terrain.OreRock (Item.Ore Ore.Copper, 1u)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Copper, 1u), verbDescriptor.item, world.random |> Terrain.createDirt)
            | (Terrain.OreRock (Item.Ore Ore.Copper, x)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Copper, (1u)), verbDescriptor.item, Terrain.OreRock (Item.Ore Ore.Copper, (x-1u)))

            | (Terrain.OreRock (Item.Ore Ore.Tin, 1u)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Tin, 1u), verbDescriptor.item, world.random |> Terrain.createDirt)
            | (Terrain.OreRock (Item.Ore Ore.Tin, x)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Tin, (1u)), verbDescriptor.item, Terrain.OreRock (Item.Ore Ore.Tin, (x-1u)))

            | (Terrain.OreRock (Item.Ore Ore.Iron, 1u)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Iron, 1u), verbDescriptor.item, world.random |> Terrain.createDirt)
            | (Terrain.OreRock (Item.Ore Ore.Iron, x)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Iron, (1u)), verbDescriptor.item, Terrain.OreRock (Item.Ore Ore.Iron, (x-1u)))

            | (Terrain.OreRock (Item.Ore Ore.Mithril, 1u)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Mithril, 1u), verbDescriptor.item, world.random |> Terrain.createDirt)
            | (Terrain.OreRock (Item.Ore Ore.Mithril, x)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Mithril, (1u)), verbDescriptor.item, Terrain.OreRock (Item.Ore Ore.Mithril, (x-1u)))

            | (Terrain.OreRock (Item.Ore Ore.Unobtainium, 1u)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Unobtainium, 1u), verbDescriptor.item, world.random |> Terrain.createDirt)
            | (Terrain.OreRock (Item.Ore Ore.Unobtainium, x)), Verb.UsePick _ ->
                (Some (Item.Ore Ore.Unobtainium, (1u)), verbDescriptor.item, Terrain.OreRock (Item.Ore Ore.Unobtainium, (x-1u)))

            /////////////////////////////////////////////////////////////////////////////////
            // Shoveling
            /////////////////////////////////////////////////////////////////////////////////
            | Terrain.Clear Stump, Verb.UseShovel _ ->
                (Some (Item.Seed Seed.Tree,world.random.Next(4) |> uint32), verbDescriptor.item, world.random |> Terrain.createDirt)
            | Terrain.Seed (Seed.Tree,_), Verb.UseShovel _ -> 
                (Some (Item.Seed Seed.Tree,1u), verbDescriptor.item, world.random |> Terrain.createDirt)
            | Terrain.Clear Rubble, Verb.UseShovel _ -> 
                (Some (Item.Gravel,1u), verbDescriptor.item, world.random |> Terrain.createDirt)

            /////////////////////////////////////////////////////////////////////////////////
            // Spearfishing
            /////////////////////////////////////////////////////////////////////////////////
            | Terrain.Water (0u,t), Verb.UseSpear _ -> 
                (None, None, (0u,t) |> Terrain.Water)
            | Terrain.Water (x,t), Verb.UseSpear _ when x > 0u -> 
                (Some(Item.Carcass Carcass.Fish,1u), verbDescriptor.item, (x-1u,t) |> Terrain.Water)

            /////////////////////////////////////////////////////////////////////////////////
            // Hammering Places
            /////////////////////////////////////////////////////////////////////////////////
            | Terrain.Fixture (Item.CraftingBox CraftingMethod.Altar), Verb.UseHammer _->
                (None, verbDescriptor.item, Terrain.Clear Rubble)

            /////////////////////////////////////////////////////////////////////////////////
            // Tilling Places (with a Hoe!)
            /////////////////////////////////////////////////////////////////////////////////
            | Terrain.Clear Grass, Verb.UseHoe _->
                (None, verbDescriptor.item, Terrain.createDirt world.random)


            | _ -> 
                (None, None, cell.terrain)

        let tagon =
            (tagon.addOptionalItems itemsGained).removeOptionalItem itemLost
        
        let cell = 
            {cell with terrain=newTerrain}
        
        (world, itemsGained)
        ||> Option.fold (fun acc (i,c) -> acc |> messageAppender (Hue.Silver,sprintf "+%u %s (%u)" c (i.name) (tagon.getItemCount i)))
        |> tagonUpdater tagon
        |> cellUpdater cell

    member private world.doPickBerryInteraction  (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (cellUpdater:AtlasCell->World->World) (tagon:TagonDescriptor) (cell:AtlasCell) (verbDescriptor:VerbDescriptor) : World = 
        match cell.items.tryGetFirstItem() with
        | Some i -> 
            let tagon =
                tagon.addItemCount i 1u

            let cell = 
                {cell with items = cell.items.removeItemCount i 1u}

            world
            |> messageAppender (Hue.Silver,sprintf "+1 %s (%u)" (i.name) (tagon.getItemCount i))
            |> tagonUpdater tagon
            |> cellUpdater cell
        | _ -> 
            world

    member private world.doFertilizeInteraction  (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (cellUpdater:AtlasCell->World->World) (tagon:TagonDescriptor) (cell:AtlasCell) (verbDescriptor:VerbDescriptor) : World = 
        let tagon = 
            tagon.removeOptionalItem verbDescriptor.item

        let terrainInstance =
            match cell.terrain with
            | Terrain.Bush b ->
                {b with timers = b.timers |> Map.add BushTimer.BerrySpawn (b.timers.[BushTimer.BerrySpawn]/2u)} |> Terrain.Bush
            | Terrain.Seed (Seed.Berry, x) ->
                (Seed.Berry, x/2u) |> Terrain.Seed
            | Terrain.Seed (Seed.Tree, x) ->
                (Seed.Tree, x/2u) |> Terrain.Seed
            | _ -> cell.terrain
        
        let cell =
            {cell with terrain = terrainInstance}

        (world, verbDescriptor.item)
        ||> Option.fold(fun w i -> w |> messageAppender (Hue.Silver,sprintf "-1 %s (%u)" (i.name) (tagon.getItemCount i)))
        |> tagonUpdater tagon
        |> cellUpdater cell

    member private world.doBerryUse  (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (tagon:TagonDescriptor) (verbDescriptor:VerbDescriptor) : World = 
        match verbDescriptor.useDescriptor with
        | UseDescriptor.HealEnergy energy ->
            let tagon =
                (tagon.removeItemCount verbDescriptor.item.Value 1u).addEnergy energy
            world
            |> messageAppender (Hue.Silver, sprintf "-1 %s (%d)" (verbDescriptor.item.Value.name) (tagon.getItemCount verbDescriptor.item.Value))
            |> messageAppender (Hue.Silver, sprintf "+%d Energy (%d)" (energy |> int) (tagon.getCurrentStatistic Statistic.Energy |>int))
            |> tagonUpdater tagon
        | _ ->
            world

    member private world.doMeatUse  (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (tagon:TagonDescriptor) (verbDescriptor:VerbDescriptor) : World = 
        match verbDescriptor.useDescriptor with
        | UseDescriptor.HealHunger hunger ->
            let tagon =
                (tagon.removeItemCount verbDescriptor.item.Value 1u).addStomach hunger
            world
            |> messageAppender (Hue.Silver, sprintf "-1 %s (%d)" (verbDescriptor.item.Value.name) (tagon.getItemCount verbDescriptor.item.Value))
            |> messageAppender (Hue.Silver, sprintf "+%d Stomach (%d)" (hunger |> int) (tagon.getCurrentStatistic Statistic.Hunger |>int))
            |> tagonUpdater tagon
        | _ ->
            world

    member world.doUseWithUpdates (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (cellUpdater:AtlasCell->World->World) (tagon:TagonDescriptor) (cell:AtlasCell) (verbDescriptor:VerbDescriptor) : World = 
        if (tagon.getCurrentStatistic Statistic.Energy)>=(verbDescriptor.energy) then
            let tagon =
                (tagon.spendEnergy (verbDescriptor.energy)).setExertion verbDescriptor.exertion
            match verbDescriptor.verb with
            | Verb.EatBerry _ ->
                world.doBerryUse messageAppender tagonUpdater tagon verbDescriptor
            | Verb.UseRawMeat
            | Verb.UseCookedMeat ->
                world.doMeatUse messageAppender tagonUpdater tagon verbDescriptor
            | _ -> world
        else
            world
            |> messageAppender (verbDescriptor.insufficientEnergyMessage)

    member private world.placeTerrainWithUpdates (terrain:Terrain)  (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (cellUpdater:AtlasCell->World->World) (tagon:TagonDescriptor) (cell:AtlasCell) (verbDescriptor:VerbDescriptor) : World = 
        let tagon = 
            tagon.removeItemCount verbDescriptor.item.Value 1u

        let cell =
            {cell with terrain = terrain}

        world
        |> tagonUpdater tagon
        |> cellUpdater cell

    member world.doLookInteraction (messageAppender: (Hue*string) -> World -> World) (cell:AtlasCell) : World = 
        let message:string option = 
            match cell.terrain with
            | Terrain.Water (x,_) -> x |> sprintf "Water (%u fish)" |> Some
            | Terrain.Tree (x,_) -> x |> sprintf "Tree (%u wood)" |> Some
            | Terrain.Flintrock x -> x |> sprintf "Flintrock (%u flint)" |> Some
            | Terrain.Rock x -> x |> sprintf "Rock (%u stone)" |> Some
            | Terrain.Clear Grass -> "Grass" |> Some
            | Terrain.DeadBush -> "Dead Bush" |> Some
            | Terrain.Clear Stump -> "Stump" |> Some
            | Terrain.Fixture (Item.CraftingBox CraftingMethod.Workbench) -> "Workbench" |> Some
            | Terrain.Fixture (Item.CraftingBox CraftingMethod.Furnace) -> "Furnace" |> Some
            | Terrain.Clear Rubble -> "Rubble" |> Some
            | Terrain.Clear (Dirt _) -> "Dirt" |> Some
            | Terrain.Seed (Seed.Berry,x) -> x |> sprintf "Berry Seed (%u time remaining)" |> Some
            | Terrain.Seed (Seed.Tree, x) -> x |> sprintf "Sapling (%u time remaining)" |> Some
            | Terrain.Wall WallMaterial.Wood -> "Wood Wall" |> Some
            | Terrain.Wall WallMaterial.Stone -> "Stone Wall" |> Some
            | _ -> None
            
        (world, message)
        ||> Option.fold(fun w m -> w |> messageAppender (Hue.Silver,m))

    member world.doInteractionWithUpdates (messageAppender: (Hue*string) -> World -> World) (tagonUpdater: TagonDescriptor->World->World) (cellUpdater:AtlasCell->World->World) (tagon:TagonDescriptor) (cell:AtlasCell) (verbDescriptor:VerbDescriptor) : World = 
        if (tagon.getCurrentStatistic Statistic.Energy)>=(verbDescriptor.energy) then
            let tagon =
                (tagon.spendEnergy (verbDescriptor.energy)).setExertion verbDescriptor.exertion
            match verbDescriptor.verb with
            | Verb.Look ->
                world.doLookInteraction messageAppender cell
            | Verb.TakeLog
            | Verb.UseAxe _ 
            | Verb.UsePick _ 
            | Verb.UseSpear _ 
            | Verb.UseShovel _
            | Verb.UseHammer _ 
            | Verb.UseHoe _ ->
                world.doToolInteraction messageAppender tagonUpdater cellUpdater tagon cell verbDescriptor

            | Verb.PickBerry ->
                world.doPickBerryInteraction messageAppender tagonUpdater cellUpdater tagon cell verbDescriptor

            | Verb.UseBoneMeal ->
                world.doFertilizeInteraction messageAppender tagonUpdater cellUpdater tagon cell verbDescriptor

            | Verb.Open ->
                world.doOpenInteraction messageAppender tagonUpdater cellUpdater tagon cell verbDescriptor

            | Verb.Shut ->
                world.doShutInteraction messageAppender tagonUpdater cellUpdater tagon cell verbDescriptor

            | Verb.PlaceTerrain t ->
                world.placeTerrainWithUpdates t messageAppender tagonUpdater cellUpdater tagon cell verbDescriptor

            | Verb.Plant Plant.Berry ->
                world.placeTerrainWithUpdates (world.random |> Terrain.createBerrySeed) messageAppender tagonUpdater cellUpdater tagon cell verbDescriptor

            | Verb.Plant Plant.Tree ->
                world.placeTerrainWithUpdates (world.random |> Terrain.createTreeSeed) messageAppender tagonUpdater cellUpdater tagon cell verbDescriptor

            | _ -> 
                world
        else
            world
            |> messageAppender (verbDescriptor.insufficientEnergyMessage)
    member private world.generateCell (column:int, row:int) : World =
        let column = Utility.wrapColumn column
        let row = Utility.wrapRow row
        let atlasCell =
            match world.atlas.[column].table |> Map.tryFind row with
            | None when (column = (world.avatar.getX)) && (row=(world.avatar.getY)) -> AtlasCell.createTagon(world.random)
            | None -> AtlasCell.spawn world.random
            | Some cell -> cell
        let atlasColumn =
            {world.atlas.[column] with table =  world.atlas.[column].table |> Map.add row atlasCell}
        {world with atlas = world.atlas |> Map.add column atlasColumn}

    member private world.generateColumn (column:int) : World =
        let column = Utility.wrapColumn column
        let atlasColumn =
            match world.atlas |> Map.tryFind column with
            | None -> 
                AtlasColumn.create()
            | Some exists ->  exists
        ({world with atlas = world.atlas |> Map.add column atlasColumn}, [-world.avatar.sight..world.avatar.sight])
        ||> List.fold (fun w row -> w.generateCell (column,row+(world.avatar.getY)))

    member world.generate : World =
        (world,[-world.avatar.sight..world.avatar.sight])
        ||> List.fold (fun w column -> w.generateColumn (column+(world.avatar.getX)))

    static member create () : World =
        {avatar = Avatar.create(); 
        random=new Random();
        atlas = Map.empty;
        messages=[]}.generate

    member world.tryGetAtlasCell (column:int, row:int) : AtlasCell option =
        let column = Utility.wrapColumn column
        let row = Utility.wrapRow row
        match world.atlas |> Map.tryFind column with
        | None -> None
        | Some atlasColumn -> atlasColumn.table |> Map.tryFind row

    member world.getAtlasCell (column:int, row:int) = world.tryGetAtlasCell (column, row) |> Option.get

    member world.setAtlasCell (column:int, row:int) (atlasCell:AtlasCell) : World =
        let column = Utility.wrapColumn column
        let row = Utility.wrapRow row
        let atlasColumn = 
            {table = 
                match world.atlas |> Map.tryFind column with
                | Some x -> x.table
                | None -> Map.empty
                |> Map.add row atlasCell}
        {world with atlas = world.atlas |> Map.add column atlasColumn}

    member internal world.transformAvatar (f:Avatar->Avatar) : World =
        {world with avatar = world.avatar |> f}

    member internal world.setAvatarFacing (direction:Direction) : World =
        world.transformAvatar (fun a -> a.setFacing direction)

    member internal world.setAvatarCommandMode (commandMode:CommandMode) : World =
        world.transformAvatar (fun a -> a.setCommandMode commandMode)

    member private world.getCreature (position:int*int) : Creature option =
        world.tryGetAtlasCell position
        |> Option.bind (fun x -> x.creature)

    member private world.placeCreature (creature:Creature option) (position:int*int) : World =
        let atlasCell =
            (world.tryGetAtlasCell position
            |> Option.get).placeCreature creature
        world.setAtlasCell position atlasCell
        

    member private world.clearCreature = world.placeCreature None

    member private world.setAvatarPosition (position:int*int) : World =
        {world with avatar = world.avatar.setPosition position}

    member internal world.moveAvatar (position:int*int) : World =
        let tagon = world.getCreature world.avatar.position
        ((((world.clearCreature world.avatar.position)
        .placeCreature (tagon |> Creature.setExertion (-0.25) |> Creature.upkeep) position)
        .setAvatarPosition position)
        .generate)

    member internal world.canEnter (position:int*int) : bool =
        (world.tryGetAtlasCell position |> Option.get).canEnter

    member private world.placeTerrain (position:int*int) (terrain:Terrain) : World =
        let atlasCell =
            (world.tryGetAtlasCell position
            |> Option.get).placeTerrain terrain
        world.setAtlasCell position atlasCell

    member private world.addItems (position: int*int) (count:uint32) (item:Item) : World =
        let atlasCell =
            (world.tryGetAtlasCell position
            |> Option.get).addItems item count
        world.setAtlasCell position atlasCell
        
    member private world.removeItems (position: int*int) (count:uint32) (item:Item) : World =
        let atlasCell =
            (world.tryGetAtlasCell position
            |> Option.get).removeItems item count
        world.setAtlasCell position atlasCell
        
    member private world.updateCreature (position:int*int) (f:Creature->Creature option) : World =
        let atlasCell =
            world.tryGetAtlasCell position
            |> Option.get
        match atlasCell.creature with
        | None -> 
            world
        | Some c -> 
            world.setAtlasCell position (atlasCell.placeCreature (c |> f))

    member private world.spawnCreature (position:int*int) (creature:Creature) : World =
        let atlasCell =
            world.tryGetAtlasCell position
            |> Option.get
        match atlasCell.creature with
        | None -> 
            world.setAtlasCell position (atlasCell.placeCreature (creature |> Some))
        | Some _ -> 
            world

    member private world.moveCreature (position:int*int) (direction:Direction) : World =
        let atlasCell =
            world.tryGetAtlasCell position
            |> Option.get
        let nextPosition = 
            direction 
            |> Direction.nextPosition position
        let nextAtlasCell =
            match world.tryGetAtlasCell nextPosition with
            | None -> AtlasCell.spawn world.random
            | Some x -> x
        match atlasCell.creature, nextAtlasCell.creature with
        | Some c, None ->
            if nextAtlasCell.terrain.canEnter then
                ((world.setAtlasCell position (atlasCell.placeCreature None))
                .setAtlasCell nextPosition (nextAtlasCell.placeCreature (Some c)))
            else
                world.setAtlasCell nextPosition nextAtlasCell
        | _ -> 
            world.setAtlasCell nextPosition nextAtlasCell

    member private world.removeCreature (position:int*int) : World =
        let atlasCell =
            world.tryGetAtlasCell position
            |> Option.get
        match atlasCell.creature with
        | Some c ->
            world.setAtlasCell position (atlasCell.placeCreature None)
        | _ -> 
            world 

    member world.respawnAvatar : World =
        world

    member private world.applyEffect (effect:EffectInstance) : World =
        match effect.position, effect.effect with
        | (position, Effect.RemoveCreature) ->
            world.removeCreature position
        | (position, Effect.Terrain t) -> 
            world.placeTerrain position t
        | (position, Effect.AddItem (c,i)) ->
            world.addItems position c i
        | (position, Effect.RemoveItem (c,i)) ->
            world.removeItems position c i
        | (position, Effect.SpawnCreature c) ->
            world.spawnCreature position c
        | (position, Effect.MoveCreature d) ->
            world.moveCreature position d
        | (position, Effect.UpdateCreature u) ->
            world.updateCreature position u
        | _ -> world
    
    member private world.applyEffects (effects:EffectInstance list) : World =
        (world, effects)
        ||> List.fold (fun w e -> w.applyEffect e)

    member private world.finalizeCreatures : World =
        (world, world.atlas)
        ||> Map.fold (fun w column atlasColumn -> {w with atlas=w.atlas |> Map.add column (atlasColumn.finalizeCreatures)})

    member internal world.doTimers: World =
        let effects =
            ([], world.atlas)
            ||> Map.fold (fun effects column atlasColumn -> effects |> List.append (atlasColumn.doTimers world.random column))
        (world.applyEffects effects).finalizeCreatures

    member internal world.handleTagonDeath: World =
        match world.tryGetAvatarCreature with
        | Some (Creature.Tagon t) when t.isDead ->
            let position = world.avatar.position
            let updatedCell = 
                ((((world.getAtlasCell position).placeCreature None), t.inventory.table)
                ||> Map.fold 
                    (fun acc k v -> 
                        acc.addItems k v)).addItems (Item.Carcass Carcass.Human) 1u
            (world.setAtlasCell position updatedCell).setAvatarCommandMode CommandMode.Dead
        | _ -> 
            world

    member private world.tryGetAvatarAtlasCell : AtlasCell option =
        world.tryGetAtlasCell world.avatar.position

    member world.getAvatarAtlasCell = world.tryGetAvatarAtlasCell |> Option.get

    member world.tryGetAvatarCreature : Creature option =
        (world.tryGetAvatarAtlasCell).Value.creature

    member world.getAvatarCreature = world.tryGetAvatarCreature |> Option.get

    member world.getAvatarInteractAtlasCell : AtlasCell =
        let position =
            world.avatar.getCursorPosition
        world.tryGetAtlasCell position
        |> Option.get

    member world.messageAppender (message:Hue*string) : World =
        let messages = 
            match world.messages |> List.length with
            | n when n > 4 -> world.messages |> List.append [message] |> List.take 5
            | _ -> world.messages |> List.append [message]
        {world with messages = messages}

    member private world.tagonUpdater (position:int*int) (t:TagonDescriptor) : World =
        let updatedCell = (world.getAtlasCell position).placeCreature (t.upkeep |> Creature.Tagon |> Some)
        (world.setAtlasCell position updatedCell).handleTagonDeath

    member private world.cellUpdater (position:int*int) (c:AtlasCell) : World =
        world.setAtlasCell position c

    member internal world.doUse (verbDescriptor:VerbDescriptor) : World =
        let cell = world.getAvatarAtlasCell
        match verbDescriptor.useDescriptor , world.getAvatarCreature with
        | UseDescriptor.Nothing, _ -> 
            world
        | _, Creature.Tagon t ->
            world.doUseWithUpdates (fun x w -> w.messageAppender x) (fun x w -> w.tagonUpdater w.avatar.position x) (fun x w -> w.cellUpdater w.avatar.position x) t cell verbDescriptor
        | _ -> 
            world
    
    member internal world.doInteraction (verbDescriptor:VerbDescriptor) : World =
        let cell = world.getAvatarInteractAtlasCell
        match cell.terrain |> verbDescriptor.canInteract,world.getAvatarCreature with
        | true, Creature.Tagon t ->
            world.doInteractionWithUpdates (fun x w -> w.messageAppender x) (fun x w -> w.tagonUpdater w.avatar.position x) (fun x w -> w.cellUpdater w.avatar.getCursorPosition x) t cell verbDescriptor
        | _ -> 
            world

    member world.setCommandMode (commandMode:CommandMode) = world.transformAvatar (fun a -> a.setCommandMode commandMode)

    member world.updateAvatarCell (cell:AtlasCell) : World =
        world.setAtlasCell world.avatar.position cell

﻿namespace Ggd.Splorr.Gadabout

open System

type EffectInstance = 
    {position : int*int;
    effect: Effect}
    static member create position effect =
        {position = position; effect = effect}
    static member private doBushTimers (random:Random) (position:int*int) (bushDescriptor:BushDescriptor) : EffectInstance list =
        let berryEffects, berryTimer = 
            if bushDescriptor.timers.[BushTimer.BerrySpawn] = 0u then 
                ([EffectInstance.create position (Effect.AddItem (1u,Item.Berry bushDescriptor.berry))],BushTimer.BerrySpawn.generator |> Generator.generate random) 
            else 
                ([],bushDescriptor.timers.[BushTimer.BerrySpawn] - 1u)
        if bushDescriptor.timers.[BushTimer.BushDeath] = 0u then
            berryEffects 
            |> List.append [EffectInstance.create position (Effect.Terrain Terrain.DeadBush)]
        else
            let descriptor = 
                {bushDescriptor with 
                    timers = bushDescriptor.timers 
                                |> Map.add BushTimer.BerrySpawn berryTimer 
                                |> Map.add BushTimer.BushDeath (bushDescriptor.timers.[BushTimer.BushDeath] - 1u)}
            berryEffects
            |> List.append [EffectInstance.create position (descriptor |> Terrain.Bush |> Effect.Terrain)]

    static member private doBerrySeedGrowthTimer (random:Random) (position:int*int) (growthTimer:uint32) : EffectInstance list =
        match growthTimer with
        | 0u -> [EffectInstance.create position (random |> BushDescriptor.create |> Terrain.Bush |> Effect.Terrain)]
        | x -> [EffectInstance.create position ((Seed.Berry, x-1u) |> Terrain.Seed |> Effect.Terrain)]

    static member private doTreeSeedGrowthTimer (position:int*int) (growthTimer:uint32) : EffectInstance list =
        match growthTimer with
        | 0u -> [EffectInstance.create position ((10u, 100u) |> Terrain.Tree |> Effect.Terrain)]
        | x -> [EffectInstance.create position ((Seed.Tree, x-1u) |> Terrain.Seed |> Effect.Terrain)]

    static member private doAcornTimer (random:Random) (position:int*int) (woodLeft: uint32, acornTimer:uint32) : EffectInstance list =
        match acornTimer with
        | 0u -> 
            //TODO: pick random empty neighbor and place an acorn there
            [EffectInstance.create position ((woodLeft, 100u) |> Terrain.Tree |> Effect.Terrain)]
        | x -> 
            [EffectInstance.create position ((woodLeft, x-1u) |> Terrain.Tree |> Effect.Terrain)]

    static member private doDirtGrassTimer (position:int*int) (grassTimer:uint32) : EffectInstance list =
        match grassTimer with
        | 0u -> 
            [EffectInstance.create position (Terrain.Clear Grass |> Effect.Terrain)]
        | x -> 
            [EffectInstance.create position ((x-1u) |> Dirt |> Terrain.Clear |> Effect.Terrain)]

    static member private doRabbitHoleSpawnTimer (random:Random) (position:int*int) (grassTimer:uint32) : EffectInstance list =
        match grassTimer with
        | 0u -> [EffectInstance.create position (random |> Terrain.createRabbitHole |> Effect.Terrain); EffectInstance.create position (RabbitDescriptor.create() |> Creature.Rabbit |> Effect.SpawnCreature)]
        | x -> [EffectInstance.create position ((Rabbit,x-1u) |> Terrain.Spawner |> Effect.Terrain)]

    static member private doWaterRestockTimer (random:Random) (position:int*int) (fishLeft:uint32) (timer:uint32) : EffectInstance list =
        match fishLeft,timer with
        | f, _ when f >= 20u -> []
        | f, 0u when (random.Next(20)|>uint32) <= f -> [EffectInstance.create position ((f+1u,20u) |> Terrain.Water |> Effect.Terrain )]
        | f, 0u -> [EffectInstance.create position ((f,20u) |> Terrain.Water |> Effect.Terrain )]
        | f, x -> [EffectInstance.create position ((f,x-1u) |> Terrain.Water |> Effect.Terrain )]

    static member doTimers (random:Random) (position:int*int) (terrain:Terrain) : EffectInstance list =
        match terrain with
        | Terrain.Bush bush                     -> bush |> EffectInstance.doBushTimers random position
        | Terrain.Clear (Dirt x)                -> x |> EffectInstance.doDirtGrassTimer position
        | Terrain.Spawner (Rabbit,x)            -> x |> EffectInstance.doRabbitHoleSpawnTimer random position
        | Terrain.Seed (Seed.Berry,growthTimer) -> growthTimer |> EffectInstance.doBerrySeedGrowthTimer random position
        | Terrain.Seed (Seed.Tree, growthTimer) -> growthTimer |> EffectInstance.doTreeSeedGrowthTimer position
        | Terrain.Tree (woodLeft,acornTimer)    -> (woodLeft, acornTimer) |> EffectInstance.doAcornTimer random position
        | Terrain.Water (fish,timer)            -> (fish,timer) ||> EffectInstance.doWaterRestockTimer random position
        | _                                     -> []

    static member doRabbitTimers (random:System.Random) (position:int*int) (atlasCell:AtlasCell) (rabbit:RabbitDescriptor) : EffectInstance list =
        match rabbit.moveTimer with
        | 0u -> 
            [EffectInstance.create position (Creature.resetMoveTimer |> Effect.UpdateCreature);

            EffectInstance.create position ([(Direction.North,1u);(Direction.East,1u);(Direction.South,1u);(Direction.West,1u);] 
            |> Map.ofList 
            |> Generator.generate random 
            |> Effect.MoveCreature)]

        | _ -> 
            match atlasCell.items.table |> Map.tryPick (fun k v -> if k.isTrap && v>0u then Some k else None) with
            | Some x ->
                [EffectInstance.create position Effect.RemoveCreature; 
                EffectInstance.create position ((1u, x) |> Effect.RemoveItem); 
                EffectInstance.create position ((1u, Item.Carcass Carcass.Rabbit) |> Effect.AddItem)]
            | _ -> 
                [EffectInstance.create position (Creature.decreaseMoveTimer |> Effect.UpdateCreature)] 

    static member doCreatureTimers (random:System.Random) (position:int*int) (cell:AtlasCell) : EffectInstance list =
        match cell.creature with
        | Some (Creature.Rabbit r) -> r |> EffectInstance.doRabbitTimers random position cell
        | _ -> []

    static member doAtlasCellTimers (random:Random) (position:int*int) (atlasCell:AtlasCell) : EffectInstance list =
        (atlasCell |> EffectInstance.doCreatureTimers random position)
        |> List.append (atlasCell.terrain |> EffectInstance.doTimers random position)



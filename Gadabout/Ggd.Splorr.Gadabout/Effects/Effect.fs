﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Effect =
    | Terrain of Terrain
    | SpawnCreature of Creature
    | UpdateCreature of (Creature->Creature option)
    | RemoveCreature
    | MoveCreature of Direction
    | AddItem of uint32 * Item
    | RemoveItem of uint32 * Item
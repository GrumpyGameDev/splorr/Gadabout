﻿namespace Ggd.Splorr.Gadabout

open System

module AtlasCell =
    [<RequireQualifiedAccess>]
    type GeneratedTerrainType =
        | Grass
        | Tree
        | Rock
        | Flintrock
        | Water
        | Bush
        | RabbitHole
        | Malachite
        | Cassiterite
        | Bauxite
        | MithrilOre
        | UnobtainiumOre
        | Trader
        | Quest

    let terrainSpawner:Map<GeneratedTerrainType, Random->Terrain> =
        [(GeneratedTerrainType.Grass, Terrain.createGrass);
        (GeneratedTerrainType.Tree,Terrain.createTree);
        (GeneratedTerrainType.Rock,Terrain.createRock);
        (GeneratedTerrainType.Flintrock,Terrain.createFlintrock);
        (GeneratedTerrainType.Water,Terrain.createWater);
        (GeneratedTerrainType.Malachite,Terrain.createMalachite);
        (GeneratedTerrainType.Cassiterite,Terrain.createCassiterite);
        (GeneratedTerrainType.Bauxite,Terrain.createBauxite);
        (GeneratedTerrainType.MithrilOre,Terrain.createMithrilOre);
        (GeneratedTerrainType.UnobtainiumOre,Terrain.createUnobtainiumOre);
        (GeneratedTerrainType.RabbitHole,Terrain.createRabbitHole);
        (GeneratedTerrainType.Trader,Terrain.createTrader);
        (GeneratedTerrainType.Quest,Terrain.createQuest);
        (GeneratedTerrainType.Bush, (fun x -> x|> BushDescriptor.create |> Terrain.Bush))]
        |> Map.ofList

    let terrainTypeGenerator =
        [(GeneratedTerrainType.Grass,        80000u);
        (GeneratedTerrainType.Tree,          20000u);
        (GeneratedTerrainType.Rock,           5000u);
        (GeneratedTerrainType.Bush,            250u);
        (GeneratedTerrainType.RabbitHole,      100u);
        (GeneratedTerrainType.Malachite,       100u);
        (GeneratedTerrainType.Cassiterite,      50u);
        (GeneratedTerrainType.Bauxite,          25u);
        (GeneratedTerrainType.MithrilOre,       10u);
        (GeneratedTerrainType.UnobtainiumOre,    5u);
        (GeneratedTerrainType.Flintrock,       500u);
        (GeneratedTerrainType.Trader,           50u);//50
        (GeneratedTerrainType.Quest,            10u);
        (GeneratedTerrainType.Water,          2500u)]
        |> Map.ofList


type AtlasCell =
    {terrain:Terrain;
    items:Inventory;
    creature:Creature option}
    static member create (terrain:Terrain) : AtlasCell =
        {terrain=terrain; items={table=Map.empty}; creature = None}

    static member spawn (random:Random) : AtlasCell =
        let terrainType = AtlasCell.terrainTypeGenerator |> Generator.generate random;
        {terrain=random |> AtlasCell.terrainSpawner.[terrainType];items={table=Map.empty};creature=None}

    static member createTagon (random:Random) : AtlasCell =
        {terrain=Terrain.Clear Grass; items ={table= Map.empty}; creature = TagonDescriptor.create(random) |> Creature.Tagon |> Some}

    member atlasCell.canEnter : bool =
        match atlasCell.creature, atlasCell.terrain with
        | (Some _, _) -> false
        | (None, terrain) -> terrain.canEnter

    member atlasCell.placeCreature (creature:Creature option) : AtlasCell =
        {atlasCell with creature = creature}

    member atlasCell.placeTerrain (terrain:Terrain) : AtlasCell =
        {atlasCell with terrain = terrain}

    member atlasCell.getItemCount (item:Item) : uint32 =
        match atlasCell.items.table |> Map.tryFind item with
        | Some x -> x
        | _ -> 0u

    member atlasCell.addItems (item:Item) (count:uint32) : AtlasCell =
        {atlasCell with items = atlasCell.items.addItemCount item (count + (atlasCell.getItemCount item))}

    member atlasCell.removeItems (item:Item) (count:uint32) : AtlasCell =
        {atlasCell with items = atlasCell.items.removeItemCount item count}

    member atlasCell.clearCreature = atlasCell.placeCreature None

    member atlasCell.finalizeCreature : AtlasCell =
        match atlasCell.creature with
        | Some (Creature.Moved creature) -> 
            {atlasCell with creature = creature |> Some}.finalizeCreature
        | Some (Creature.Spawned creature) -> 
            {atlasCell with creature = creature |> Some}.finalizeCreature
        | Some (Creature.Removed _) -> 
            {atlasCell with creature = None}
        | _ -> atlasCell

    member atlasCell.initializeOnGroundScroller (listSize:uint32) : Scroller =
        let itemCount = 
            atlasCell.items.visibleTable
            |> Map.fold (fun acc _ v -> if v > 0u then acc + 1u else acc) 0u
        Scroller.initialize itemCount listSize

    member atlasCell.getItems : Inventory =
        atlasCell.items

    member atlasCell.tryGetScrollerItem (scroller:Scroller) = (atlasCell.getItems).tryGetScrollerItem scroller

    member atlasCell.getScrollerItem (scroller:Scroller) = atlasCell.tryGetScrollerItem scroller |> Option.get

    member atlasCell.getScrollableItemCount = (atlasCell.getItems).getScrollableItemCount

type Fetcher = (int*int) -> AtlasCell


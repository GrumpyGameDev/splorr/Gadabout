﻿namespace Ggd.Splorr.Gadabout

open System

[<RequireQualifiedAccess>]
type Verb =
    | Look
    | PickBerry
    | TakeLog
    | Open
    | Shut
    | EatBerry of Berry
    | PlaceTerrain of Terrain
    | UseAxe of ToolMaterial
    | UsePick of ToolMaterial
    | UseShovel of ToolMaterial
    | UseSpear of ToolMaterial
    | UseHammer of ToolMaterial
    | UseHoe of ToolMaterial
    | Plant of Plant
    | UseRawMeat
    | UseCookedMeat
    | UseTrap
    | UseBoneMeal
    member x.name =
        match x with
        | Look           -> "Look"
        | PickBerry      -> "Pick Berry"
        | TakeLog        -> "Take Log"
        | Open           -> "Open"
        | Shut           -> "Shut"
        | EatBerry x     -> x.name |> sprintf "Eat %s"
        | PlaceTerrain x -> x.name |> sprintf "Place %s"
        | UseAxe x       -> x.adjective |> sprintf "Use %s Axe"
        | UsePick x      -> x.adjective |> sprintf "Use %s Pick"
        | UseShovel x    -> x.adjective |> sprintf "Use %s Shovel"
        | UseSpear x     -> x.adjective |> sprintf "Use %s Spear"
        | UseHammer x    -> x.adjective |> sprintf "Use %s Hammer"
        | UseHoe x       -> x.adjective |> sprintf "Use %s Hoe"
        | Plant x        -> x.name |> sprintf "Plant %s"
        | UseRawMeat     -> "Use Raw Meat"
        | UseCookedMeat  -> "Use Cooked Meat"
        | UseTrap        -> "Use Trap"
        | UseBoneMeal    -> "Use Bone Meal"
    member x.description =
        match x with 
        | Look           -> "Examines terrain in the interactive cell"
        | PickBerry      -> "Picks a berry from the interactive cell"
        | TakeLog        -> "Takes a log from the interactive cell"
        | Open           -> "Opens the interactive cell"
        | Shut           -> "Shuts the interactive cell"
        | EatBerry x     -> x.name |> sprintf "Eats a(n) %s"
        | PlaceTerrain x -> x.name |> sprintf "Places a(n) %s"
        | UseAxe x       -> x.adjective |> sprintf "Uses a(n) %s axe to gather wood from the interactive cell"
        | UsePick x      -> x.adjective |> sprintf "Uses a(n) %s pick to gather stone or ore from the interactive cell"
        | UseShovel x    -> x.adjective |> sprintf "Uses a(n) %s shovel to clear the interactive cell"
        | UseSpear x     -> x.adjective |> sprintf "Uses a(n) %s spear to hunt on the interactive cell"
        | UseHammer x    -> x.adjective |> sprintf "Uses a(n) %s hammer to hit something on the interactive cell"
        | UseHoe x       -> x.adjective |> sprintf "Uses a(n) %s hoe to till something on the interactive cell"
        | Plant x        -> x.name |> sprintf "Plants a %s in the interactive cell"
        | UseRawMeat     -> "Eats a piece of raw meat"
        | UseCookedMeat  -> "Eats a piece of cooked meat"
        | UseTrap        -> "Uses a trap on the interactive cell"
        | UseBoneMeal    -> "Uses bone meal to fertilize on the interactive cell"

    static member getPreferredVerbs (terrain:Terrain) : Verb list =
        match terrain with
        | Terrain.Bush _ -> 
            [Verb.PickBerry; 
            Verb.Look]
        | Terrain.Tree _ -> 
            [Verb.UseAxe ToolMaterial.Wood; 
            Verb.UseAxe ToolMaterial.Stone; 
            Verb.UseAxe ToolMaterial.Flint; 
            Verb.UseAxe ToolMaterial.Copper; 
            Verb.UseAxe ToolMaterial.Bronze; 
            Verb.UseAxe ToolMaterial.Iron; 
            Verb.UseAxe ToolMaterial.Mithril; 
            Verb.UseAxe ToolMaterial.Unobtanium; 
            Verb.TakeLog ; 
            Verb.Look]
        | Terrain.OreRock _
        | Terrain.Rock _
        | Terrain.Flintrock _ -> 
            [Verb.UsePick ToolMaterial.Wood; 
            Verb.UsePick ToolMaterial.Stone; 
            Verb.UsePick ToolMaterial.Flint; 
            Verb.UsePick ToolMaterial.Copper; 
            Verb.UsePick ToolMaterial.Bronze; 
            Verb.UsePick ToolMaterial.Iron; 
            Verb.UsePick ToolMaterial.Mithril; 
            Verb.UsePick ToolMaterial.Unobtanium; 
            Verb.Look]
        | Terrain.Water _ -> 
            [Verb.UseSpear ToolMaterial.Wood; 
            Verb.UseSpear ToolMaterial.Stone; 
            Verb.UseSpear ToolMaterial.Flint; 
            Verb.UseSpear ToolMaterial.Copper; 
            Verb.UseSpear ToolMaterial.Bronze; 
            Verb.UseSpear ToolMaterial.Iron; 
            Verb.UseSpear ToolMaterial.Mithril; 
            Verb.UseSpear ToolMaterial.Unobtanium; 
            Verb.Look]
        | Terrain.Door true -> 
            [Verb.Open; 
            Verb.Look]
        | Terrain.Door false -> 
            [Verb.Shut; 
            Verb.Look]
        | Terrain.Clear Rubble
        | Terrain.Clear Stump -> 
            [Verb.UseShovel ToolMaterial.Wood; 
            Verb.UseShovel ToolMaterial.Stone; 
            Verb.UseShovel ToolMaterial.Flint; 
            Verb.UseShovel ToolMaterial.Copper; 
            Verb.UseShovel ToolMaterial.Bronze; 
            Verb.UseShovel ToolMaterial.Iron; 
            Verb.UseShovel ToolMaterial.Mithril; 
            Verb.UseShovel ToolMaterial.Unobtanium; 
            Verb.Look]
        | _ -> 
            [Verb.Look]
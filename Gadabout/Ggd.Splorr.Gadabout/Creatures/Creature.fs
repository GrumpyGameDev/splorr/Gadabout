﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Creature =
    | Tagon of TagonDescriptor
    | Rabbit of RabbitDescriptor
    | Spawned of Creature
    | Removed of Creature
    | Moved of Creature
    static member decreaseMoveTimer (creature:Creature) : Creature option=
        match creature with
        | Creature.Rabbit r -> {r with moveTimer=r.moveTimer-1u} |> Creature.Rabbit |> Some
        | _ -> creature |> Some
    
    static member resetMoveTimer (creature:Creature) : Creature option=
        match creature with
        | Creature.Rabbit r -> {r with moveTimer=2u} |> Creature.Rabbit |> Some
        | _ -> creature |> Some

    member creature.getPattern : Hue * Pattern =
        match creature with
        | Creature.Tagon t -> (t.hue, Pattern.Tagon)
        | Creature.Rabbit _ -> (Hue.Turquoise, Pattern.Rabbit)
        | Creature.Moved x -> x.getPattern
        | Creature.Spawned x -> x.getPattern
        | Creature.Removed _ -> (Hue.Onyx, Pattern.Empty)
    
    member creature.tryGetRabbitDescriptor : RabbitDescriptor option =
        match creature with
        | Creature.Rabbit t -> t |> Some
        | _ -> None

    member creature.getRabbitDescriptor = creature.tryGetRabbitDescriptor |> Option.get

    member creature.canDoVerb (terrains:Terrain * Terrain) (verbDescriptor:VerbDescriptor) : bool =
        match creature with
        | Creature.Tagon t -> t.canDoVerb terrains verbDescriptor
        | _ -> false

    member creature.mightDoVerb (verbDescriptor:VerbDescriptor) : bool =
        match creature with
        | Creature.Tagon t -> t.mightDoVerb verbDescriptor
        | _ -> false

    member creature.getVerbs (terrains:Terrain * Terrain) (verbs:VerbDescriptor list) : (bool*VerbDescriptor) list =
        ([], verbs)
        ||> List.fold 
            (fun output verb -> 
                if creature.canDoVerb terrains verb then 
                    output |> List.append [(true,verb)] 
                else if creature.mightDoVerb verb && verb.item.IsSome then
                    output |> List.append [(false,verb)]
                else
                    output)

    member creature.tryGetTagonDescriptor : TagonDescriptor option =
        match creature with
        | Creature.Tagon t -> t |> Some
        | _ -> None

    member creature.getTagonDescriptor = creature.tryGetTagonDescriptor |> Option.get

    static member upkeep (creature:Creature option) : Creature option=
        match creature with
        | Some (Creature.Tagon t) -> t.upkeep |> Creature.Tagon |> Some
        | _ -> creature

    static member setExertion (exertion:float) (creature:Creature option) : Creature option=
        match creature with
        | Some (Creature.Tagon t) -> t.setExertion exertion |> Creature.Tagon |> Some
        | _ -> creature

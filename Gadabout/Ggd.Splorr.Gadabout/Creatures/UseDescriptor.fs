﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type UseDescriptor =
    | Nothing
    | HealEnergy of float
    | HealHunger of float
    member x.description =
        match x with
        | Nothing -> ""
        | HealEnergy _ -> "Heals Energy"
        | HealHunger _ -> "Heals Hunger"
﻿namespace Ggd.Splorr.Gadabout

type TagonDescriptor =

    {inventory:Inventory;
    statistics:Statistics;
    hue:Hue}

    member tagon.isDead : bool =
        tagon.statistics.table.ContainsKey(Statistic.Health) && (tagon.statistics.table.[Statistic.Health].current = tagon.statistics.table.[Statistic.Health].minimum)

    static member create (random:System.Random) :TagonDescriptor =
        {inventory={table=Map.empty};
        statistics={table=Map.empty};
        hue = [(Hue.SuperDark,1u);(Hue.Darkest,2u);(Hue.Darker,3u);(Hue.Dark,4u);(Hue.MediumDark,5u);(Hue.Medium,6u);(Hue.MediumLight,5u);(Hue.Light,4u);(Hue.Lighter,3u);(Hue.Lightest,2u);(Hue.SuperLight,1u);] |> Map.ofList |> Generator.generate random}

    member tagon.hasItem (item:Item) : bool =
        match tagon.inventory.table.TryFind item with
        | Some _ -> true
        | _ -> false

    member private tagon.getStatistics : Statistics =
        tagon.statistics

    member x.getCurrentStatistic (statistic:Statistic) = x.getStatistics.getCurrent statistic

    member tagon.updateInventory (transform:Inventory->Inventory) : TagonDescriptor =
        {tagon with inventory = tagon.inventory |> transform}

    member tagon.updateStatistics (transform:Statistics->Statistics) : TagonDescriptor =
        {tagon with statistics = tagon.statistics |> transform}

    member private tagon.setCurrentStatistic (statistic:Statistic) (value:float) = tagon.updateStatistics (fun x -> x.setCurrent statistic value)

    member tagon.setExertion = tagon.setCurrentStatistic Statistic.Exertion

    member tagon.getInventory : Inventory =
        tagon.inventory

    member tagon.getItemCount (item:Item) = 
        tagon.getInventory.getItemCount item

    member tagon.addItemCount (item:Item) (count:uint32) = tagon.updateInventory (fun x->x.addItemCount item count)

    member tagon.removeItemCount (item:Item) (count:uint32) = tagon.updateInventory (fun x->x.removeItemCount item count)

    member tagon.removeOptionalItem (item:Item option) : TagonDescriptor =
        (tagon, item)
        ||> Option.fold (fun acc v -> acc.removeItemCount v 1u)

    member tagon.addOptionalItems (items:(Item*uint32) option) : TagonDescriptor =
        (tagon, items)
        ||> Option.fold (fun acc v -> acc.addItemCount (v|>fst) (v|>snd))

    //"might" means that tagon has the necessary equipment, so it is possible
    member tagon.mightDoVerb (verbDescriptor:VerbDescriptor) : bool =
        match verbDescriptor, verbDescriptor.item with
        | descriptor, None -> 
            (descriptor.energy <= (tagon.getCurrentStatistic Statistic.Energy))
        | descriptor, Some item ->
            (descriptor.energy <= (tagon.getCurrentStatistic Statistic.Energy)) && (tagon.hasItem item)

    member tagon.canDoVerb (terrains:Terrain * Terrain) (verbDescriptor:VerbDescriptor) : bool =
        match verbDescriptor, verbDescriptor.item with
        | descriptor, None -> 
            (descriptor.energy <= (tagon.getCurrentStatistic Statistic.Energy)) && (terrains |> fst |> descriptor.canInteract)
        | descriptor, Some item ->
            (descriptor.energy <= (tagon.getCurrentStatistic Statistic.Energy)) && (tagon.hasItem item) && (terrains |> fst |> descriptor.canInteract)

    member tagon.changeCurrentStatisticBy (statistic:Statistic) (change:float) : TagonDescriptor =
        tagon.updateStatistics (fun x -> x.changeCurrentBy statistic change)

    member tagon.addEnergy = tagon.changeCurrentStatisticBy Statistic.Energy

    member tagon.addStomach = tagon.changeCurrentStatisticBy Statistic.Hunger

    member tagon.spendEnergy (energy: float) = tagon.addEnergy (-energy)

    member tagon.upkeep =
        let regeneration =
            match tagon.getCurrentStatistic Statistic.Hunger with
            | x when x < 0.0 -> x / 100.0
            | x when x > 100.0 -> (x - 100.0) / 100.0
            | _ -> 0.0
        (tagon.changeCurrentStatisticBy Statistic.Hunger (tagon.getCurrentStatistic Statistic.Exertion)).changeCurrentStatisticBy Statistic.Health regeneration

    member tagon.initializeInHandScroller (listSize:uint32) =
        listSize 
        |> Scroller.initialize (tagon.inventory.visibleTable |> Map.fold (fun a _ v -> if v>0u then a + 1u else a) 0u)

    member tagon.tryGetScrollerItem (scroller:Scroller) = (tagon.getInventory).tryGetScrollerItem scroller

    member tagon.getScrollerItem (scroller:Scroller) = tagon.tryGetScrollerItem scroller |> Option.get

    member tagon.getScrollableItemCount = (tagon.getInventory).getScrollableItemCount

    member tagon.craftRecipe (recipe:Recipe) : TagonDescriptor =
        tagon.updateInventory (fun x-> (x.subtract recipe.input).add recipe.output)



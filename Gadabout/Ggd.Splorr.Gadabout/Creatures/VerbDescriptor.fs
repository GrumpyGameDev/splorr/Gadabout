﻿namespace Ggd.Splorr.Gadabout

type VerbDescriptor =
    {
    verb:Verb;
    pattern:Pattern;
    hue:Hue;
    caption:string;
    item:Item option;
    energy:float;
    canInteract:Terrain->bool;
    useDescriptor:UseDescriptor;
    insufficientEnergyMessage:Hue*string;
    exertion:float;
    ordinal:float}
    static member noExertion = 0.0
    static member trivialExertion = 0.05
    static member veryLightExertion = (-1.0)
    static member lightExertion = (-2.0)
    static member moderateExertion = (-3.0)
    static member heavyExertion = (-4.0)
    static member veryHeavyExertion = (-5.0)
    static member noEnergyCost = 0.0
    static member standardEnergyCost = 1.0

    static member private genericInsufficientEnergyMessage = (Hue.Carnelian,"Not enough energy!")

    static member private makeUseAxe (t:ToolMaterial) (h:Hue) (o:float): VerbDescriptor =
            {verb=Verb.UseAxe t;
            caption="Chop..."; 
            pattern=Pattern.Axe; 
            hue=h; 
            item=(Tool.Axe, t) |> Item.Tool |> Some; 
            energy=VerbDescriptor.standardEnergyCost;
            canInteract=(Terrain.canChop); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.moderateExertion;
            ordinal=o}

    static member private makeUsePick (t:ToolMaterial) (h:Hue) (o:float): VerbDescriptor =
            {verb=Verb.UsePick t;
            caption="Mine..."; 
            pattern=Pattern.PickAxe; 
            hue=h; 
            item=(Tool.Pick, t) |> Item.Tool |> Some; 
            energy=VerbDescriptor.standardEnergyCost;
            canInteract=(Terrain.canMine); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.moderateExertion;
            ordinal=o}

    static member private makeUseShovel (t:ToolMaterial) (h:Hue) (o:float): VerbDescriptor =
            {verb=Verb.UseShovel t;
            caption="Dig..."; 
            pattern=Pattern.Shovel; 
            hue=h; 
            item=(Tool.Shovel, t) |> Item.Tool |> Some; 
            energy=VerbDescriptor.standardEnergyCost;
            canInteract=(Terrain.canShovel); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.moderateExertion;
            ordinal=o}

    static member private makeUseSpear (t:ToolMaterial) (h:Hue) (o:float): VerbDescriptor =
            {verb=Verb.UseSpear t;
            caption="Spear..."; 
            pattern=Pattern.Spear; 
            hue=h; 
            item=(Tool.Spear, t) |> Item.Tool |> Some; 
            energy=VerbDescriptor.standardEnergyCost;
            canInteract=(Terrain.canSpear); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.veryLightExertion;
            ordinal=o}

    static member private makeUseHammer(t:ToolMaterial) (h:Hue) (o:float): VerbDescriptor =
            {verb=Verb.UseHammer t;
            caption="Hammer..."; 
            pattern=Pattern.Hammer; 
            hue=h; 
            item=(Tool.Hammer, t) |> Item.Tool |> Some; 
            energy=VerbDescriptor.standardEnergyCost;
            canInteract=(Terrain.canHammer); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.lightExertion;
            ordinal=o}

    static member private makeUseHoe(t:ToolMaterial) (h:Hue) (o:float): VerbDescriptor =
            {verb=Verb.UseHoe t;
            caption="Till..."; 
            pattern=Pattern.Hoe; 
            hue=h; 
            item=(Tool.Hoe, t) |> Item.Tool |> Some; 
            energy=VerbDescriptor.standardEnergyCost;
            canInteract=(Terrain.canHoe); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.heavyExertion;
            ordinal=o}

    static member descriptors : VerbDescriptor list =
        [
            {verb=Verb.Look; 
            caption="Look..."; 
            pattern=Pattern.Eye; 
            hue=Hue.Silver; 
            item=None; 
            energy=VerbDescriptor.noEnergyCost; 
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=0.0}

            {verb=Verb.PickBerry;
            caption="Forage..."; 
            pattern=Pattern.Bush; 
            hue=Hue.DarkJade; 
            item=None; 
            energy=VerbDescriptor.noEnergyCost; 
            canInteract=(Terrain.isBush); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.trivialExertion;
            ordinal=0.1}

            {verb=Verb.TakeLog;
            caption="Take Log..."; 
            pattern=Pattern.Log; 
            hue=Hue.DarkCopper; 
            item=None; 
            energy=VerbDescriptor.standardEnergyCost;
            canInteract=(Terrain.isTree); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.lightExertion;
            ordinal=0.2}

            {verb=Verb.Open;
            caption="Open..."; 
            pattern=Pattern.RightHand; 
            hue=Hue.Medium; 
            item=None; 
            energy=VerbDescriptor.standardEnergyCost;
            canInteract=(Terrain.isOpenable); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.veryLightExertion;
            ordinal=0.3}

            {verb=Verb.Shut;
            caption="Shut..."; 
            pattern=Pattern.RightHand; 
            hue=Hue.Medium; 
            item=None; 
            energy=VerbDescriptor.standardEnergyCost;
            canInteract=(Terrain.isShuttable); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.veryLightExertion;
            ordinal=0.31}

            /////////////////////////////////////////////////////////////////////////
            // Axes
            /////////////////////////////////////////////////////////////////////////

            (VerbDescriptor.makeUseAxe ToolMaterial.Wood       Hue.DarkCopper   2.0)
            (VerbDescriptor.makeUseAxe ToolMaterial.Stone      Hue.DarkSilver   2.01)
            (VerbDescriptor.makeUseAxe ToolMaterial.Flint      Hue.DarkerCopper 2.02)
            (VerbDescriptor.makeUseAxe ToolMaterial.Copper     Hue.Copper       2.03)
            (VerbDescriptor.makeUseAxe ToolMaterial.Bronze     Hue.MediumCopper 2.04)
            (VerbDescriptor.makeUseAxe ToolMaterial.Iron       Hue.MediumSilver 2.05)
            (VerbDescriptor.makeUseAxe ToolMaterial.Mithril    Hue.Ruby         2.06)
            (VerbDescriptor.makeUseAxe ToolMaterial.Unobtanium Hue.Amethyst     2.07)

            /////////////////////////////////////////////////////////////////////////
            // Picks
            /////////////////////////////////////////////////////////////////////////

            (VerbDescriptor.makeUsePick ToolMaterial.Wood       Hue.DarkCopper   2.1)
            (VerbDescriptor.makeUsePick ToolMaterial.Stone      Hue.DarkSilver   2.11)
            (VerbDescriptor.makeUsePick ToolMaterial.Flint      Hue.DarkerCopper 2.12)
            (VerbDescriptor.makeUsePick ToolMaterial.Copper     Hue.Copper       2.13)
            (VerbDescriptor.makeUsePick ToolMaterial.Bronze     Hue.MediumCopper 2.14)
            (VerbDescriptor.makeUsePick ToolMaterial.Iron       Hue.MediumSilver 2.15)
            (VerbDescriptor.makeUsePick ToolMaterial.Mithril    Hue.Ruby         2.16)
            (VerbDescriptor.makeUsePick ToolMaterial.Unobtanium Hue.Amethyst     2.17)

            /////////////////////////////////////////////////////////////////////////
            // Shovels
            /////////////////////////////////////////////////////////////////////////

            (VerbDescriptor.makeUseShovel ToolMaterial.Wood       Hue.DarkCopper   2.2)
            (VerbDescriptor.makeUseShovel ToolMaterial.Stone      Hue.DarkSilver   2.21)
            (VerbDescriptor.makeUseShovel ToolMaterial.Flint      Hue.DarkerCopper 2.22)
            (VerbDescriptor.makeUseShovel ToolMaterial.Copper     Hue.Copper       2.23)
            (VerbDescriptor.makeUseShovel ToolMaterial.Bronze     Hue.MediumCopper 2.24)
            (VerbDescriptor.makeUseShovel ToolMaterial.Iron       Hue.MediumSilver 2.25)
            (VerbDescriptor.makeUseShovel ToolMaterial.Mithril    Hue.Ruby         2.26)
            (VerbDescriptor.makeUseShovel ToolMaterial.Unobtanium Hue.Amethyst     2.27)

            /////////////////////////////////////////////////////////////////////////
            // Spears
            /////////////////////////////////////////////////////////////////////////

            (VerbDescriptor.makeUseSpear ToolMaterial.Wood       Hue.DarkCopper   2.3)
            (VerbDescriptor.makeUseSpear ToolMaterial.Stone      Hue.DarkSilver   2.31)
            (VerbDescriptor.makeUseSpear ToolMaterial.Flint      Hue.DarkerCopper 2.32)
            (VerbDescriptor.makeUseSpear ToolMaterial.Copper     Hue.Copper       2.33)
            (VerbDescriptor.makeUseSpear ToolMaterial.Bronze     Hue.MediumCopper 2.34)
            (VerbDescriptor.makeUseSpear ToolMaterial.Iron       Hue.MediumSilver 2.35)
            (VerbDescriptor.makeUseSpear ToolMaterial.Mithril    Hue.Ruby         2.36)
            (VerbDescriptor.makeUseSpear ToolMaterial.Unobtanium Hue.Amethyst     2.37)

            /////////////////////////////////////////////////////////////////////////
            // Hammers
            /////////////////////////////////////////////////////////////////////////

            (VerbDescriptor.makeUseHammer ToolMaterial.Wood       Hue.DarkCopper   2.4)
            (VerbDescriptor.makeUseHammer ToolMaterial.Stone      Hue.DarkSilver   2.41)
            (VerbDescriptor.makeUseHammer ToolMaterial.Flint      Hue.DarkerCopper 2.42)
            (VerbDescriptor.makeUseHammer ToolMaterial.Copper     Hue.Copper       2.43)
            (VerbDescriptor.makeUseHammer ToolMaterial.Bronze     Hue.MediumCopper 2.44)
            (VerbDescriptor.makeUseHammer ToolMaterial.Iron       Hue.MediumSilver 2.45)
            (VerbDescriptor.makeUseHammer ToolMaterial.Mithril    Hue.Ruby         2.46)
            (VerbDescriptor.makeUseHammer ToolMaterial.Unobtanium Hue.Amethyst     2.47)

            /////////////////////////////////////////////////////////////////////////
            // Hoes
            /////////////////////////////////////////////////////////////////////////

            (VerbDescriptor.makeUseHoe ToolMaterial.Wood       Hue.DarkCopper   2.5)
            (VerbDescriptor.makeUseHoe ToolMaterial.Stone      Hue.DarkSilver   2.51)
            (VerbDescriptor.makeUseHoe ToolMaterial.Flint      Hue.DarkerCopper 2.52)
            (VerbDescriptor.makeUseHoe ToolMaterial.Copper     Hue.Copper       2.53)
            (VerbDescriptor.makeUseHoe ToolMaterial.Bronze     Hue.MediumCopper 2.54)
            (VerbDescriptor.makeUseHoe ToolMaterial.Iron       Hue.MediumSilver 2.55)
            (VerbDescriptor.makeUseHoe ToolMaterial.Mithril    Hue.Ruby         2.56)
            (VerbDescriptor.makeUseHoe ToolMaterial.Unobtanium Hue.Amethyst     2.57)

            /////////////////////////////////////////////////////////////////////////
            // Berries
            /////////////////////////////////////////////////////////////////////////

            {verb=Verb.EatBerry Berry.Carberry;
            caption="Eat Carberry"; 
            pattern=Pattern.Berries; 
            hue=Hue.Carnelian; 
            item=Berry.Carberry |> Item.Berry |> Some; 
            energy=VerbDescriptor.noEnergyCost;
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealEnergy 5.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=5.0};

            {verb=Verb.EatBerry Berry.Copberry;
            caption="Eat Copberry"; 
            pattern=Pattern.Berries; 
            hue=Hue.Copper; 
            item=Berry.Copberry |> Item.Berry |> Some; 
            energy=VerbDescriptor.noEnergyCost;
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealEnergy 10.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=5.1};

            {verb=Verb.EatBerry Berry.Golberry;
            caption="Eat Golberry"; 
            pattern=Pattern.Berries; 
            hue=Hue.Gold; 
            item=Berry.Golberry |> Item.Berry |> Some; 
            energy=VerbDescriptor.noEnergyCost;
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealEnergy 15.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=5.2};

            {verb=Verb.EatBerry Berry.Jadberry;
            caption="Eat Jadberry"; 
            pattern=Pattern.Berries; 
            hue=Hue.Jade; 
            item=Berry.Jadberry |> Item.Berry |> Some; 
            energy=VerbDescriptor.noEnergyCost; 
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealEnergy 20.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=5.3};

            {verb=Verb.EatBerry Berry.Turberry;
            caption="Eat Turberry"; 
            pattern=Pattern.Berries; 
            hue=Hue.Turquoise; 
            item=Berry.Turberry |> Item.Berry |> Some; 
            energy=VerbDescriptor.noEnergyCost; 
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealEnergy 25.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=5.4};

            {verb=Verb.EatBerry Berry.Ruberry;
            caption="Eat Ruberry"; 
            pattern=Pattern.Berries; 
            hue=Hue.Ruby; 
            item=Berry.Ruberry |> Item.Berry |> Some; 
            energy=VerbDescriptor.noEnergyCost; 
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealEnergy 30.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=5.5};

            {verb=Verb.EatBerry Berry.Amberry;
            caption="Eat Amberry"; 
            pattern=Pattern.Berries; 
            hue=Hue.Amethyst; 
            item=Berry.Amberry |> Item.Berry |> Some; 
            energy=VerbDescriptor.noEnergyCost; 
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealEnergy 40.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=5.6};

            {verb=Verb.EatBerry Berry.Silberry;
            caption="Eat Silberry"; 
            pattern=Pattern.Berries; 
            hue=Hue.Silver; 
            item=Berry.Silberry |> Item.Berry |> Some; 
            energy=VerbDescriptor.noEnergyCost; 
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealEnergy 45.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=5.7};

            /////////////////////////////////////////////////////////////////////////
            // Planting...
            /////////////////////////////////////////////////////////////////////////

            {verb=Verb.Plant Plant.Berry;
            caption="Plant Berry Seed..."; 
            pattern=Pattern.BerrySeed; 
            hue=Hue.DarkCopper; 
            item=Seed.Berry |> Item.Seed |> Some; 
            energy=VerbDescriptor.standardEnergyCost; 
            canInteract=(Terrain.canPlant Plant.Berry); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.veryLightExertion;
            ordinal=6.0};

            {verb=Verb.Plant Plant.Tree;
            caption="Plant Acorn..."; 
            pattern=Pattern.Acorn; 
            hue=Hue.DarkCopper; 
            item=Seed.Tree |> Item.Seed |> Some; 
            energy=VerbDescriptor.standardEnergyCost; 
            canInteract=(Terrain.canPlant Plant.Tree); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.veryLightExertion;
            ordinal=6.01};

            {verb=Verb.UseBoneMeal;
            caption="Use Bone Meal..."; 
            pattern=Pattern.Pile; 
            hue=Hue.Silver; 
            item=Item.BoneMeal |> Some; 
            energy=VerbDescriptor.standardEnergyCost; 
            canInteract=(Terrain.canUseBoneMeal); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.veryLightExertion;
            ordinal=6.1};

            /////////////////////////////////////////////////////////////////////////
            // Meat...
            /////////////////////////////////////////////////////////////////////////
            
            {verb=Verb.UseRawMeat;
            caption="Raw Meat"; 
            pattern=Pattern.Meat; 
            hue=Hue.Amethyst; 
            item=false |> Item.Meat |> Some; 
            energy=VerbDescriptor.noEnergyCost;  
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealHunger 5.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=7.0};

            {verb=Verb.UseCookedMeat;
            caption="Cooked Meat"; 
            pattern=Pattern.Meat; 
            hue=Hue.DarkAmethyst; 
            item=true |> Item.Meat |> Some; 
            energy=VerbDescriptor.noEnergyCost;  
            canInteract=(Terrain.alwaysInteract); 
            useDescriptor=UseDescriptor.HealHunger 10.0;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.noExertion;
            ordinal=7.1};


            /////////////////////////////////////////////////////////////////////////
            // Placing...
            /////////////////////////////////////////////////////////////////////////

            {verb=Verb.PlaceTerrain (Terrain.Fixture (Item.CraftingBox CraftingMethod.Workbench));
            caption="Place Workbench..."; 
            pattern=Pattern.Workbench; 
            hue=Hue.DarkCopper; 
            item=CraftingMethod.Workbench |>  Item.CraftingBox |> Some; 
            energy=VerbDescriptor.standardEnergyCost;  
            canInteract=(Terrain.isClear); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.veryHeavyExertion;
            ordinal=10.0}

            {verb=Verb.PlaceTerrain (Terrain.Fixture (Item.CraftingBox CraftingMethod.Altar));
            caption="Place Altar..."; 
            pattern=Pattern.Workbench; 
            hue=Hue.DarkSilver; 
            item=CraftingMethod.Altar |> Item.CraftingBox |> Some; 
            energy=VerbDescriptor.standardEnergyCost;  
            canInteract=(Terrain.isDirt);
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.veryHeavyExertion;
            ordinal=10.0}

            {verb=Verb.PlaceTerrain (Terrain.Clear Rubble);
            caption="Place Gravel..."; 
            pattern=Pattern.Pile; 
            hue=Hue.DarkSilver; 
            item=Item.Gravel |> Some; 
            energy=VerbDescriptor.standardEnergyCost;  
            canInteract=(Terrain.isClear); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.moderateExertion;
            ordinal=11.0}

            {verb=Verb.PlaceTerrain (Terrain.Fixture (Item.CraftingBox CraftingMethod.Furnace));
            caption="Place Furnace..."; 
            pattern=Pattern.Furnace; 
            hue=Hue.DarkSilver; 
            item=CraftingMethod.Furnace |> Item.CraftingBox |> Some; 
            energy=VerbDescriptor.standardEnergyCost; 
            canInteract=(Terrain.isDirt); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.moderateExertion;
            ordinal=12.0}

            {verb=Verb.PlaceTerrain (Terrain.Wall WallMaterial.Wood);
            caption="Place Wall..."; 
            pattern=Pattern.Wall; 
            hue=Hue.DarkCopper; 
            item=WallMaterial.Wood |> Item.Wall |> Some; 
            energy=VerbDescriptor.standardEnergyCost;  
            canInteract=(Terrain.isDirt); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.moderateExertion;
            ordinal=13.0}

            {verb=Verb.PlaceTerrain (Terrain.Wall WallMaterial.Stone);
            caption="Place Wall..."; 
            pattern=Pattern.Wall; 
            hue=Hue.DarkSilver; 
            item=WallMaterial.Stone |> Item.Wall |> Some; 
            energy=VerbDescriptor.standardEnergyCost;  
            canInteract=(Terrain.isDirt); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.moderateExertion;
            ordinal=13.01}

            {verb=Verb.PlaceTerrain (Terrain.Door true);
            caption="Place Door..."; 
            pattern=Pattern.Door; 
            hue=Hue.DarkCopper; 
            item=Item.Door |> Some; 
            energy=VerbDescriptor.standardEnergyCost;  
            canInteract=(Terrain.isDirt); 
            useDescriptor=UseDescriptor.Nothing;
            insufficientEnergyMessage=VerbDescriptor.genericInsufficientEnergyMessage;
            exertion=VerbDescriptor.moderateExertion;
            ordinal=13.1}
        ]
    
    static member getDescriptor (verb:Verb) : VerbDescriptor =
        VerbDescriptor.descriptors
        |> List.find (fun v -> v.verb=verb)

    static member getVerbs () : VerbDescriptor list =
        VerbDescriptor.descriptors





﻿namespace Ggd.Splorr.Gadabout

type Recipe =
    {input:Inventory;
    output:Inventory;
    forbidden:Inventory;
    canCraftOnTerrain:Terrain->bool;
    name:string;
    description:string}

    static member private craftIngot (fireType:Fire,ores:(Ore*uint32) list,toolMaterial:ToolMaterial, terrainFilter:Terrain->bool) =
        {
            input = ores |> List.map (fun (o,c) -> (Item.Ore o,c)) |> List.append [(Item.Fire fireType,1u)] |>Inventory.ofList
            output=[(Item.Ingot toolMaterial,1u)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=terrainFilter;
            name="Ingot";
            description=sprintf "Craft %s Ingot" toolMaterial.adjective
        }

    static member private craftBoneMeal (sourceItem) =
        {
            input=[(Item.Bone,1u);(sourceItem,1u)]|>Inventory.ofList;
            output=[(Item.BoneMeal,1u)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=fun x -> x.isWorkbench;
            name="Bone Meal";
            description="Craft bone meal"
        }

    static member private craftAxe (item,toolMaterial,output,craftFilter,name,description) =
        {
            input=[(Item.Log,2u);(item,3u)]|>Inventory.ofList;
            output=[(Item.Tool (Tool.Axe,toolMaterial),output)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=craftFilter;
            name=name;
            description=description
        }

    static member private craftPick (item,toolMaterial,output,craftFilter,name,description) =
        {
            input=[(Item.Log,2u);(item,3u)]|>Inventory.ofList;
            output=[(Item.Tool (Tool.Pick, toolMaterial),output)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=craftFilter;
            name=name;
            description=description
        }

    static member private craftSpear (item,toolMaterial,output,craftFilter,name,description) =
        {
            input=[(Item.Log,2u);(item,1u)]|>Inventory.ofList;
            output=[(Item.Tool (Tool.Spear, toolMaterial),output)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=craftFilter;
            name=name;
            description=description
        }

    static member private craftShovel (item,toolMaterial,output,craftFilter,name,description) =
        {
            input=[(Item.Log,2u);(item,1u)]|>Inventory.ofList;
            output=[(Item.Tool (Tool.Shovel, toolMaterial),output)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=craftFilter;
            name=name;
            description=description
        }

    static member private craftHammer (item,toolMaterial,output,craftFilter,name,description) =
        {
            input=[(Item.Log,2u);(item,1u)]|>Inventory.ofList;
            output=[(Item.Tool (Tool.Hammer, toolMaterial),output)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=craftFilter;
            name=name;
            description=description
        }

    static member private craftHoe (item,toolMaterial,output,craftFilter,name,description) =
        {
            input=[(Item.Log,2u);(item,2u)]|>Inventory.ofList;
            output=[(Item.Tool (Tool.Hoe, toolMaterial),output)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=craftFilter;
            name=name;
            description=description
        }

    static member private craftKnife (item,toolMaterial,output,craftFilter,name,description) =
        {
            input=[(Item.Log,1u);(item,1u)]|>Inventory.ofList;
            output=[(Item.Tool (Tool.Knife, toolMaterial),output)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=craftFilter;
            name=name;
            description=description
        }

    static member private craftWall (item, wallMaterial,name,description) =
        {
            input=[(item,9u)]|>Inventory.ofList;
            output=[(Item.Wall wallMaterial,1u)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=Terrain.isWorkbench;
            name=name;
            description=description
        }

    static member private craftBerrySeed (berry,name,description) =
        {
            input=[(Item.Berry berry,1u)]|>Inventory.ofList;
            output=[(Item.Seed Seed.Berry,1u)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=Terrain.isNotTraderOrQuest;
            name=name;
            description=description
        }

    static member private skinRabbit (toolMaterial,name,description) =
        {
            input=[(Item.Carcass Carcass.Rabbit,1u);(Item.Tool (Tool.Knife, toolMaterial),1u)]|>Inventory.ofList;
            output=[(Item.Meat false,1u);(Item.RabbitPelt,1u)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=fun x -> (x.isWorkbench || x.isAltar);
            name=name;
            description=description
        }

    static member private skinTagon (toolMaterial,name,description) =
        {
            input=[(Item.Carcass Carcass.Human,1u);(Item.Tool (Tool.Knife, toolMaterial),3u)]|>Inventory.ofList;
            output=[(Item.Meat false,10u)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=fun x -> (x.isWorkbench || x.isAltar);
            name=name;
            description=description
        }

    static member private gutFish (toolMaterial,name,description) =
        {
            input=[(Item.Carcass Carcass.Fish,1u);(Item.Tool (Tool.Knife,toolMaterial),1u)]|>Inventory.ofList;
            output=[(Item.Meat false,1u);(Item.Bone,1u)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=fun x -> (x.isWorkbench || x.isAltar);
            name=name;
            description=description
        }

    static member private simpleTrade (inputItem,inputCount,inputTokenCount,outputItem,outputCount, outputTokenCount,traderType) =
        {
            input=[(inputItem,inputCount);(Item.TradeToken traderType,inputTokenCount)]|>Inventory.ofList;
            output=[(outputItem,outputCount);(Item.TradeToken traderType,outputTokenCount)]|>Inventory.ofList;
            forbidden=[]|>Inventory.ofList;
            canCraftOnTerrain=Terrain.isTrader traderType;
            name=sprintf "%s for %s" inputItem.name outputItem.name;
            description=sprintf "%d %s for %d %s" inputCount inputItem.name outputCount outputItem.name
        }

    static member private addInput (item:Item) (count:uint32) (recipe:Recipe): Recipe =
        {recipe with input = recipe.input.addItemCount item count}

    static member private addOutput (item:Item) (count:uint32) (recipe:Recipe): Recipe =
        {recipe with output = recipe.output.addItemCount item count}

    static member private addForbidden (item:Item) (count:uint32) (recipe:Recipe): Recipe =
        {recipe with forbidden = recipe.forbidden.addItemCount item count}

    member private recipe.multiplex (itemCounts:(Item*uint32) list) : Recipe list =
        itemCounts
        |> List.map (fun (i,c) -> recipe |> Recipe.addInput i c)
    
    static member private hammersForCopper  = [Item.Tool (Tool.Hammer,ToolMaterial.Wood),1u;Item.Tool (Tool.Hammer,ToolMaterial.Stone),1u;Item.Tool (Tool.Hammer,ToolMaterial.Flint),1u;Item.Tool (Tool.Hammer,ToolMaterial.Copper),1u;Item.Tool (Tool.Hammer,ToolMaterial.Bronze),1u;Item.Tool (Tool.Hammer,ToolMaterial.Iron),1u;Item.Tool (Tool.Hammer,ToolMaterial.Mithril),1u;Item.Tool (Tool.Hammer,ToolMaterial.Unobtanium),1u]
    static member private hammersForBronze  = [Item.Tool (Tool.Hammer,ToolMaterial.Copper),1u;Item.Tool (Tool.Hammer,ToolMaterial.Bronze),1u;Item.Tool (Tool.Hammer,ToolMaterial.Iron),1u;Item.Tool (Tool.Hammer,ToolMaterial.Mithril),1u;Item.Tool (Tool.Hammer,ToolMaterial.Unobtanium),1u]
    static member private hammersForIron    = [Item.Tool (Tool.Hammer,ToolMaterial.Iron),1u;Item.Tool (Tool.Hammer,ToolMaterial.Mithril),1u;Item.Tool (Tool.Hammer,ToolMaterial.Unobtanium),1u]
    static member private hammersForMithril = [Item.Tool (Tool.Hammer,ToolMaterial.Iron),1u;Item.Tool (Tool.Hammer,ToolMaterial.Mithril),1u;Item.Tool (Tool.Hammer,ToolMaterial.Unobtanium),1u]

    static member recipes =
        [
            //////////////////////////////////////////////////////////////////////
            //Workbench
            //////////////////////////////////////////////////////////////////////

            {
                input=[(Item.Log,4u)]|>Inventory.ofList;
                output=[(Item.CraftingBox CraftingMethod.Workbench,1u)]|>Inventory.ofList;
                forbidden=[]|>Inventory.ofList;
                canCraftOnTerrain=Terrain.isNotTraderOrQuest;
                name="Workbench";
                description="Craft Workbench"
            }
            
            //////////////////////////////////////////////////////////////////////
            //Altar
            //////////////////////////////////////////////////////////////////////

            {
                input=[(Item.Ore Stone,4u)]|>Inventory.ofList;
                output=[(Item.CraftingBox CraftingMethod.Altar,1u)]|>Inventory.ofList;
                forbidden=[]|>Inventory.ofList;
                canCraftOnTerrain=fun x -> x.isWorkbench;
                name="Altar";
                description="Craft Altar"
            }
            
            //////////////////////////////////////////////////////////////////////
            //Walls
            //////////////////////////////////////////////////////////////////////

            Recipe.craftWall (Item.Log   , WallMaterial.Wood  ,"Wood Wall","Craft Wood Wall");
            Recipe.craftWall (Item.Ore Stone , WallMaterial.Stone ,"Stone Wall","Craft Stone Wall");

            //////////////////////////////////////////////////////////////////////
            //Doors
            //////////////////////////////////////////////////////////////////////

            {
                input=[(Item.Log,6u)]|>Inventory.ofList;
                output=[(Item.Door,1u)]|>Inventory.ofList;
                forbidden=[]|>Inventory.ofList;
                canCraftOnTerrain=fun x -> x.isWorkbench;
                name="Door";
                description="Craft Door"
            }

            
            //////////////////////////////////////////////////////////////////////
            //Furnace
            //////////////////////////////////////////////////////////////////////

            {
                input=[(Item.Ore Stone,8u)]|>Inventory.ofList;
                output=[(Item.CraftingBox CraftingMethod.Furnace,1u)]|>Inventory.ofList;
                forbidden=[]|>Inventory.ofList;
                canCraftOnTerrain=fun x -> x.isWorkbench;
                name="Furnace";
                description="Craft Furnace"
            }
            
            //////////////////////////////////////////////////////////////////////
            //Axes
            //////////////////////////////////////////////////////////////////////

            Recipe.craftAxe (Item.Log                           , ToolMaterial.Wood       ,   25u , Terrain.isWorkbench, "Axe", "Craft Wooden Axe");
            Recipe.craftAxe (Item.Ore Stone                     , ToolMaterial.Stone      ,   50u , Terrain.isWorkbench, "Axe", "Craft Stone Axe");
            Recipe.craftAxe (Item.Ore Flint                     , ToolMaterial.Flint      ,   75u , Terrain.isWorkbench, "Axe", "Craft Flint Axe");
            
            //////////////////////////////////////////////////////////////////////
            //Picks
            //////////////////////////////////////////////////////////////////////

            Recipe.craftPick (Item.Log                           , ToolMaterial.Wood       ,   25u , Terrain.isWorkbench, "Pick", "Craft Wooden Pick");
            Recipe.craftPick (Item.Ore Stone                     , ToolMaterial.Stone      ,   50u , Terrain.isWorkbench, "Pick", "Craft Stone Pick");
            Recipe.craftPick (Item.Ore Flint                     , ToolMaterial.Flint      ,   75u , Terrain.isWorkbench, "Pick", "Craft Flint Pick");
            
            //////////////////////////////////////////////////////////////////////
            //Spears
            //////////////////////////////////////////////////////////////////////

            Recipe.craftSpear (Item.Log                           , ToolMaterial.Wood       ,   25u , Terrain.isWorkbench, "Spear", "Craft Wooden Spear");
            Recipe.craftSpear (Item.Ore Stone                     , ToolMaterial.Stone      ,   50u , Terrain.isWorkbench, "Spear", "Craft Stone Spear");
            Recipe.craftSpear (Item.Ore Flint                     , ToolMaterial.Flint      ,   75u , Terrain.isWorkbench, "Spear", "Craft Flint Spear");
            
            //////////////////////////////////////////////////////////////////////
            //Shovels
            //////////////////////////////////////////////////////////////////////

            Recipe.craftShovel (Item.Log                           , ToolMaterial.Wood       ,   25u , Terrain.isWorkbench, "Shovel", "Craft Wooden Shovel");
            Recipe.craftShovel (Item.Ore Stone                     , ToolMaterial.Stone      ,   50u , Terrain.isWorkbench, "Shovel", "Craft Stone Shovel");
            Recipe.craftShovel (Item.Ore Flint                     , ToolMaterial.Flint      ,   75u , Terrain.isWorkbench, "Shovel", "Craft Flint Shovel");

            //////////////////////////////////////////////////////////////////////
            //Knives
            //////////////////////////////////////////////////////////////////////

            Recipe.craftKnife (Item.Log                           , ToolMaterial.Wood       ,   25u , Terrain.isWorkbench, "Knife", "Craft Wooden Knife");
            Recipe.craftKnife (Item.Ore Stone                     , ToolMaterial.Stone      ,   50u , Terrain.isWorkbench, "Knife", "Craft Stone Knife");
            Recipe.craftKnife (Item.Ore Flint                     , ToolMaterial.Flint      ,   75u , Terrain.isWorkbench, "Knife", "Craft Flint Knife");
            //////////////////////////////////////////////////////////////////////
            //Hammers
            //////////////////////////////////////////////////////////////////////

            Recipe.craftHammer (Item.Log                           , ToolMaterial.Wood       ,   25u , Terrain.isWorkbench, "Hammer", "Craft Wooden Hammer");
            Recipe.craftHammer (Item.Ore Stone                     , ToolMaterial.Stone      ,   50u , Terrain.isWorkbench, "Hammer", "Craft Stone Hammer");
            Recipe.craftHammer (Item.Ore Flint                     , ToolMaterial.Flint      ,   75u , Terrain.isWorkbench, "Hammer", "Craft Flint Hammer");

            //////////////////////////////////////////////////////////////////////
            //Hoes
            //////////////////////////////////////////////////////////////////////

            Recipe.craftHoe (Item.Log                           , ToolMaterial.Wood       ,   25u , Terrain.isWorkbench, "Hoe", "Craft Wooden Hoe");
            Recipe.craftHoe (Item.Ore Stone                     , ToolMaterial.Stone      ,   50u , Terrain.isWorkbench, "Hoe", "Craft Stone Hoe");
            Recipe.craftHoe (Item.Ore Flint                     , ToolMaterial.Flint      ,   75u , Terrain.isWorkbench, "Hoe", "Craft Flint Hoe");

            //////////////////////////////////////////////////////////////////////
            //Berry Seeds
            //////////////////////////////////////////////////////////////////////

            Recipe.craftBerrySeed (Berry.Carberry, "Berry Seed", "Craft Berry Seed");
            Recipe.craftBerrySeed (Berry.Copberry, "Berry Seed", "Craft Berry Seed");
            Recipe.craftBerrySeed (Berry.Golberry, "Berry Seed", "Craft Berry Seed");
            Recipe.craftBerrySeed (Berry.Jadberry, "Berry Seed", "Craft Berry Seed");
            Recipe.craftBerrySeed (Berry.Turberry, "Berry Seed", "Craft Berry Seed");
            Recipe.craftBerrySeed (Berry.Ruberry , "Berry Seed", "Craft Berry Seed");
            Recipe.craftBerrySeed (Berry.Amberry , "Berry Seed", "Craft Berry Seed");
            Recipe.craftBerrySeed (Berry.Silberry, "Berry Seed", "Craft Berry Seed");

            //////////////////////////////////////////////////////////////////////
            //Meat
            //////////////////////////////////////////////////////////////////////

            Recipe.skinRabbit (ToolMaterial.Wood      , "Raw Meat", "Butcher Rabbit");
            Recipe.skinRabbit (ToolMaterial.Stone     , "Raw Meat", "Butcher Rabbit");
            Recipe.skinRabbit (ToolMaterial.Flint     , "Raw Meat", "Butcher Rabbit");
            Recipe.skinRabbit (ToolMaterial.Copper    , "Raw Meat", "Butcher Rabbit");
            Recipe.skinRabbit (ToolMaterial.Bronze    , "Raw Meat", "Butcher Rabbit");
            Recipe.skinRabbit (ToolMaterial.Iron      , "Raw Meat", "Butcher Rabbit");
            Recipe.skinRabbit (ToolMaterial.Mithril   , "Raw Meat", "Butcher Rabbit");
            Recipe.skinRabbit (ToolMaterial.Unobtanium, "Raw Meat", "Butcher Rabbit");

            Recipe.skinTagon (ToolMaterial.Wood      , "Raw Meat", "Butcher Human");
            Recipe.skinTagon (ToolMaterial.Stone     , "Raw Meat", "Butcher Human");
            Recipe.skinTagon (ToolMaterial.Flint     , "Raw Meat", "Butcher Human");
            Recipe.skinTagon (ToolMaterial.Copper    , "Raw Meat", "Butcher Human");
            Recipe.skinTagon (ToolMaterial.Bronze    , "Raw Meat", "Butcher Human");
            Recipe.skinTagon (ToolMaterial.Iron      , "Raw Meat", "Butcher Human");
            Recipe.skinTagon (ToolMaterial.Mithril   , "Raw Meat", "Butcher Human");
            Recipe.skinTagon (ToolMaterial.Unobtanium, "Raw Meat", "Butcher Human");

            Recipe.gutFish (ToolMaterial.Wood      , "Raw Meat", "Gut Fish");
            Recipe.gutFish (ToolMaterial.Stone     , "Raw Meat", "Gut Fish");
            Recipe.gutFish (ToolMaterial.Flint     , "Raw Meat", "Gut Fish");
            Recipe.gutFish (ToolMaterial.Copper    , "Raw Meat", "Gut Fish");
            Recipe.gutFish (ToolMaterial.Bronze    , "Raw Meat", "Gut Fish");
            Recipe.gutFish (ToolMaterial.Iron      , "Raw Meat", "Gut Fish");
            Recipe.gutFish (ToolMaterial.Mithril   , "Raw Meat", "Gut Fish");
            Recipe.gutFish (ToolMaterial.Unobtanium, "Raw Meat", "Gut Fish");

            {
                input=[(Item.Meat false,1u);(Item.Fire Fire.Normal,1u)]|>Inventory.ofList;
                output=[(Item.Meat true,1u)]|>Inventory.ofList;
                forbidden=[]|>Inventory.ofList;
                canCraftOnTerrain=fun x -> x.isFurnace;
                name="Cooked Meat";
                description="Cook meat"
            }

            Recipe.craftBoneMeal (Item.Ore Stone)
            Recipe.craftBoneMeal (Item.Tool (Tool.Hammer, ToolMaterial.Wood))
            Recipe.craftBoneMeal (Item.Tool (Tool.Hammer, ToolMaterial.Stone))
            Recipe.craftBoneMeal (Item.Tool (Tool.Hammer, ToolMaterial.Flint))
            Recipe.craftBoneMeal (Item.Tool (Tool.Hammer, ToolMaterial.Copper))
            Recipe.craftBoneMeal (Item.Tool (Tool.Hammer, ToolMaterial.Bronze))
            Recipe.craftBoneMeal (Item.Tool (Tool.Hammer, ToolMaterial.Iron))
            Recipe.craftBoneMeal (Item.Tool (Tool.Hammer, ToolMaterial.Mithril))
            Recipe.craftBoneMeal (Item.Tool (Tool.Hammer, ToolMaterial.Unobtanium))

            //////////////////////////////////////////////////////////////////////
            //Fire
            //////////////////////////////////////////////////////////////////////

            {
                input=[(Item.Ore Flint,1u)]|>Inventory.ofList;
                output=[(Item.Fire Fire.Normal,1u)]|>Inventory.ofList;
                forbidden=[]|>Inventory.ofList;
                canCraftOnTerrain=Terrain.isNotTraderOrQuest;
                name="Fire";
                description="Start fire"
            }

            {
                input=[(Item.Fire Fire.Normal,1u);(Item.Log,1u)]|>Inventory.ofList;
                output=[(Item.Fire Fire.Normal,10u)]|>Inventory.ofList;
                forbidden=[]|>Inventory.ofList;
                canCraftOnTerrain=fun x -> x.isFurnace;
                name="Fire";
                description="Burn Log"
            }

            {
                input=[(Item.Fire Fire.Normal,1u);(Item.CraftingBox CraftingMethod.Workbench,1u)]|>Inventory.ofList;
                output=[(Item.Fire Fire.Normal,10u)]|>Inventory.ofList;
                forbidden=[]|>Inventory.ofList;
                canCraftOnTerrain=fun x -> x.isFurnace;
                name="Fire";
                description="Burn Workbench"
            }

            {
                input=[(Item.Fire Fire.Normal,1u);(Item.Meat false,1u)]|>Inventory.ofList;
                output=[(Item.Fire Fire.Magic,1u)]|>Inventory.ofList;
                forbidden=[]|>Inventory.ofList;
                canCraftOnTerrain=fun x -> x.isAltar;
                name="Magic Fire";
                description="Make Sacrifice"
            }

            //////////////////////////////////////////////////////////////////////
            //Ingots
            //////////////////////////////////////////////////////////////////////

            Recipe.craftIngot (Fire.Normal,[(Copper,2u)],ToolMaterial.Copper,Terrain.isFurnace)
            Recipe.craftIngot (Fire.Normal,[(Copper,1u);(Tin,1u)],ToolMaterial.Bronze,Terrain.isFurnace)
            Recipe.craftIngot (Fire.Normal,[(Iron,2u)],ToolMaterial.Iron,Terrain.isFurnace)
            Recipe.craftIngot (Fire.Magic,[(Mithril,2u)],ToolMaterial.Mithril,Terrain.isFurnace)
            Recipe.craftIngot (Fire.Magic,[(Unobtainium,2u)],ToolMaterial.Unobtanium,Terrain.isFurnace)

            /////////////////////////////////////////////////////////////////////////////////////////////
            // Traders
            /////////////////////////////////////////////////////////////////////////////////////////////

            //log trader
            Recipe.simpleTrade(Item.Log,   2u, 0u, Item.Berry Berry.Carberry, 1u, 1u, Trader.Wood)
            Recipe.simpleTrade(Item.Log,   5u, 1u, Item.Berry Berry.Copberry, 1u, 2u, Trader.Wood)
            Recipe.simpleTrade(Item.Log,  10u, 2u, Item.Berry Berry.Golberry, 1u, 3u, Trader.Wood)
            Recipe.simpleTrade(Item.Log,  20u, 3u, Item.Berry Berry.Jadberry, 1u, 4u, Trader.Wood)
            Recipe.simpleTrade(Item.Log,  50u, 4u, Item.Berry Berry.Turberry, 1u, 5u, Trader.Wood)
            Recipe.simpleTrade(Item.Log, 100u, 5u, Item.Berry Berry.Ruberry,  1u, 6u, Trader.Wood)
            Recipe.simpleTrade(Item.Log, 200u, 6u, Item.Berry Berry.Amberry,  1u, 7u, Trader.Wood)
            Recipe.simpleTrade(Item.Log, 500u, 7u, Item.Berry Berry.Silberry, 1u, 8u, Trader.Wood)
            Recipe.simpleTrade(Item.Berry Berry.Carberry, 1u,  5u, Item.Log,   1u,  4u, Trader.Wood)
            Recipe.simpleTrade(Item.Berry Berry.Copberry, 1u, 10u, Item.Log,   2u,  9u, Trader.Wood)
            Recipe.simpleTrade(Item.Berry Berry.Golberry, 1u, 15u, Item.Log,   5u, 14u, Trader.Wood)
            Recipe.simpleTrade(Item.Berry Berry.Jadberry, 1u, 20u, Item.Log,  10u, 19u, Trader.Wood)
            Recipe.simpleTrade(Item.Berry Berry.Turberry, 1u, 25u, Item.Log,  25u, 24u, Trader.Wood)
            Recipe.simpleTrade(Item.Berry Berry.Ruberry,  1u, 30u, Item.Log,  50u, 29u, Trader.Wood)
            Recipe.simpleTrade(Item.Berry Berry.Amberry,  1u, 35u, Item.Log, 100u, 34u, Trader.Wood)
            Recipe.simpleTrade(Item.Berry Berry.Silberry, 1u, 40u, Item.Log, 250u, 39u, Trader.Wood)

            //fur trader
            Recipe.simpleTrade(Item.Log, 10u, 0u, Item.Tool (Tool.Trap, ToolMaterial.Wood), 5u, 0u, Trader.Fur) 
            |> Recipe.addForbidden (Item.QuestFlag QuestFlag.FurTrading) 5u 
            |> Recipe.addOutput (Item.QuestFlag QuestFlag.FurTrading) 1u

            Recipe.simpleTrade(Item.RabbitPelt,  4u,  0u, Item.Tool (Tool.Trap, ToolMaterial.Wood),   5u,  1u, Trader.Fur)
            Recipe.simpleTrade(Item.RabbitPelt,  7u, 10u, Item.Tool (Tool.Trap, ToolMaterial.Stone), 10u, 11u, Trader.Fur)
            Recipe.simpleTrade(Item.RabbitPelt, 10u, 50u, Item.Tool (Tool.Trap, ToolMaterial.Flint), 15u, 51u, Trader.Fur)

            /////////////////////////////////////////////////////////////////////////////////////////////
            // Quest
            /////////////////////////////////////////////////////////////////////////////////////////////

            {
                input=[(Item.Log,10u)]|>Inventory.ofList;
                output=[(Item.QuestFlag QuestFlag.Log,1u)]|>Inventory.ofList;
                forbidden=[(Item.QuestFlag QuestFlag.Log,1u)]|>Inventory.ofList;
                canCraftOnTerrain=fun x -> x.isQuest;
                name="Log Quest";
                description="Log Quest"
            }

        ]

        |> List.append ((Recipe.craftAxe (Item.Ingot ToolMaterial.Copper     , ToolMaterial.Copper,       100u , Terrain.isFurnace,   "Axe", "Craft Copper Axe")).multiplex Recipe.hammersForCopper)
        |> List.append ((Recipe.craftAxe (Item.Ingot ToolMaterial.Bronze     , ToolMaterial.Bronze     ,  150u , Terrain.isFurnace,   "Axe", "Craft Bronze Axe")).multiplex Recipe.hammersForBronze)
        |> List.append ((Recipe.craftAxe (Item.Ingot ToolMaterial.Iron       , ToolMaterial.Iron       ,  250u , Terrain.isFurnace,   "Axe", "Craft Iron Axe")).multiplex Recipe.hammersForIron)
        |> List.append ((Recipe.craftAxe (Item.Ingot ToolMaterial.Mithril    , ToolMaterial.Mithril    ,  500u , Terrain.isFurnace,   "Axe", "Craft Mithril Axe")).multiplex Recipe.hammersForMithril)
        |> List.append ((Recipe.craftAxe (Item.Ingot ToolMaterial.Unobtanium , ToolMaterial.Unobtanium , 1000u , Terrain.isFurnace,   "Axe", "Craft Unobtainium Axe")).multiplex Recipe.hammersForMithril)

        |> List.append ((Recipe.craftPick (Item.Ingot ToolMaterial.Copper     , ToolMaterial.Copper,       100u , Terrain.isFurnace,   "Pick", "Craft Copper Pick")).multiplex Recipe.hammersForCopper)
        |> List.append ((Recipe.craftPick (Item.Ingot ToolMaterial.Bronze     , ToolMaterial.Bronze     ,  150u , Terrain.isFurnace,   "Pick", "Craft Bronze Pick")).multiplex Recipe.hammersForBronze)
        |> List.append ((Recipe.craftPick (Item.Ingot ToolMaterial.Iron       , ToolMaterial.Iron       ,  250u , Terrain.isFurnace,   "Pick", "Craft Iron Pick")).multiplex Recipe.hammersForIron)
        |> List.append ((Recipe.craftPick (Item.Ingot ToolMaterial.Mithril    , ToolMaterial.Mithril    ,  500u , Terrain.isFurnace,   "Pick", "Craft Mithril Pick")).multiplex Recipe.hammersForMithril)
        |> List.append ((Recipe.craftPick (Item.Ingot ToolMaterial.Unobtanium , ToolMaterial.Unobtanium , 1000u , Terrain.isFurnace,   "Pick", "Craft Unobtainium Pick")).multiplex Recipe.hammersForMithril)

        |> List.append ((Recipe.craftSpear (Item.Ingot ToolMaterial.Copper     , ToolMaterial.Copper,       100u , Terrain.isFurnace,   "Spear", "Craft Copper Spear")).multiplex Recipe.hammersForCopper)
        |> List.append ((Recipe.craftSpear (Item.Ingot ToolMaterial.Bronze     , ToolMaterial.Bronze     ,  150u , Terrain.isFurnace,   "Spear", "Craft Bronze Spear")).multiplex Recipe.hammersForBronze)
        |> List.append ((Recipe.craftSpear (Item.Ingot ToolMaterial.Iron       , ToolMaterial.Iron       ,  250u , Terrain.isFurnace,   "Spear", "Craft Iron Spear")).multiplex Recipe.hammersForIron)
        |> List.append ((Recipe.craftSpear (Item.Ingot ToolMaterial.Mithril    , ToolMaterial.Mithril    ,  500u , Terrain.isFurnace,   "Spear", "Craft Mithril Spear")).multiplex Recipe.hammersForMithril)
        |> List.append ((Recipe.craftSpear (Item.Ingot ToolMaterial.Unobtanium , ToolMaterial.Unobtanium , 1000u , Terrain.isFurnace,   "Spear", "Craft Unobtainium Spear")).multiplex Recipe.hammersForMithril)

        |> List.append ((Recipe.craftKnife (Item.Ingot ToolMaterial.Copper     , ToolMaterial.Copper,       100u , Terrain.isFurnace,   "Knife", "Craft Copper Knife")).multiplex Recipe.hammersForCopper)
        |> List.append ((Recipe.craftKnife (Item.Ingot ToolMaterial.Bronze     , ToolMaterial.Bronze     ,  150u , Terrain.isFurnace,   "Knife", "Craft Bronze Knife")).multiplex Recipe.hammersForBronze)
        |> List.append ((Recipe.craftKnife (Item.Ingot ToolMaterial.Iron       , ToolMaterial.Iron       ,  250u , Terrain.isFurnace,   "Knife", "Craft Iron Knife")).multiplex Recipe.hammersForIron)
        |> List.append ((Recipe.craftKnife (Item.Ingot ToolMaterial.Mithril    , ToolMaterial.Mithril    ,  500u , Terrain.isFurnace,   "Knife", "Craft Mithril Knife")).multiplex Recipe.hammersForMithril)
        |> List.append ((Recipe.craftKnife (Item.Ingot ToolMaterial.Unobtanium , ToolMaterial.Unobtanium , 1000u , Terrain.isFurnace,   "Knife", "Craft Unobtainium Knife")).multiplex Recipe.hammersForMithril)

        |> List.append ((Recipe.craftHoe (Item.Ingot ToolMaterial.Copper     , ToolMaterial.Copper,       100u , Terrain.isFurnace,   "Hoe", "Craft Copper Hoe")).multiplex Recipe.hammersForCopper)
        |> List.append ((Recipe.craftHoe (Item.Ingot ToolMaterial.Bronze     , ToolMaterial.Bronze     ,  150u , Terrain.isFurnace,   "Hoe", "Craft Bronze Hoe")).multiplex Recipe.hammersForBronze)
        |> List.append ((Recipe.craftHoe (Item.Ingot ToolMaterial.Iron       , ToolMaterial.Iron       ,  250u , Terrain.isFurnace,   "Hoe", "Craft Iron Hoe")).multiplex Recipe.hammersForIron)
        |> List.append ((Recipe.craftHoe (Item.Ingot ToolMaterial.Mithril    , ToolMaterial.Mithril    ,  500u , Terrain.isFurnace,   "Hoe", "Craft Mithril Hoe")).multiplex Recipe.hammersForMithril)
        |> List.append ((Recipe.craftHoe (Item.Ingot ToolMaterial.Unobtanium , ToolMaterial.Unobtanium , 1000u , Terrain.isFurnace,   "Hoe", "Craft Unobtainium Hoe")).multiplex Recipe.hammersForMithril)

        |> List.append ((Recipe.craftHammer (Item.Ingot ToolMaterial.Copper     , ToolMaterial.Copper,       100u , Terrain.isFurnace,   "Hammer", "Craft Copper Hammer")).multiplex Recipe.hammersForCopper)
        |> List.append ((Recipe.craftHammer (Item.Ingot ToolMaterial.Bronze     , ToolMaterial.Bronze     ,  150u , Terrain.isFurnace,   "Hammer", "Craft Bronze Hammer")).multiplex Recipe.hammersForBronze)
        |> List.append ((Recipe.craftHammer (Item.Ingot ToolMaterial.Iron       , ToolMaterial.Iron       ,  250u , Terrain.isFurnace,   "Hammer", "Craft Iron Hammer")).multiplex Recipe.hammersForIron)
        |> List.append ((Recipe.craftHammer (Item.Ingot ToolMaterial.Mithril    , ToolMaterial.Mithril    ,  500u , Terrain.isFurnace,   "Hammer", "Craft Mithril Hammer")).multiplex Recipe.hammersForMithril)
        |> List.append ((Recipe.craftHammer (Item.Ingot ToolMaterial.Unobtanium , ToolMaterial.Unobtanium , 1000u , Terrain.isFurnace,   "Hammer", "Craft Unobtainium Hammer")).multiplex Recipe.hammersForMithril)

        |> List.append ((Recipe.craftShovel (Item.Ingot ToolMaterial.Copper     , ToolMaterial.Copper,       100u , Terrain.isFurnace,   "Shovel", "Craft Copper Shovel")).multiplex Recipe.hammersForCopper)
        |> List.append ((Recipe.craftShovel (Item.Ingot ToolMaterial.Bronze     , ToolMaterial.Bronze     ,  150u , Terrain.isFurnace,   "Shovel", "Craft Bronze Shovel")).multiplex Recipe.hammersForBronze)
        |> List.append ((Recipe.craftShovel (Item.Ingot ToolMaterial.Iron       , ToolMaterial.Iron       ,  250u , Terrain.isFurnace,   "Shovel", "Craft Iron Shovel")).multiplex Recipe.hammersForIron)
        |> List.append ((Recipe.craftShovel (Item.Ingot ToolMaterial.Mithril    , ToolMaterial.Mithril    ,  500u , Terrain.isFurnace,   "Shovel", "Craft Mithril Shovel")).multiplex Recipe.hammersForMithril)
        |> List.append ((Recipe.craftShovel (Item.Ingot ToolMaterial.Unobtanium , ToolMaterial.Unobtanium , 1000u , Terrain.isFurnace,   "Shovel", "Craft Unobtainium Shovel")).multiplex Recipe.hammersForMithril)

        |> List.sortBy (fun x-> x.description)

    member r.isCraftable (terrain:Terrain) (inventory:Inventory) : bool =
        (terrain |> r.canCraftOnTerrain) && (inventory.meetsNeed r.input) && ((inventory.hasContraband r.forbidden) |> not)
    
    member r.isVisible (terrain:Terrain) (inventory:Inventory) : bool =
        (terrain |> r.canCraftOnTerrain) && (inventory.hasAny r.input) && ((inventory.hasContraband r.forbidden) |> not)

    static member getCraftableRecipes (terrain:Terrain) (inventory:Inventory) : Recipe list =
        Recipe.recipes
        |> List.filter (fun r -> r.isCraftable terrain inventory)

    static member getVisibleRecipes (terrain:Terrain) (inventory:Inventory) : Recipe list =
        Recipe.recipes
        |> List.filter (fun r -> r.isVisible terrain inventory)

    static member getCraftableRecipeCount (terrain:Terrain) (inventory:Inventory) : uint32 =
        (0u, Recipe.recipes)
        ||> List.fold (fun acc r ->
                        if r.isCraftable terrain inventory then
                            acc + 1u
                        else
                            acc)
    
    static member getVisibleRecipeCount (terrain:Terrain) (inventory:Inventory) : uint32 =
        (0u, Recipe.recipes)
        ||> List.fold (fun acc r ->
                        if r.isVisible terrain inventory then
                            acc + 1u
                        else
                            acc)


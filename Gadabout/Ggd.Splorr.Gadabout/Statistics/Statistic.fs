﻿namespace Ggd.Splorr.Gadabout

[<RequireQualifiedAccess>]
type Statistic =
    | Energy
    | Health
    | Hunger
    | Exertion

﻿namespace Ggd.Splorr.Gadabout

type StatisticValue =
    {minimum:float;
    maximum:float;
    current:float}
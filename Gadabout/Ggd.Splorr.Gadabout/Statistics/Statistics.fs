﻿namespace Ggd.Splorr.Gadabout

type Statistics =
    {table: Map<Statistic,StatisticValue>}

    static member private defaultStatistics =
        [(Statistic.Energy, {minimum=0.0;maximum=100.0;current=100.0});
        (Statistic.Health, {minimum=0.0;maximum=100.0;current=100.0});
        (Statistic.Exertion, {minimum=(-5.0);maximum=0.0;current=(-0.1)});
        (Statistic.Hunger, {minimum=(-100.0);maximum=200.0;current=100.0})]
        |> Map.ofList

    member private statistics.getStatistic (statistic:Statistic) : StatisticValue =
        match statistics.table |> Map.tryFind statistic with
        | Some v -> v
        | None -> Statistics.defaultStatistics.[statistic]

    member private statistics.setStatistic (statistic:Statistic) (statisticValue:StatisticValue) : Statistics =
        {table = statistics.table |> Map.add statistic statisticValue}

    member statistics.getCurrent (statistic:Statistic) : float =
        (statistic |> statistics.getStatistic).current

    member private statistics.getMinimum (statistic:Statistic) : float =
        (statistic |> statistics.getStatistic).minimum

    member statistics.setCurrent (statistic:Statistic) (current:float) : Statistics =
        let statisticValue =
            (statistic |> statistics.getStatistic)
        let current =
            match current with
            | v when v > statisticValue.maximum -> statisticValue.maximum
            | v when v < statisticValue.minimum -> statisticValue.minimum
            | v  -> v
        statistics.setStatistic statistic {statisticValue with current = current}

    member statistics.changeCurrentBy (statistic:Statistic) (change:float) : Statistics =
        statistics.setCurrent statistic ((statistics.getCurrent statistic)+change)

    member statistics.isCurrentAtMinimum (statistic:Statistic) : bool =
        (statistics.getCurrent statistic) = (statistics.getMinimum statistic)


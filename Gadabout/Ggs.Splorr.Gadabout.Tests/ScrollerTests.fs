﻿namespace Ggd.Splorr.Gadabout.Tests

open Microsoft.VisualStudio.TestTools.UnitTesting
open Ggd.Splorr.Gadabout

[<TestClass>]
type ScrollerTests() = 
    
    [<TestMethod>]
    member this.TestScrollerInitialize() =
        let itemCount = 1u
        let listSize = 2u
        let scroller = Scroller.initialize itemCount listSize
        Assert.AreEqual(itemCount, scroller.itemCount)
        Assert.AreEqual(listSize, scroller.listSize)
        Assert.AreEqual(0u, scroller.index)
        Assert.AreEqual(0u, scroller.scroll)

    [<TestMethod>]
    member this.TestGetMaximumScrollWithCountMoreThanSize() =
        let itemCount = 2u
        let listSize = 1u
        let actual = (Scroller.initialize itemCount listSize).getMaximumScroll()
        let expected = 1u
        Assert.AreEqual(expected, actual)

    [<TestMethod>]
    member this.TestGetMaximumScrollWithCountLessThanSize() =
        let itemCount = 1u
        let listSize = 2u
        let actual = (Scroller.initialize itemCount listSize).getMaximumScroll()
        let expected = 0u
        Assert.AreEqual(expected, actual)

    [<TestMethod>]
    member this.TestGetMaximumScrollWithCountSameAsSize() =
        let itemCount = 2u
        let listSize = 2u
        let actual = (Scroller.initialize itemCount listSize).getMaximumScroll()
        let expected = 0u
        Assert.AreEqual(expected, actual)

    [<TestMethod>]
    member this.TestSetItemCount()=
        let itemCount = 1u
        let listSize = 2u
        let newItemCount = 3u
        let scroller = (Scroller.initialize itemCount listSize).setItemCount newItemCount
        Assert.AreEqual(newItemCount, scroller.itemCount)
        Assert.AreEqual(0u, scroller.scroll)

    [<TestMethod>]
    member this.TestNextIndex()=
        let itemCount = 3u
        let listSize = 1u
        let scroller = (Scroller.initialize itemCount listSize).nextIndex()
        let expectedIndex = 1u
        let expectedScroll = 1u
        Assert.AreEqual(expectedIndex, scroller.index)
        Assert.AreEqual(expectedScroll, scroller.scroll)

    [<TestMethod>]
    member this.TestNextIndexTwice()=
        let itemCount = 3u
        let listSize = 1u
        let scroller = (Scroller.initialize itemCount listSize).nextIndex().nextIndex()
        let expectedIndex = 2u
        let expectedScroll = 2u
        Assert.AreEqual(expectedIndex, scroller.index)
        Assert.AreEqual(expectedScroll, scroller.scroll)

    [<TestMethod>]
    member this.TestPreviousIndex()=
        let itemCount = 3u
        let listSize = 1u
        let scroller = (Scroller.initialize itemCount listSize).previousIndex()
        let expectedIndex = 2u
        let expectedScroll = 2u
        Assert.AreEqual(expectedIndex, scroller.index)
        Assert.AreEqual(expectedScroll, scroller.scroll)

    [<TestMethod>]
    member this.TestPreviousIndexTwice()=
        let itemCount = 3u
        let listSize = 1u
        let scroller = (Scroller.initialize itemCount listSize).previousIndex().previousIndex()
        let expectedIndex = 1u
        let expectedScroll = 1u
        Assert.AreEqual(expectedIndex, scroller.index)
        Assert.AreEqual(expectedScroll, scroller.scroll)

    
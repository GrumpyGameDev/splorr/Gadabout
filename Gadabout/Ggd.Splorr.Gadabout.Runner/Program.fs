﻿open Ggd.Splorr.Gadabout.MGClient

[<EntryPoint>]
let main argv = 
    use client = new Client()
    client.Run()
    0
